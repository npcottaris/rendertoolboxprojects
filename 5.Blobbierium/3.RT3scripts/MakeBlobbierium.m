function MakeBlobbierium

    [rootDir, ~] = fileparts(which(mfilename()));
    workingFolder = '/Volumes/SDXC_128GB/RT3Caches';
    workingFolder = '/Users/nicolas/RT3Caches10';
    
    sceneNames = {...
        'BlobbieriumHighDensity' ...
        };
    
    sceneNames = {...
        'BlobbieriumHighDensityNoCutout' ...
        };
    
    % These must match the settings in Blender/Python for expected geometry
    viewingDistance = 76.4;
    cameraFOV = 33;
    cameraElevation = 19.0;
    cameraWidthToHeightAspectRatio = 1.45;
    displayPanelDims = [51.7988 32.3618];
    
    for k = numel(sceneNames):-1:1
        MakeRoom(rootDir, workingFolder, sceneNames{k}, viewingDistance, cameraFOV, cameraElevation, cameraWidthToHeightAspectRatio, displayPanelDims);
    end
    
end


function MakeRoom(rootDir, workingFolder, sceneName, viewingDistance, cameraFOV, cameraElevation, cameraWidthToHeightAspectRatio, displayPanelDims)
    
    
    colladaDir = strrep(rootDir,'3.RT3scripts', '2.ColladaExports');
    
    parentSceneFile = sprintf('%s/%s.dae', colladaDir, sceneName);
    conditionsFile  = sprintf('%s/1.Conditions/BlobbieriumConditions.txt', rootDir);
    mappingsFile    = sprintf('%s/2.Mappings/BlobbieriumMappings.txt', rootDir);
    
    fprintf('Collada file: %s\nOutput in %s\n', parentSceneFile, workingFolder);
    
    % The image size to generate. This can be scaled up/down in StereoViewController
    desiredImageWidthInPixels = 1280;
    desiredImageHeightInPixels =  round(desiredImageWidthInPixels/cameraWidthToHeightAspectRatio);
    
    stimulusWidthInCm = 2*viewingDistance*tan(cameraFOV/2/180*pi);
    stimulusHeightInCm = stimulusWidthInCm / cameraWidthToHeightAspectRatio;
    
    if (stimulusWidthInCm > displayPanelDims(1))
        error('Stimulus width (%2.2f) will be larger than display width (%2.2f). Reduce stimulus width!', stimulusWidthInCm, displayPanelDims(1));
    end
    if (stimulusHeightInCm > displayPanelDims(2))
        error('Stimulus height (%2.2f) be larger than display height (%2.2f). Reduce stimulus height!', stimulusHeightInCm, displayPanelDims(2));
    end
    
    if (stimulusWidthInCm <= displayPanelDims(1)) && (stimulusHeightInCm <= displayPanelDims(2))
        fprintf('Stimulus size [%2.1f %2.1f] will fit in display panel [%2.1f %2.1f]\n', stimulusWidthInCm, stimulusHeightInCm, displayPanelDims(1), displayPanelDims(2));
    end
    
    % Choose batch renderer options.
    hints = struct(...
        'imageWidth',               desiredImageWidthInPixels, ...
        'imageHeight',              desiredImageHeightInPixels, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba', ...
        'workingFolder',            fullfile(workingFolder, mfilename()) ...
        );
    
    ChangeToWorkingFolder(hints);

    isScaleGamma = true;
    toneMapFactor = 4;
    
    eyeLabel   = {'Left','Right'};
    eyeXpos    = [ -3.2 3.2];
    
    eyeLabel   = {'Cyclopean'};
    eyeXpos    = [ 0.0 ];
    
    % match the settings in the python file for exact geometry
    
    cameraDepthPos = -viewingDistance;
    
    stereoView = containers.Map(eyeLabel,eyeXpos);
   
    blobbieAlpha = 0.1;
    blobbieAlphas = blobbieAlpha * [1 1 1 1 1 1 1 1];
    blobbieDiffuseSPDs = {...
        'mccBabel-9.spd' ...
        'mccBabel-4.spd' ...
        'mccBabel-10.spd' ...
        'mccBabel-11.spd' ...
        'mccBabel-13.spd' ...
        'mccBabel-14.spd' ...
        'mccBabel-15.spd' ...
        'mccBabel-17.spd' ...
        };
    
    diffuseSPD = 'mccBabel-9.spd';  % lightblue
    diffuseSPD = 'mccBabel-10.spd'; % rasberry
    diffuseSPD = 'mccBabel-11.spd';  % strawberry
    diffuseSPD = 'mccBabel-13.spd'; %  VeggieGreen
    diffuseSPD = 'mccBabel-14.spd'; % purple 
    diffuseSPD = 'mccBabel-15.spd'; % mustard yellow
    diffuseSPD = 'mccBabel-17.spd'; % light blue
    diffuseSPD = 'mccBabel-18.spd'; % lime green
    diffuseSPD = 'mccBabel-19.spd'; % magenta
    diffuseSPD = 'mccBabel-21.spd';% pistqchio green
    diffuseSPD = 'mccBabel-22.spd';%red robin yellow
    diffuseSPD = 'mccBabel-23.spd';  % ciel blue
    
    blobbieSpecularSPD = 'mccBabel-4.spd'; % white (brightest)
    blobbieSpecularSPD = 'mccBabel-8.spd'; % white (second brightest)
    blobbieSpecularSPD = 'mccBabel-12.spd'; % white (third brightest)
    
    blobbieDiffuseSPDs = {...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        diffuseSPD ...
        };
    
    blobbieDiffuseSPDs = {...
        'mccBabel-23.spd'  ... % ciel blue
        'mccBabel-22.spd'  ... %red robin yellow
        'mccBabel-21.spd' ... % pistqchio green
        'mccBabel-18.spd' ...  % lime green
        'mccBabel-11.spd' ...  % strawberry
        'mccBabel-10.spd' ...  % rasberry
        'mccBabel-13.spd' ... %  VeggieGreen
        'mccBabel-19.spd' ... % magenta
        };
    
    for eyeLabel = keys(stereoView)
  
        fprintf('Generating %s eye view\n', char(eyeLabel));
        conditionKeys = {'eyeXpos', 'eyeYpos', 'distance', 'fov', ...
            'blobbie1DiffuseSPD', 'blobbie1SpecularSPD', 'blobbie1Alpha', ...
            'blobbie2DiffuseSPD', 'blobbie2SpecularSPD', 'blobbie2Alpha', ...
            'blobbie3DiffuseSPD', 'blobbie3SpecularSPD', 'blobbie3Alpha', ...
            'blobbie4DiffuseSPD', 'blobbie4SpecularSPD','blobbie4Alpha', ...
            'blobbie5DiffuseSPD', 'blobbie5SpecularSPD','blobbie5Alpha', ...
            'blobbie6DiffuseSPD', 'blobbie6SpecularSPD','blobbie6Alpha', ...
            'blobbie7DiffuseSPD', 'blobbie7SpecularSPD','blobbie7Alpha', ...
            'blobbie8DiffuseSPD', 'blobbie8SpecularSPD','blobbie8Alpha', ...
            };
        conditionValues = {stereoView(char(eyeLabel)), cameraElevation, cameraDepthPos, cameraFOV, ...
            blobbieDiffuseSPDs{1}, blobbieSpecularSPD, blobbieAlphas(1), ...
            blobbieDiffuseSPDs{2}, blobbieSpecularSPD, blobbieAlphas(2), ...
            blobbieDiffuseSPDs{3}, blobbieSpecularSPD, blobbieAlphas(3), ...
            blobbieDiffuseSPDs{4}, blobbieSpecularSPD, blobbieAlphas(4),...
            blobbieDiffuseSPDs{5}, blobbieSpecularSPD, blobbieAlphas(5), ...
            blobbieDiffuseSPDs{6}, blobbieSpecularSPD, blobbieAlphas(6), ...
            blobbieDiffuseSPDs{7}, blobbieSpecularSPD, blobbieAlphas(7), ...
            blobbieDiffuseSPDs{8}, blobbieSpecularSPD, blobbieAlphas(8) ...
            };  
        conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, conditionValues);
    
        nativeSceneFiles = MakeSceneFiles(parentSceneFile, conditionsFile, mappingsFile, hints);
        radianceDataFiles = BatchRender(nativeSceneFiles, hints);

        montageName = sprintf('MultiColored_%s_specSPD%s_Alpha%1.3f%s', sceneName, blobbieSpecularSPD, blobbieAlpha);
        montageFile = [montageName '.tiff'];
    
        [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScaleGamma, hints);
        ShowXYZAndSRGB([], SRGBMontage, montageName);
    end
    
    fprintf('Results available at %s\n', hints.workingFolder);
end
