# Script to generate the Blobbierium scene
#
# 6/28/2015  npc  Wrote it.


def generateMaterials(scene):
	from mathutils import Vector

	materials = {};

    # -the room material
	params = { 'name'              : 'roomMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };
	materials['room'] = scene.generateMaterialType(params);

    # the pedestal material
	params = { 'name'              : 'pedestalMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.8, 0.8, 0.8)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };
	materials['pedestal'] = scene.generateMaterialType(params);



  # -the backwall material
	params = { 'name'              : 'backWallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };
	materials['backWall'] = scene.generateMaterialType(params);


  # -the frontwall material
	params = { 'name'              : 'frontWallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 0.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };
	materials['frontWall'] = scene.generateMaterialType(params);

	# -the dark tilematerial
	params = { 'name'              : 'darkTileMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 1.0,
               'diffuse_color'     : Vector((0.2, 0.2, 0.2)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };
	materials['darkTile'] = scene.generateMaterialType(params);


	# Generate the blobbie material
	params = {  'name'              : 'blobbie1Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.7, 0.0, 0.1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie1'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie2Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.7, 0.7, 0.1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie2'] = scene.generateMaterialType(params);


	params = {  'name'              : 'blobbie3Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.1, 0.7, 0.7)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie3'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie4Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.1, 0.1, 0.7)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie4'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie5Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.1, 0.7, 0.1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie5'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie6Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.9, 0.4, 0.1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie6'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie7Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.5, 0.1, 0.9)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie7'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie8Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.6, 0.6, 0.5)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie8'] = scene.generateMaterialType(params);

	params = {  'name'              : 'blobbie9Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.1, 0.6, 0.8)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie9'] = scene.generateMaterialType(params);


	params = {  'name'              : 'blobbie10Material',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.4, 0.8, 0.1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((1.0, 1.0, 1.0)),
                'alpha'             : 1.0
              };
	materials['blobbie10'] = scene.generateMaterialType(params);


	return(materials);


def generateBlobbie(blobbieParams, scene):
    # Basic imports
    import sys
    import imp
    import bpy
    from math import floor, cos, sin, sqrt, atan2, pow, pi
    from mathutils import Vector

    # Start with a sphere
    params = blobbieParams;
    params['flipNormal'] = False;

    blobbieObject = scene.addSphere(params);

    # Modify vertices to introduce bumps
    aX = cos(params['angleX']) * params['freqX'];
    bX = sin(params['angleX']) * params['freqX'];
    aY = cos(params['angleY']) * params['freqY'];
    bY = sin(params['angleY']) * params['freqY'];
    aZ = cos(params['angleZ']) * params['freqZ'];
    bZ = sin(params['angleZ']) * params['freqZ'];

    for f in blobbieObject.data.polygons:
        for idx in f.vertices:
          oldX = blobbieObject.data.vertices[idx].co.x;
          oldY = blobbieObject.data.vertices[idx].co.y;
          oldZ = blobbieObject.data.vertices[idx].co.z;
          blobbieObject.data.vertices[idx].co.x = oldX + params['gainX'] * sin(aX*oldY + bX*oldZ);
          blobbieObject.data.vertices[idx].co.y = oldY + params['gainY'] * sin(aY*oldX + bY*oldZ);
          blobbieObject.data.vertices[idx].co.z = oldZ + params['gainZ'] * sin(aZ*oldX + bZ*oldY);

    return(blobbieObject);


def generateScene(sceneParams, blobbieParams):
	import sys
	import imp
	import bpy
	import random
	from math import floor, cos, sin, tan, sqrt, atan2, atan, pow, pi
	from mathutils import Vector



	# ------------------------------ SCENE LAYOUT-----------------------------
	roomWidth      = 50;
	roomHeight     = 50;
	roomDepth      = 38; #22 26 30 # 34 # 38;
	# aperture width and height are computed via StereoRigDesigner.app with sceneWidth = 42, sceneHeight = 30, roomDepth = 20;
	apertureWidth  = 20.0;
	apertureHeight =  15; # 15.75;

	viewingDistance     = 76.4;
	cameraHorizontalFOV = 33.0;
	cameraElevation     = 19;  # the eye level
	cameraWidthToHeightAspectRatio = 1.45;

	roomWallThickness = 1.0;

	apertureDepthPosition = -roomDepth+roomWallThickness/2;
	apertureHorizontalFOV = 2*atan(apertureWidth/(2*(viewingDistance-roomDepth)))/pi*180;
	print('aperture horizontalFOV: {} deg'.format(apertureHorizontalFOV))
	print('aperture dimensions   :{}cm x {}cm'.format(apertureWidth, apertureHeight))
  	# ------------------------------ SCENE MANAGER SETUP -----------------------------

	print('ToolboxDir = {}'.format(sceneParams['toolboxDirectory']));
	# Append the path to my custom Python scene toolbox to the Blender path
	sys.path.append(sceneParams['toolboxDirectory']);

	# Import the custom scene toolbox module
	import SceneUtilsV1;
	imp.reload(SceneUtilsV1);

	# Initialize a sceneManager
	sceneName = "{}".format(sceneParams['sceneName']);
	params = { 'name'               : sceneName,                 # name of new scene
               'erasePreviousScene' : True,                      # erase old scene
               'sceneWidthInPixels' : 640*2,                     # pixels along the horizontal-dimension
               'sceneHeightInPixels': 480*2,                     # pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0,                       # arbitrary units
               'sceneGridSpacing'   : 10.0,                      # set the spacing between grid lines to 10
               'sceneGridLinesNum'  : 20,                        # display 20 grid lines
              };
	scene = SceneUtilsV1.sceneManager(params);



	# Set the random seed
	random.seed(923431)

	# ---------------------------------- MATERIALS -----------------------------------
  	# Generate the materials
	materials = generateMaterials(scene)


  	# ------------------------- ENCLOSING ROOM ---------------------------
	roomLocation = Vector((0,-roomDepth/2, 0.0));

	roomParams = {'floorName'             : 'floor',
                'backWallName'          : 'backWall',
                'frontWallName'         : 'frontWall',
                'leftWallName'          : 'leftWall',
                'rightWallName'         : 'rightWall',
                'ceilingName'           : 'ceiling',
                'floorMaterialType'     : materials['room'],
                'backWallMaterialType'  : materials['backWall'],
                'frontWallMaterialType' : materials['frontWall'],
                'leftWallMaterialType'  : materials['room'],
                'rightWallMaterialType' : materials['room'],
                'ceilingMaterialType'   : materials['room'],
                'roomWidth'             : roomWidth,
                'roomDepth'             : roomDepth,
                'roomHeight'            : roomHeight,
                'roomLocation'          : roomLocation,
                'wallThickness'			: roomWallThickness,      # a wall thickness is necessary to bore out the window
              };
	roomBox = scene.addRoom(roomParams);



	# make window opening in front
	apertureParams = { 'name': 'windowAperture',
 					 'scaling':  Vector((apertureWidth/2,roomParams['wallThickness']*2,apertureHeight/2)),
 					 'rotation': Vector((0,0,0)),
 					 'location': Vector((0, apertureDepthPosition, cameraElevation)),    # aperture at same height as eye-level
 					 'material': materials['room'],
 	};
	theWindowAperture = scene.addCube(apertureParams);
	scene.boreOut(roomBox['frontWallPlane'], theWindowAperture, True);


	# ---------------------------------BACKWALL 3D TILES ----------------------------
	checkSize = 2.0;

	tileParams = { 	'name': '',
 					'scaling':  Vector((checkSize/2,0.15,checkSize/2)),
 					'rotation': Vector((0,0,0)),
 					'location': Vector((0,0,0)),
 					'material': materials['room'],
 	};

	materialParams = { 'name'      : '',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 1.0,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             };

	checksXNum = int(roomWidth/checkSize);
	checksZNum = int(roomHeight/checkSize);
	for x in range(0, checksXNum+1):
		for z in range(0,checksZNum+1):
			if (x + z)%2 < 1:
				#material = darkGrayMaterial;
				grayLevel = 0.15 + 0.2*random.random();
				extrusion = 2.1 + (random.random()-0.5)*0.7;
			else:
				grayLevel = 0.2 + 0.3*random.random();
				#material = lightGrayMaterial;
				extrusion = 2.1 + (random.random()-0.5)*0.7;
			xx = (x-checksXNum/2)*checkSize;
			zz = (z-checksZNum/2)*checkSize;
			tileParams['name'] = 'tileAt{}-{}'.format(x,z);
			tileParams['location'] = Vector((xx,-extrusion,zz+roomHeight/2));
			materialParams['name'] = 'materialAt{}-{}'.format(x,z);
			materialParams['diffuse_color'] = Vector((grayLevel, grayLevel, grayLevel));
			tileParams['material'] = scene.generateMaterialType(materialParams);
			scene.addCube(tileParams);

	# ------------ ADD FLOOR PEDESTAL WHERE BALL SHADOWS WILL BE PROJECTED-------------------
	pedestalParams = { 	'name': 'floorPedestal',
 					'scaling':  Vector((roomWidth*0.9/2,roomDepth*0.9/2,0.25)),
 					'rotation': Vector((0,0,0)),
 					'location': Vector((0,-roomDepth/2+roomParams['wallThickness'],11)),
 					'material': materials['pedestal'],
 	};
	scene.addCube(pedestalParams);


	# -------- ADD FLOOR TILES ----


	floorTileSize = 4.0;
	floorTilesXNum = int(roomWidth/floorTileSize);
	floorTilesYNum = int(roomWidth/floorTileSize);

	floorTileParams = { 	'name': '',
 					'scaling':  Vector((floorTileSize/2,0.15,floorTileSize/2)),
 					'rotation': Vector((pi/2,0,0)),
 					'location': Vector((0,0,0)),
 					'material': materials['room'],
 	};

	for x in range(0,floorTilesXNum+1):
		for y in range(0, floorTilesYNum+1):
			if (x + y)%2 < 1:
				xx = (x-floorTilesXNum/2)*floorTileSize;
				yy = (y+0.5)*floorTileSize;
				floorTileParams['name'] = 'floorTileAt{}-{}'.format(x,y);
				floorTileParams['location'] = Vector((xx,-roomDepth+roomParams['wallThickness']*2+yy, pedestalParams['location'].z+0.175));
				floorTileParams['material']  = materials['darkTile'];
				scene.addCube(floorTileParams);

	# ------------ ADD BACK WALL PEDESTAL WHERE BALL SHADOWS WILL BE PROJECTED-------------------
	pedestalParams = { 	'name': 'backWallPedestal',
 					'scaling':  Vector((9,9,0.25)),
 					'rotation': Vector((pi/2,0,0)),
 					'location': Vector((0,-3.0,cameraElevation)),
 					'material': materials['pedestal'],
 	};
	scene.addCube(pedestalParams);

	# -------------- ADD THE BLOBBIES -----------------
	print('Generating Blobbies. This may take a while ...')

	#angleModulation = 2*pi;
	#frequencyModulation = 2;
	#gainModulation = 10;


	# deepest depth
	maxDepth = 12;   # next to the wall
	minDepth = -14;  # near the aperture
	minVerticalLocation = 12.5;  # near the floor
	minSize = 1.5;

	blobbiesNumMax = 10;
	blobbieSize = np.zeros((blobbiesNumMax,3))
	blobbieRotation = np.zeros((blobbiesNumMax,3))
	depth = np.zeros(blobbiesNumMax)
	verticalLocation = np.zeros(blobbiesNumMax)
	horizontalLocation = np.zeros(blobbiesNumMax)
	frequencies = np.zeros((blobbiesNumMax,3))
	gains = np.zeros((blobbiesNumMax,3))
	phases = np.zeros((blobbiesNumMax,3))
	subdivisions = np.zeros(blobbiesNumMax)


	# Middle row, on the left bottom
	blobbieIndex = 0;
	blobbieSize[blobbieIndex,:] = minSize * 2.0 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 15
	verticalLocation[blobbieIndex] = minVerticalLocation + 2
	horizontalLocation[blobbieIndex] = -10
	frequencies[blobbieIndex,:] = np.array([10, -15, -12])
	gains[blobbieIndex,:] = np.array([6.3, 9.6, 9.0])/1000
	phases[blobbieIndex,:] = np.array([1/3, -1/5-1/2, -1/5+1/2])*pi
	subdivisions[blobbieIndex] = 5;

  # Middle row, on the right bottom
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 2.0 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 19
	verticalLocation[blobbieIndex] = minVerticalLocation + 3
	horizontalLocation[blobbieIndex] = 9
	frequencies[blobbieIndex,:] = np.array([7, 8, -2])
	gains[blobbieIndex,:] = np.array([8, 2, 10.0])/1000
	phases[blobbieIndex,:] = np.array([1/3, -1/5-1/3, -1/5+1/3])*pi
	subdivisions[blobbieIndex] = 6;

	# Middle row, on the right bottom
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 1.0 * np.array([3.2, 5.0, 0.1])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 10
	verticalLocation[blobbieIndex] = minVerticalLocation -1
	horizontalLocation[blobbieIndex] = 10
	frequencies[blobbieIndex,:] = np.array([10, 1, 1])
	gains[blobbieIndex,:] = np.array([20, 30, 30.0])/1000
	phases[blobbieIndex,:] = np.array([0, 1/5, 1/2])*pi
	subdivisions[blobbieIndex] = 7;


	# Front row, on the middle bottom
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 1.5 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 7
	verticalLocation[blobbieIndex] = minVerticalLocation + 1
	horizontalLocation[blobbieIndex] = 0.5
	frequencies[blobbieIndex,:] = np.array([12, 12, -13])
	gains[blobbieIndex,:] = np.array([2, 10, 10.0])/1000
	phases[blobbieIndex,:] = np.array([1/3, 1/5-1/3, 1/5+1/3])*pi
	subdivisions[blobbieIndex] = 6;

	# Front row, on the right bottom
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 1.0 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 0
	verticalLocation[blobbieIndex] = minVerticalLocation + 0.5
	horizontalLocation[blobbieIndex] = -4
	frequencies[blobbieIndex,:] = np.array([12, 18, -12])
	gains[blobbieIndex,:] = np.array([20, 2, 10.0])/1000
	phases[blobbieIndex,:] = np.array([-1/3, 1/5-1/3, 1/5+1/3])*pi
	subdivisions[blobbieIndex] = 6;

	# Front row, on the right bottom
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 0.6 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth - 0.1
	verticalLocation[blobbieIndex] = minVerticalLocation - 0.1
	horizontalLocation[blobbieIndex] = 3
	frequencies[blobbieIndex,:] = np.array([-7, -18, 18])
	gains[blobbieIndex,:] = np.array([10, 14, 17.0])/1000
	phases[blobbieIndex,:] = np.array([1/2, 1/3, -1/3])*pi
	subdivisions[blobbieIndex] = 6;


	# Back row, on the right, vertically elongated
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 1.4 * np.array([1.0, 1.2, 2.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 27
	verticalLocation[blobbieIndex] = minVerticalLocation + 10
	horizontalLocation[blobbieIndex] = 3
	frequencies[blobbieIndex,:] = np.array([5, 22, -4])
	gains[blobbieIndex,:] = np.array([30, 10, 18.0])/1000
	phases[blobbieIndex,:] = np.array([1/2, 1/8, -1/3])*pi
	subdivisions[blobbieIndex] = 6;


	# Back row, on the  left, horizontally elongated
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 1.4 * np.array([1.7, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 26
	verticalLocation[blobbieIndex] = minVerticalLocation + 7
	horizontalLocation[blobbieIndex] = -6
	frequencies[blobbieIndex,:] = np.array([22, 1, 2])
	gains[blobbieIndex,:] = np.array([12, 14, 22.0])/1000
	phases[blobbieIndex,:] = np.array([1/2, -1/2, 1/6])*pi
	subdivisions[blobbieIndex] = 4;


	# Back row, on the upper left
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 2.0 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 23
	verticalLocation[blobbieIndex] = minVerticalLocation + 15
	horizontalLocation[blobbieIndex] = -10
	frequencies[blobbieIndex,:] = np.array([-2, 8, -5])
	gains[blobbieIndex,:] = np.array([13, 5, 8.0])/1000
	phases[blobbieIndex,:] = np.array([1/3, -1/5-1/3, -1/5+1/3])*pi
	subdivisions[blobbieIndex] = 5;

	# Back row, on the upper right
	blobbieIndex = blobbieIndex + 1;
	blobbieSize[blobbieIndex,:] = minSize * 2.0 * np.array([1.0, 1.0, 1.0])
	blobbieRotation[blobbieIndex,:] = np.array([0.0, 0.0, 0.0])
	depth[blobbieIndex] = minDepth + 28
	verticalLocation[blobbieIndex] = minVerticalLocation + 14
	horizontalLocation[blobbieIndex] = 9
	frequencies[blobbieIndex,:] = np.array([-12, 1,15])
	gains[blobbieIndex,:] = np.array([8, 20, 5.0])/1000
	phases[blobbieIndex,:] = np.array([1/3, 1/5-1/3, -1/5-1/3])*pi
	subdivisions[blobbieIndex] = 5;


	blobbiesNum = blobbieIndex
	for blobbieIndex in range(0, blobbiesNum+1):
		print('generating blobbie #{} of {}'.format(blobbieIndex+1, blobbiesNum+1))
		blobbieParams['name'] = 'blobbie{}'.format(blobbieIndex);
		blobbieParams['material'] = materials['blobbie{}'.format(blobbieIndex%10+1)];
		blobbieParams['scaling'] = Vector((blobbieSize[blobbieIndex,0], blobbieSize[blobbieIndex,1], blobbieSize[blobbieIndex,2]));
		blobbieParams['rotation'] = Vector((blobbieRotation[blobbieIndex,0], blobbieRotation[blobbieIndex,1], blobbieRotation[blobbieIndex,2]))
		blobbieParams['location'] = roomLocation + Vector((horizontalLocation[blobbieIndex], depth[blobbieIndex], verticalLocation[blobbieIndex]));
		blobbieParams['freqX'] = frequencies[blobbieIndex,0]
		blobbieParams['freqY'] = frequencies[blobbieIndex,1]
		blobbieParams['freqZ'] = frequencies[blobbieIndex,2]
		blobbieParams['gainX'] = gains[blobbieIndex,0]
		blobbieParams['gainY'] = gains[blobbieIndex,1]
		blobbieParams['gainZ'] = gains[blobbieIndex,2]
		blobbieParams['angleX'] = phases[blobbieIndex,0]
		blobbieParams['angleY'] = phases[blobbieIndex,1]
		blobbieParams['angleZ'] = phases[blobbieIndex,2]
		blobbieParams['subdivisions'] = subdivisions[blobbieIndex]
		blobbieObject = generateBlobbie(blobbieParams, scene);




  # ---------------------------------- CAMERA -------------------------------------
  # Define our cameraType
	nearClipDistance = 0.1;
	farClipDistance  = 1000;

	renderedImageWidthInCm  = 2*viewingDistance * tan(cameraHorizontalFOV/2*pi/180);
	renderedImageHeightInCm = renderedImageWidthInCm/cameraWidthToHeightAspectRatio;
	print('***-->rendered image width  :{}cm'.format(renderedImageWidthInCm));
	print('***-->rendered image height :{}cm'.format(renderedImageHeightInCm));


	params = {'clipRange'            : Vector((nearClipDistance, farClipDistance)),
              'fieldOfViewInDegrees' : cameraHorizontalFOV,   # horizontal FOV
              'widthToHeightAspectRatio' : cameraWidthToHeightAspectRatio,
              'pixelSamplesAlongWidth': 512,  # this only affects the Blender rendering, nothing else
              'drawSize'             : 2,     # camera wireframe size, this only affects the Blender rendering, nothing else
             };
	cameraType = scene.generateCameraType(params);

  # Add the camera
	cameraDistance = -viewingDistance;  # negative distance is towards us, positive is away from us
	cameraRotationInDeg = 0;
	theta  = cameraRotationInDeg * (pi/180)
	cameraHorizPosition = cameraDistance * sin(theta);
	cameraDepthPosition = cameraDistance * cos(theta);

	params = {  'name'          :'Camera',
                'cameraType'    : cameraType,
                'location'      : Vector((cameraHorizPosition, cameraDepthPosition, cameraElevation)),
                'lookAt'        : Vector((0, 0, cameraElevation)),
                'showName'      : True,
          };
	mainCamera = scene.addCameraObject(params);

  # ---------------------------------- LIGHTS -------------------------------------
  # Generate the area lamp model
	params = {'name'  : 'areaLampModel',
              'color' : Vector((1,1,1)),
              'fallOffDistance': 120,
              'width1': 2, # sceneParams['areaLampSize'],
              'width2': 2, # sceneParams['areaLampSize']
              };
	brightLight = scene.generateAreaLampType(params);

	params = {'name'  : 'areaLampModel',
              'color' : Vector((1,1,1)),
              'fallOffDistance': 120,
              'width1': 1, # sceneParams['areaLampSize'],
              'width2': 2, # sceneParams['areaLampSize']
              }
	brightLightTall = scene.generateAreaLampType(params);

	lightElevation = roomHeight-10;


  # - Add the LEFTSIDE area lamp (REAR)
	lightDepthPosition = -22;
	lightHorizPosition = -20;

	leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation           # elevation (z-coord)
                           ));

	leftAreaLampLooksAt = Vector((
                             -4,      # horizontal position (x-coord)
                             0,      # depth position (y-coord)
                             lightElevation/3       # elevation (z-coord)
                           ));


	params = {'name'     : 'sideLeftAreaLamp',
              'model'    : brightLightTall,
              'showName' : True,
              'location' : leftAreaLampPosition,
              'lookAt'   : leftAreaLampLooksAt
          };
	sideLeftAreaLamp = scene.addLampObject(params);

  	# - Add the RIGHTSIDE area lamp (REAR)
	lightHorizPosition = 20;

	rightAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation/3           # elevation (z-coord)
                           ));

	rightAreaLampLooksAt = Vector((
                             4,      # horizontal position (x-coord)
                             0,      # depth position (y-coord)
                             lightElevation       # elevation (z-coord)
                           ));


	params = {'name'     : 'sideRightAreaLamp',
              'model'    : brightLightTall,
              'showName' : True,
              'location' : rightAreaLampPosition,
              'lookAt'   : rightAreaLampLooksAt
          };
	sideRightAreaLamp = scene.addLampObject(params);


  # - Add the FRONT left area lamp
	lightDepthPosition = -34;
	lightHorizPosition = -12;

	leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	leftAreaLampLooksAt = Vector((
                             -lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'frontLeftAreaLamp',
              'model'    : brightLight,
              'showName' : True,
              'location' : leftAreaLampPosition,
              'lookAt'   : leftAreaLampLooksAt
          };
	frontLeftAreaLamp = scene.addLampObject(params);


	# - Add the FRONT right area lamp
	rightAreaLampPosition = Vector((
                            -lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation           # elevation (z-coord)
                           ));
	rightAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'frontRightAreaLamp',
              'model'    : brightLight,
              'showName' : True,
              'location' : rightAreaLampPosition,
              'lookAt'   : rightAreaLampLooksAt
          };
	frontRightAreaLamp = scene.addLampObject(params);



	# - Add the REAR left area lamp
	lightDepthPosition = lightDepthPosition+10;
	lightHorizPosition = -5;


	leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	leftAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'rearLeftAreaLamp',
              'model'    : brightLight,
              'showName' : True,
              'location' : leftAreaLampPosition,
              'lookAt'   : leftAreaLampLooksAt
          };
	rearLeftAreaLamp = scene.addLampObject(params);


	# - Add the REAR right area lamp
	rightAreaLampPosition = Vector((
                            -lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	rightAreaLampLooksAt = Vector((
                             -lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'rearRightAreaLamp',
              'model'    : brightLight,
              'showName' : True,
              'location' : rightAreaLampPosition,
              'lookAt'   : rightAreaLampLooksAt
          };
	rearRightAreaLamp = scene.addLampObject(params);

  # ---------------------------------- ACTION ! -------------------------------------

  # Finally, export collada file
	scene.exportToColladaFile(sceneParams['exportsDirectory']);

	print('All done !')




# ------------------------------- main() ----------------------------
import os
import sys
import argparse
import numpy as np
from math import pi
from mathutils import Vector

baseDir = '/Users/nicolas/Documents/MATLAB/toolboxes'
toolboxDir='{}/RenderToolbox4/Utilities/BlenderPython'.format(baseDir)
print(toolboxDir)
homeDir = '/Users/nicolas/Documents/1_developer/1_python/RTprojects/rendertoolboxprojects/5.Blobbierium'
exportsDir = '{}/2.ColladaExports'.format(homeDir)


blobbieParams = {
				'subdivisions' : 4
	};

params = {'toolboxDirectory' : toolboxDir,
		  'exportsDirectory' : exportsDir,
				 'sceneName' : 'BlobbieriumDifferentSizesAndShapes',
		 };

generateScene(params, blobbieParams);
