#!/bin/sh

# directory defininitions
baseDirectory="/Volumes/SDXC_64GB"
baseDirectory="/Users/Shared"
toolboxDirectory="$baseDirectory/Matlab/Toolboxes/RenderToolbox3/Utilities/BlenderPython"

homeDirectory="/Volumes/SDXC_64GB"
homeDirectory="/Users/nicolas/Documents/1.Code/2.RenderToolboxImagery/1.Projects"
homeDirectory="/Users/nicolas/Documents/1.Code/0.BitBucketPrivateRepos/rendertoolbox3tools/Code"

rootDirectory="$homeDirectory/1.BlobbiesMaterialColor"
#blendFile="${rootDirectory}/1.Python/generateColorMaterialVaryingBlobbies.py"
blendFile="${rootDirectory}/1.Python/generateColorMaterialVaryingBlobbiesInSunRoom.py"
exportsDirectory="${rootDirectory}/2.ColladaExports/HighDynamicRange"



lowGainX2="--gainX 0.009"
lowGainY2="--gainY 0.008"
lowGainZ2="--gainZ 0.015"

lowGainX="--gainX 0.0108"
lowGainY="--gainY 0.0096"
lowGainZ="--gainZ 0.0180"


highGainX="--gainX 0.018"
highGainY="--gainY 0.016 "
highGainZ="--gainZ 0.030"

veryLowFreqX="--frequencyX 3"
veryLowFreqY="--frequencyY 4.5"
veryLowFreqZ="--frequencyZ -3"

lowFreqX="--frequencyX 4.5"
lowFreqY="--frequencyY 5.2"
lowFreqZ="--frequencyZ -4.5"

mediumFreqX="--frequencyX 6"
mediumFreqY="--frequencyY 9"
mediumFreqZ="--frequencyZ -6"

highFreqX="--frequencyX 9"
highFreqY="--frequencyY 13"
highFreqZ="--frequencyZ -9"

veryHighFreqX="--frequencyX 12"
veryHighFreqY="--frequencyY 18"
veryHighFreqZ="--frequencyZ -12"

ultraHighFreqX="--frequencyX 15"
ultraHighFreqY="--frequencyY 20"
ultraHighFreqZ="--frequencyZ -15"



# assemble command

/Applications/Blender/blender.app/Contents/MacOS/blender \
--python $blendFile -- \
--MacBethColorCheckerInsteadOfBlobbie False \
--shadowCastingPlane True \
--objectName "BlobbieObject" \
--sceneName "Blobbie8SubsVeryLowFreqMultipleBlobbiesOpenRoof" \
--blobbieSubdivisions 8 \
$veryLowFreqX \
$veryLowFreqY \
$veryLowFreqZ \
$lowGainX2 \
$lowGainY2 \
$lowGainZ2 \
--addTinyBlobbieInShadow True \
--addPrimaryMacBethColorChecker True \
--primaryMacBethColorCheckerCheckSize 4.5 \
--primaryMacBethColorCheckerXrotation 12.5 \
--primaryMacBethColorCheckerYrotation 0.0 \
--primaryMacBethColorCheckerZrotation 90.0 \
--primaryMacBethColorCheckerXpos 24 \
--primaryMacBethColorCheckerYpos 18 \
--primaryMacBethColorCheckerZpos 4.8 \
--addSecondaryMacBethColorChecker True \
--secondaryMacBethColorCheckerCheckSize 5.5 \
--secondaryMacBethColorCheckerXrotation 67.0 \
--secondaryMacBethColorCheckerYrotation 00.0 \
--secondaryMacBethColorCheckerZrotation 0.0 \
--secondaryMacBethColorCheckerXpos -45 \
--secondaryMacBethColorCheckerYpos 83.79 \
--secondaryMacBethColorCheckerZpos 13.6 \
--toolboxDirectory $toolboxDirectory \
--exportsDirectory $exportsDirectory



#/Applications/Blender/blender.app/Contents/MacOS/blender \
#--python $blendFile -- \
#--MacBethColorCheckerInsteadOfBlobbie False \
#--shadowCastingPlane True \
#--objectName "BlobbieObject" \
#--sceneName "Blobbie8SubsHighFreqMultipleBlobbiesOpenRoof" \
#--blobbieSubdivisions 8 \
#$highFreqX \
#$highFreqY \
#$highFreqZ \
#$lowGainX \
#$lowGainY \
#$lowGainZ \
#--addTinyBlobbieInShadow True \
#--addPrimaryMacBethColorChecker True \
#--primaryMacBethColorCheckerCheckSize 4.5 \
#--primaryMacBethColorCheckerXrotation 12.5 \
#--primaryMacBethColorCheckerYrotation 0.0 \
#--primaryMacBethColorCheckerZrotation 90.0 \
#--primaryMacBethColorCheckerXpos 24 \
#--primaryMacBethColorCheckerYpos 18 \
#--primaryMacBethColorCheckerZpos 4.8 \
#--addSecondaryMacBethColorChecker True \
#--secondaryMacBethColorCheckerCheckSize 5.5 \
#--secondaryMacBethColorCheckerXrotation 67.0 \
#--secondaryMacBethColorCheckerYrotation 00.0 \
#--secondaryMacBethColorCheckerZrotation 0.0 \
#--secondaryMacBethColorCheckerXpos -45 \
#--secondaryMacBethColorCheckerYpos 83.79 \
#--secondaryMacBethColorCheckerZpos 13.6 \
#--toolboxDirectory $toolboxDirectory \
#--exportsDirectory $exportsDirectory




