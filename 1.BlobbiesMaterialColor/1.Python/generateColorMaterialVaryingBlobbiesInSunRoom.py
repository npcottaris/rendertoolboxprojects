# Script to generate color-material varying blobbies
# 
# 12/31/2014  npc  Wrote it. 
#  5/13/2015  npc  Added option to insert the MacBethColorChecker instead of the Blobbie
#  6/24/2014  npc  Added option to insert additional MacBethColorCheckers, modularized colorchecker generation

def generateMaterials(scene, MacBethColorCheckerInsteadOfBlobbie):
    from mathutils import Vector

    materials = {};

    # Generate the macbeth background material
    params = {'name'              : 'MacBethColorCheckerBackground',
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : 0.5,
                'diffuse_color'     : Vector((0.0, 0.0, 0.0)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((0.0, 0.0, 0.0)),
                'alpha'             : 1.0
              };
    materials['MacBethColorCheckerBackground'] = scene.generateMaterialType(params);

    # Generate the 24 chip colors
    for k in range(24):
      # Generate the different macbeth materials
      chipName = 'mccBabel-{}'.format(k+1);
      params = {  'name'              : chipName,
                    'diffuse_shader'    : 'LAMBERT',
                    'diffuse_intensity' : 0.5,
                    'diffuse_color'     : Vector((1-k/23, 0.4, k/23)),
                    'specular_shader'   : 'WARDISO',
                    'specular_intensity': 0.0,
                    'specular_color'    : Vector((0.0, 0.0, 0.0)),
                    'alpha'             : 1.0
              }; 
      materials[chipName] = scene.generateMaterialType(params);


    # Generate the blobbie material
    params = {  'name'              : 'blobbieMaterial',
                  'diffuse_shader'    : 'LAMBERT',
                  'diffuse_intensity' : 0.5,
                  'diffuse_color'     : Vector((0.7, 0.0, 0.1)),
                  'specular_shader'   : 'WARDISO',
                  'specular_intensity': 0.0,
                  'specular_color'    : Vector((1.0, 1.0, 1.0)),
                  'alpha'             : 1.0
              }; 
    materials['blobbie'] = scene.generateMaterialType(params);


    # -the pedestal material
    params = { 'name'              : 'pedestalMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 1.0, 0.7)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.1,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    materials['pedestal'] = scene.generateMaterialType(params);

    # -the shadowing plane bottom material
    params = { 'name'              : 'shadowingBottomPlaneMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.2, 0.2, 0.2)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.1,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    materials['shadowingBottomPlane'] = scene.generateMaterialType(params);

    # -the shadowing plane top material
    params = { 'name'              : 'shadowingTopPlaneMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.9, 0.9, 0.9)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.1,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    materials['shadowingTopPlane'] = scene.generateMaterialType(params);


    # -the room material
    params = { 'name'              : 'roomMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    materials['room'] = scene.generateMaterialType(params);

    # -the backwall material
    params = { 'name'              : 'backWallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    materials['backWall'] = scene.generateMaterialType(params);

    # -the dark check material
    params = { 'name'              : 'darkCheckMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.1,
               'diffuse_color'     : Vector((0.5, 0.5, 0.5)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((0.5, 0.5, 0.5)),
               'alpha'             : 1.0
             }; 
    materials['darkCheck'] = scene.generateMaterialType(params);

    # -the light check material
    params = { 'name'              : 'lightCheckMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.7,
               'diffuse_color'     : Vector((0.55, 0.55, 0.40)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((0.5, 0.5, 0.5)),
               'alpha'             : 1.0
             }; 
    materials['lightCheck'] = scene.generateMaterialType(params);

    return(materials);



def generateBlobbie(blobbieParams, scene):
    # Basic imports
    import sys
    import imp
    import bpy
    from math import floor, cos, sin, sqrt, atan2, pow, pi
    from mathutils import Vector

    # Start with a sphere
    params = blobbieParams;
    params['flipNormal'] = False;

    blobbieObject = scene.addSphere(params);
    
    # Modify vertices to introduce bumps
    aX = cos(params['angleX']) * params['freqX'];
    bX = sin(params['angleX']) * params['freqX'];
    aY = cos(params['angleY']) * params['freqY'];
    bY = sin(params['angleY']) * params['freqY'];
    aZ = cos(params['angleZ']) * params['freqZ'];
    bZ = sin(params['angleZ']) * params['freqZ'];

    for f in blobbieObject.data.polygons:
        for idx in f.vertices:
          oldX = blobbieObject.data.vertices[idx].co.x;
          oldY = blobbieObject.data.vertices[idx].co.y;
          oldZ = blobbieObject.data.vertices[idx].co.z;
          blobbieObject.data.vertices[idx].co.x = oldX + params['gainX'] * sin(aX*oldY + bX*oldZ);
          blobbieObject.data.vertices[idx].co.y = oldY + params['gainY'] * sin(aY*oldX + bY*oldZ);
          blobbieObject.data.vertices[idx].co.z = oldZ + params['gainZ'] * sin(aZ*oldX + bZ*oldY);

    return(blobbieObject);


def generateMacBethColorChecker(colorCheckerParams, materials, scene):
    # Basic imports
    import sys
    import imp
    import bpy
    from math import floor, cos, sin, sqrt, atan2, pow, pi
    from mathutils import Vector

    # Start with the background
    MacBethColorCheckerCheckSize = colorCheckerParams['checkSize'];
    MacBethColorCheckerBorder = colorCheckerParams['checkSize']/12;
    MacBethColorCheckerWidth  = MacBethColorCheckerCheckSize*6 + MacBethColorCheckerBorder*7;
    MacBethColorCheckerHeight = MacBethColorCheckerCheckSize*4 + MacBethColorCheckerBorder*5;

    params = {'name'         : colorCheckerParams['name'],
                'scaling'      : Vector((MacBethColorCheckerWidth/2, MacBethColorCheckerHeight/2, 0.1)),
                'rotation'     : Vector((0, 0, 0)),
                'location'     : Vector((0,0,0)),
                'material'     : materials['MacBethColorCheckerBackground'],
                'flipNormal'   : False,
              };
    MacBethColorCheckerObject = scene.addCube(params);

    chipMaterialName = 'mccBabel-{}'.format(1);
    chipParams = {'name'    : '',
                    'scaling' : Vector((MacBethColorCheckerCheckSize/2, MacBethColorCheckerCheckSize/2, 0.2)),
                    'rotation': Vector((0,0,0)), 
                    'location': Vector((0,0,0)),
                    'material': materials[chipMaterialName]
                  };

    # List with all the chip objects
    chipObjects = [];
    for iy in range(0,4):
        for ix in range(0,6):
          chipMaterialName = 'mccBabel-{}'.format((3-iy)+ix*4+1); 
          print('chip at (x,y) = ({},{}) with color:{}'.format(ix,iy,chipMaterialName))
          chipParams['name']     = 'chipAt({0:1d},{1:2d})'.format(ix,iy);
          chipParams['location'] =  Vector((-MacBethColorCheckerWidth/2.0  + MacBethColorCheckerCheckSize/2.0 + ix*(MacBethColorCheckerCheckSize+MacBethColorCheckerBorder) + MacBethColorCheckerBorder, 
                                            -MacBethColorCheckerHeight/2.0 + MacBethColorCheckerCheckSize/2.0 + iy*(MacBethColorCheckerCheckSize+MacBethColorCheckerBorder) + MacBethColorCheckerBorder, 
                                            0.02));
          chipParams['material'] =  materials[chipMaterialName];
          theChip = scene.addCube(chipParams);
          chipObjects.append(theChip);


    # Make the MacBethColorCheckerObject the parent of all chipObjects, so they rotate/transform together
    # Step 1. Make sure all objects are deselected because the active objet will be the parent of all selected objects
    bpy.ops.object.select_all(action='DESELECT'); # deselect all objects
      
    # Step 2. Select the object to be the parent
    MacBethColorCheckerObject.select = True;
      
    # Step 3. Select all the chip objects to be parented
    for aChipObject in chipObjects:
      aChipObject.select = True;  
        
    # Step 4. Make the pedestal the active object
    bpy.context.scene.objects.active = MacBethColorCheckerObject;    #the active object will be the parent of all selected objects

    # Step 5. Do the parenting
    bpy.ops.object.parent_set()

    # Now move the MacBethColorCheckerObject
    MacBethColorCheckerObject.location = colorCheckerParams['location'];
    # and rotate it
    MacBethColorCheckerObject.rotation_euler = (colorCheckerParams['rotation']) / 180.0 * pi;

    # return the color-checker object
    return(MacBethColorCheckerObject);



def generateScene(sceneParams):
    # Basic imports
    import sys
    import imp
    import bpy
    from math import floor, cos, sin, sqrt, atan2, pow, pi
    from mathutils import Vector

    # ------------------------------ SCENE MANAGER SETUP -----------------------------

    print('ToolboxDir = {}'.format(sceneParams.toolboxDirectory));
    # Append the path to my custom Python scene toolbox to the Blender path
    sys.path.append(sceneParams.toolboxDirectory);

    # Import the custom scene toolbox module
    import SceneUtilsV1;
    imp.reload(SceneUtilsV1);
 
    # Initialize a sceneManager
    params = { 'name'               : sceneParams.sceneName,     # name of new scene
               'erasePreviousScene' : True,                      # erase old scene
               'sceneWidthInPixels' : 640,                       # pixels along the horizontal-dimension
               'sceneHeightInPixels': 480,                       # pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0,                       # arbitrary units
               'sceneGridSpacing'   : 10.0,                      # set the spacing between grid lines to 10
               'sceneGridLinesNum'  : 20,                        # display 20 grid lines
              };
    scene = SceneUtilsV1.sceneManager(params);


    # ---------------------------------- GEOMETRIES -----------------------------------

    # Define the room, pedestal and checkerboard geometry
    # the enclosing room
    roomLocation = Vector((0,0,0));
    roomWidth    = 180;
    roomDepth    = 180;
    roomHeight   = 100;

    # the pedestal on which the blobbie is displayed
    pedestalWidth     = 20.0;
    pedestalHeight    = 2.0;

    # the checkerboard underneath the pedestal
    boardThickness    = 2.5;
    boardHalfWidth    = roomWidth/2;
    tilesAlongEachDim = 9;  # this must be odd number
    boardIsDimpled    = False;


    # ---------------------------------- MATERIALS -----------------------------------
    # Generate the materials
    materials = generateMaterials(scene, sceneParams.MacBethColorCheckerInsteadOfBlobbie);


    if sceneParams.shadowCastingPlane:
      # ----------------------------- Shadow casting plane -------------------------------

      print('Adding shadow casting plane assembly');

      # add bottom plane (dark)
      params = { 'name'    : 'shadowingBottomPlane',
                 'scaling' : Vector((pedestalWidth*1.6, pedestalWidth*1.8, 2*pedestalHeight/10)),
                 'rotation': Vector((0,32/180*pi,0)), 
                 'location': Vector((29, 35, 20.7)),
                 'material': materials['shadowingBottomPlane'],
      }; 
      shadowingBottomPlane  = scene.addCube(params);

      # add top plane (light)
      shadowingPlaneParams = { 'name'    : 'shadowingTopPlane',
                 'scaling' : Vector((pedestalWidth*1.6, pedestalWidth*1.8, 8*pedestalHeight/10)),
                 'rotation': Vector((0,32/180*pi,0)), 
                 'location': Vector((30.074, 35, 22.331)),
                 'material': materials['shadowingTopPlane'],
      }; 
      shadowingTopPlane  = scene.addCube(shadowingPlaneParams);



      # Modify the checks if the board is dimpled
      boardIsDimpled = True;
      if boardIsDimpled:
        sphereParams = { 'name'     : 'theSphere',
                         'scaling'  : Vector((24.0, 24.0, 3.0)), 
                         'location' : Vector((0,0,0)),
                         'material' : materials['shadowingTopPlane'],
               };
        indentation = 1.0;
        theSphere          = scene.addSphere(sphereParams);
        theSphere.rotation_euler = shadowingPlaneParams['rotation'];
        theSphere.location = shadowingPlaneParams['location'] + Vector((0,0, 2-indentation));
        scene.boreOut(shadowingTopPlane, theSphere, True);




    if sceneParams.addPrimaryMacBethColorChecker:
      # ------------------- Primary Macbeth color checker -------------------------------
      print('Generating primary color checkers');
      colorCheckerParams = {'name'       : 'primaryColorCheckerTop',
                            'checkSize'  : sceneParams.primaryMacBethColorCheckerCheckSize,
                            'rotation'   : Vector((float(sceneParams.primaryMacBethColorCheckerXrotation), float(sceneParams.primaryMacBethColorCheckerYrotation), float(sceneParams.primaryMacBethColorCheckerZrotation))), 
                            'location'   : Vector((sceneParams.primaryMacBethColorCheckerXpos, sceneParams.primaryMacBethColorCheckerYpos, sceneParams.primaryMacBethColorCheckerZpos)) + roomLocation,
      };

      primaryMacBethColorCheckerTop = generateMacBethColorChecker(colorCheckerParams, materials, scene);

      colorCheckerParams = {'name'       : 'primaryColorCheckerBottom',
                            'checkSize'  : sceneParams.primaryMacBethColorCheckerCheckSize/2,
                            'rotation'   : Vector((0, 0, float(sceneParams.primaryMacBethColorCheckerZrotation)+180)), 
                            'location'   : Vector((sceneParams.primaryMacBethColorCheckerXpos+2.0, sceneParams.primaryMacBethColorCheckerYpos-15, sceneParams.primaryMacBethColorCheckerZpos-2)) + roomLocation,
      };

      primaryMacBethColorCheckerBottom = generateMacBethColorChecker(colorCheckerParams, materials, scene);



    if sceneParams.addSecondaryMacBethColorChecker:
      # ------------------- Secondary Macbeth color checker -------------------------------
      print('Generating secondary color checker');
      colorCheckerParams = {'name'       : 'secondaryColorChecker',
                            'checkSize'  : sceneParams.secondaryMacBethColorCheckerCheckSize,
                            'rotation'   : Vector((float(sceneParams.secondaryMacBethColorCheckerXrotation), float(sceneParams.secondaryMacBethColorCheckerYrotation), float(sceneParams.secondaryMacBethColorCheckerZrotation))), 
                            'location'   : Vector((sceneParams.secondaryMacBethColorCheckerXpos, sceneParams.secondaryMacBethColorCheckerYpos, sceneParams.secondaryMacBethColorCheckerZpos)) + roomLocation,
      };

      secondaryMacBethColorChecker = generateMacBethColorChecker(colorCheckerParams, materials, scene);


    if sceneParams.MacBethColorCheckerInsteadOfBlobbie:
      # -------------------------------- MacBeth Color Checker --------------------------
      # Generate the Macbeth color checker
      print('Generating MacBeth color checker ...')

      colorCheckerParams = {'name'       : sceneParams.objectName,
                            'checkSize'  : 6,
                            'rotation'   : Vector((float(sceneParams.primaryMacBethColorCheckerXrotation), float(sceneParams.primaryMacBethColorCheckerYrotation), float(sceneParams.primaryMacBethColorCheckerZrotation))), 
                            'location'   : Vector((sceneParams.primaryMacBethColorCheckerXpos, sceneParams.primaryMacBethColorCheckerYpos, sceneParams.primaryMacBethColorCheckerZpos)) + roomLocation,
      };

      MacBethColorChecker = generateMacBethColorChecker(colorCheckerParams, materials, scene);
      

    else:
      # ---------------------------------- MAIN BLOBBIE -------------------------------------
      
      print('Generating Blobbie. This may take a while ...')
      blobbieParams = { 'name'         : sceneParams.objectName,
                        'scaling'      : Vector((16, 16, 16)),
                        'rotation'     : Vector((0, 0, 0)),
                        'location'     : Vector((0, 20, 18+pedestalHeight)) + roomLocation,
                        'subdivisions' : sceneParams.blobbieSubdivisions,
                        'material'     : materials['blobbie'],
                        'angleX'       : sceneParams.angleX,
                        'angleY'       : sceneParams.angleY,
                        'angleZ'       : sceneParams.angleZ,
                        'freqX'        : sceneParams.frequencyX,
                        'freqY'        : sceneParams.frequencyY,
                        'freqZ'        : sceneParams.frequencyZ,
                        'gainX'        : sceneParams.gainX,
                        'gainY'        : sceneParams.gainY,
                        'gainZ'        : sceneParams.gainZ,
      };

      blobbieObject = generateBlobbie(blobbieParams, scene);


    if sceneParams.addTinyBlobbieInShadow:
      # ---------------------------------- TINY BLOBBIE -------------------------------------
      
      print('Generating tiny Blobbie. This may take a while ...')
      tinyBlobbieParams = blobbieParams;

      # below, settings for putting the Blobbie under the colorchecker
      #tinyBlobbieParams['subdivisions'] = tinyBlobbieParams['subdivisions']-1;  # less subdivisions, as this blobbie is so tiny
      #tinyBlobbieParams['scaling']  = Vector((1.5, 1.5, 1.5));
      #tinyBlobbieParams['location'] = Vector((21.5, 3.95+0.4, 3.84)) + roomLocation;
      #tinyBlobbieObject = generateBlobbie(tinyBlobbieParams, scene);
      tinyBlobbieParams['gainX'] = blobbieParams['gainX']*0.8; 
      tinyBlobbieParams['gainY'] = blobbieParams['gainY']*0.8; 
      tinyBlobbieParams['gainZ'] = blobbieParams['gainZ']*0.8; 
      tinyBlobbieParams['scaling']  = Vector((8.0, 8.0, 8.0));
      tinyBlobbieParams['location'] = Vector((-38, 35.0, 9.50)) + roomLocation;
      tinyBlobbieObject = generateBlobbie(tinyBlobbieParams, scene);



    # ------------------------------------ PEDESTAL -------------------------------------
    print('Generating pedestal ...')
    # add pedestal
    params = { 'name'    : 'pedestal',
               'scaling' : Vector((pedestalWidth, pedestalWidth, pedestalHeight)),
               'rotation': Vector((0,0,0)), 
               'location': Vector((0, 20, pedestalHeight/2+boardThickness)),
               'material': materials['pedestal'],
    }; 
    pedestal  = scene.addCube(params);

    # If we are generating a Bloobie make the pedestal the parent of the blobbie
    if sceneParams.MacBethColorCheckerInsteadOfBlobbie == False:
      # Make the pedestal the parent of the blobbieObject, so they rotate/transform together
      # Step 1. Make sure all objects are deselected because the active objet will be the parent of all selected objects
      bpy.ops.object.select_all(action='DESELECT'); # deselect all objects
      
      # Step 2. Select the object to be the parent
      pedestal.select = True;
      
      # Step 3. Select the object to be parented
      if sceneParams.MacBethColorCheckerInsteadOfBlobbie:
        MacBethColorCheckerObject.select = True;
      else:
        blobbieObject.select = True;  
        

      # Step 4. Make the pedestal the active object
      bpy.context.scene.objects.active = pedestal;    #the active object will be the parent of all selected objects

      # Step 5. Do the parenting
      bpy.ops.object.parent_set()



    # ---------------------------------- CHECKERBOARD -----------------------------------
    print('Generating checkerboard ...');
    # compute checker size
    N                 = floor((tilesAlongEachDim-1)/2);
    deltaX            = boardHalfWidth/N;
    deltaY            = deltaX;

    checkBoardMaterialsList = [materials['lightCheck'], materials['darkCheck']];
    tileParams = {'name'    : '',
                  'scaling' : Vector((deltaX/2, deltaY/2, boardThickness/2)),
                  'rotation': Vector((0,0,0)), 
                  'location': Vector((0,0,0)),
                  'material': checkBoardMaterialsList[0]
                  };
    # define sphere for modifying tiles if the board is dimpled
    tileIsDimpled = False;
    if tileIsDimpled:
        sphereParams = { 'name'     : 'theSphere',
                         'scaling'  : Vector((1.0, 1.0, 1.0))*deltaX/2, 
                         'location' : Vector((0,0,0)),
                         'material' : checkBoardMaterialsList[0],
               };
        indentation = 0.09;

    for ix in list(range(-N,N+1)):
       for iy in list(range(-N,N+1)): 
            tileParams['name']     = 'floorTileAt({0:1d},{1:2d})'.format(ix,iy);
            tileParams['location'] =  Vector((ix*deltaX, iy*deltaY, boardThickness*0.5));
            tileParams['material'] =  checkBoardMaterialsList[(ix+iy)%2];
            theTile = scene.addCube(tileParams);
            if tileIsDimpled:
                theSphere          = scene.addSphere(sphereParams);
                theSphere.location = theTile.location + Vector((0,0,deltaX/2*(1-indentation)));
                scene.boreOut(theTile, theSphere, True);




    # ----------------------------- ENCLOSING ROOM ----------------------------------
    print('Generating enclosing room...')
    # Add the enclosing room
    roomLocation = Vector((0,0,0));


    params = {  'floorName'             : 'floor',
                'backWallName'          : 'backWall',
                'frontWallName'         : 'frontWall',
                'leftWallName'          : 'leftWall',
                'rightWallName'         : 'rightWall',
                'ceilingName'           : 'ceiling',
                'floorMaterialType'     : materials['room'],
                'backWallMaterialType'  : materials['backWall'],
                'frontWallMaterialType' : materials['room'],
                'leftWallMaterialType'  : materials['room'],
                'rightWallMaterialType' : materials['room'],
                'ceilingMaterialType'   : materials['room'],
                'roomWidth'             : roomWidth,
                'roomDepth'             : roomDepth,
                'roomHeight'            : roomHeight,
                'roomLocation'          : roomLocation,
                'wallThickness'         : 0.5, 
              }
    roomBox = scene.addRoom(params);


    # Modify the ceiling
    ceilingHasOpenings = True;
    if ceilingHasOpenings:
      Nx = 1;
      Ny = 1;
      separation = 37;
      windowSize = 12;

      # The left window
      windowParams = { 'name'     : 'theLeftWindow',
                         'scaling'  : Vector((50, windowSize*2, windowSize*2)), 
                         'location' : Vector((-90, 50, 40)),
                         'rotation' : Vector((0,0,0)),
                         'material' : materials['shadowingTopPlane'],
               };
      theWindow = scene.addCube(windowParams);
      scene.boreOut(roomBox['leftWallPlane'], theWindow, True);

      for ix in list(range(-Nx,Nx+1)):
       for iy in list(range(-Ny,Ny+1)):
          windowParams = { 'name'     : 'theCeilingWindow',
                         'scaling'  : Vector((windowSize, windowSize, 50)), 
                         'location' : Vector((separation*ix,separation*iy,100)),
                         'rotation' : Vector((0,0,0)),
                         'material' : materials['shadowingTopPlane'],
               };
          theWindow = scene.addCube(windowParams);
          scene.boreOut(roomBox['ceilingPlane'], theWindow, True);

     




    # ---------------------------------- CAMERA -------------------------------------
  	# Define our camera
    nearClipDistance = 0.1;
    farClipDistance  = 300;
    params = {'clipRange'            : Vector((nearClipDistance ,  farClipDistance)),
              'fieldOfViewInDegrees' : 42,   # horizontal FOV
              'drawSize'             : 2,    # camera wireframe size
             };
    cameraType = scene.generateCameraType(params);
 
    # Add the camera
    cameraElevation = 60;
    cameraDistance = -89;  # negative distance is towards us, positive is away from us
    cameraRotationInDeg = 0;
    theta  = cameraRotationInDeg * (pi/180)
    cameraHorizPosition = cameraDistance * sin(theta);
    cameraDepthPosition = cameraDistance * cos(theta);

    params = {  'name'          :'Camera', 
                'cameraType'    : cameraType,
                'location'      : Vector((cameraHorizPosition, cameraDepthPosition, cameraElevation)),     
                'lookAt'        : Vector((0, 0, 25)),
                'showName'      : True,
          };          
    mainCamera = scene.addCameraObject(params);


    # ---------------------------------- LIGHTS -------------------------------------
    # Generate the area lamp model
    params = {'name'  : 'areaLampModel', 
              'color' : Vector((1,1,1)), 
              'fallOffDistance': 120,
              'width1': 50, # sceneParams['areaLampSize'],
              'width2': 50, # sceneParams['areaLampSize']
              }
    brightLight = scene.generateAreaLampType(params);


	  # - Add the FRONT left area lamp
    lightElevation = 85;
    lightDistance = -25;   # negative distance is towards us, positive is away from us
    lightRotationInDeg = 90;
    theta  = lightRotationInDeg * (pi/180);
    lightHorizPosition = lightDistance * sin(theta) - 300;
    lightDepthPosition = lightDistance * cos(theta) + 40; 

    leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
    leftAreaLampLooksAt = Vector((
                             0, #lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

    params = {'name'     : 'frontLeftAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : leftAreaLampPosition, 
              'lookAt'   : leftAreaLampLooksAt
          };
    frontLeftAreaLamp = scene.addLampObject(params);


    lightRotationInDeg = 90+180;
    theta  = lightRotationInDeg * (pi/180);
    lightHorizPosition = lightDistance * sin(theta) + 300;
    lightDepthPosition = lightDistance * cos(theta) + 40; 

    rightAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation           # elevation (z-coord)
                           ));
    rightAreaLampLooksAt = Vector((
                             0, #lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

    params = {'name'     : 'frontRightAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : rightAreaLampPosition, 
              'lookAt'   : rightAreaLampLooksAt
          };
    frontRightAreaLamp = scene.addLampObject(params);



    lightHorizPosition = 0;
    lightDepthPosition = 0; 

    outsideAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            200           # elevation (z-coord)
                           ));
    outsideAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

    params = {'name'     : 'outsideAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : outsideAreaLampPosition, 
              'lookAt'   : outsideAreaLampLooksAt
          };
    outsideAreaLamp = scene.addLampObject(params)




    # ---------------------------------- ACTION ! -------------------------------------

    # Finally, export collada file
    scene.exportToColladaFile(sceneParams.exportsDirectory);

    print('All done !')



def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

# ----------------------------------------- main() -----------------------------------

import os
import sys
import argparse
from math import pi

# Parse args from the command line

# Get Python args from the command line.
# These should come after Blender arguments, separated with ' -- '
# For example, blender --background --python IsolatedBlobbie.py -- --sceneName myScene --objectName thing1
commandLine = ' '.join(sys.argv)
commandParts = commandLine.split(' -- ')
pythonArgs = '';
if len(commandParts) > 1:
    pythonArgs = commandParts[1].split(' ')

print('number of arguments = {}'.format(len(pythonArgs)));
# Parse Blobbie arguments
parser = argparse.ArgumentParser()
parser.register('type','bool',str2bool) # add type keyword to registries
parser.add_argument('--sceneName', default='BlobbieScene');
parser.add_argument('--objectName', default='BlobbieObject');
parser.add_argument('--exportsDirectory', default=os.getcwd());
parser.add_argument('--toolboxDirectory', default=os.getcwd());
parser.add_argument('--blobbieSubdivisions', type=int, default=4);
parser.add_argument('--angleX', type=float, default=pi/3);
parser.add_argument('--angleY', type=float, default=-pi/5-pi/2);
parser.add_argument('--angleZ', type=float, default=-pi/6+pi/2);
parser.add_argument('--frequencyX', type=float, default=9);
parser.add_argument('--frequencyY', type=float, default=11);
parser.add_argument('--frequencyZ', type=float, default=-8);
parser.add_argument('--gainX', type=float, default= 9/1000);
parser.add_argument('--gainY', type=float, default= 8/1000);
parser.add_argument('--gainZ', type=float, default= 15/1000);
# whether to add a shadow casting plane to increase the contrast
parser.add_argument('--shadowCastingPlane', type=str2bool, default=False);
# whether to replace the blobbie with a MacBeth color checker
parser.add_argument('--MacBethColorCheckerInsteadOfBlobbie', type=str2bool, default=False);
parser.add_argument('--MacBethColorCheckerOrientation', type=float, default=0);
parser.add_argument('--MacBethColorCheckerElevation', type=float, default=18);
# whether to add a tiny blobbie
parser.add_argument('--addTinyBlobbieInShadow', type=str2bool, default=False);
# whether to add a primary Macbeth color checker 
parser.add_argument('--addPrimaryMacBethColorChecker', type=str2bool, default=False);
parser.add_argument('--primaryMacBethColorCheckerCheckSize', type=float, default=4);
parser.add_argument('--primaryMacBethColorCheckerXrotation', type=float, default=0);
parser.add_argument('--primaryMacBethColorCheckerYrotation', type=float, default=0);
parser.add_argument('--primaryMacBethColorCheckerZrotation', type=float, default=0);
parser.add_argument('--primaryMacBethColorCheckerXpos', type=float, default=0);
parser.add_argument('--primaryMacBethColorCheckerYpos', type=float, default=0);
parser.add_argument('--primaryMacBethColorCheckerZpos', type=float, default=0);
# whether to add a secondary Macbeth color checker 
parser.add_argument('--addSecondaryMacBethColorChecker', type=str2bool, default=False);
parser.add_argument('--secondaryMacBethColorCheckerCheckSize', type=float, default=4);
parser.add_argument('--secondaryMacBethColorCheckerXrotation', type=float, default=0);
parser.add_argument('--secondaryMacBethColorCheckerYrotation', type=float, default=0);
parser.add_argument('--secondaryMacBethColorCheckerZrotation', type=float, default=0);
parser.add_argument('--secondaryMacBethColorCheckerXpos', type=float, default=0);
parser.add_argument('--secondaryMacBethColorCheckerYpos', type=float, default=0);
parser.add_argument('--secondaryMacBethColorCheckerZpos', type=float, default=0);

args = parser.parse_args(pythonArgs);

print(args)
generateScene(args);