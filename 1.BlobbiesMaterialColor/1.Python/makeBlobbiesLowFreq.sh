#!/bin/sh

# directory defininitions
baseDirectory="/Volumes/SDXC_64GB"
baseDirectory="/Users/Shared"
homeDirectory="/Volumes/SDXC_64GB"
homeDirectory="/Users/nicolas/Documents/1.Code"
toolboxDirectory="$baseDirectory/Matlab/Toolboxes/RenderToolbox3/Utilities/BlenderPython"
rootDirectory="$homeDirectory/2.RenderToolboxImagery/1.Projects/1.BlobbiesMaterialColor"

blendFile="${rootDirectory}/1.Python/generateColorMaterialVaryingBlobbies.py"
exportsDirectory="${rootDirectory}/2.ColladaExports"

# assemble command
/Applications/Blender/blender.app/Contents/MacOS/blender \
--python $blendFile -- \
--sceneName "BlobbieVeryHighFreq2" \
--blobbieSubdivisions 8 \
--frequencyX 12 \
--frequencyY 18 \
--frequencyZ -12 \
--gainX 0.018 \
--gainY 0.016 \
--gainZ 0.030 \
--toolboxDirectory $toolboxDirectory \
--exportsDirectory $exportsDirectory

