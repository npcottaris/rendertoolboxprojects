function generateControlSamsungSceneSet

    % Setup
    computerName = char(java.net.InetAddress.getLocalHost.getHostName);
    if strcmp(computerName, 'Ithaka.lan')
        setpref('RenderToolbox3', 'workingFolder', '/Volumes/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    else
        setpref('RenderToolbox3', 'workingFolder', '/Users/Shared/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    end
    
    % clean-up temporary disk
    system(sprintf('rm -r -f %s', getpref('RenderToolbox3', 'workingFolder')));
    
    % rendering quality
    renderInHighResQuality = true;
    
    if (renderInHighResQuality)
        renderedImageParams = struct(...
             'width', 880, ...
             'height', 700, ...
             'sampleCount', 2048, ...
             'pixelSamples', 2048 ...
             );
    else
        renderedImageParams = struct(...
             'width', 880/2, ...
             'height', 700/2, ...
             'sampleCount', 2048/16, ...
             'pixelSamples', 2048/16 ...
             );
    end
    
    [rootDir, renderedScenesDir] = getRootAndParentDirs(mfilename());
    hints = generateBatchRendererOptions(renderedImageParams.width, renderedImageParams.height);
    ChangeToWorkingFolder(hints);
    
    % Collada files
    colladaFiles = {'BlobbieTiltedMacBethColorCheckerScene', ...
                    'BlobbieHorizontalMacBethColorCheckerScene', ...
                    'BlobbieVerticalMacBethColorCheckerScene' ...
        };
    
    
    % --------------------- Lighting Conds --------------------------
    lightingConds{1} = struct(...
        'areaLights',        1, ...   % [0, 1, 2];
        'ceilingLights',     0, ...   % [0, 1];
        'frontWallLights',   0 ...    % [0, 1];
    );
        
    lightingConds{2} = struct(...
        'areaLights',        0, ...   % [0, 1, 2];
        'ceilingLights',     1, ...   % [0, 1];
        'frontWallLights',   0 ...    % [0, 1];
    );
    
    
    % Generate mappings file
    mappingsFile = generateMappingsFile(rootDir, renderedImageParams.sampleCount, renderedImageParams.pixelSamples);
        
    for i = 1:numel(lightingConds)
        
        s = lightingConds{i};
        
        if (s.areaLights == 0)
            areaLightSPD   = 'NeutralDay_0.00.spd';
            pointLightSPD  = 'NeutralDay_0.00.spd';
        elseif (s.areaLights == 1)
            areaLightSPD   = 'NeutralDay_0.30.spd';
            pointLightSPD  = 'NeutralDay_0.00.spd';
        elseif (s.areaLights == 2)
            areaLightSPD   = 'NeutralDay_0.00.spd';
            pointLightSPD  = 'NeutralDay_0.30.spd';
        else
            error('Unknown area type light');
        end


        if (s.frontWallLights == 0)
            frontWallLightSPD = 'NeutralDay_0.00.spd';
        elseif (s.frontWallLights == 1)
            frontWallLightSPD = 'NeutralDay_0.30.spd';
        else
            error('Unknown front wall light');
        end


        if (s.ceilingLights == 0)  
            ceilingLightSPD = 'NeutralDay_0.00.spd'; 
        elseif (s.ceilingLights == 1)  
            ceilingLightSPD = 'NeutralDay_0.30.spd'; 
        else
            error('Unknown ceiling light');
        end
    
        
        for j = 1:numel(colladaFiles)
            colladaFile = colladaFiles{j};
            fullColladaFile = fullPathToColladaFile(rootDir, colladaFile);

            % Generate unique condition string
            conditionString = sprintf('_Lights_area%d_front%d_ceiling%d', ...
                s.areaLights, s.frontWallLights, s.ceilingLights);

            % Generate conditions file
            imageName = [colladaFile conditionString];
            conditionsFile = generateConditionsFile(rootDir, imageName, ...
                pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD);

            % Action
            nativeSceneFile   = MakeSceneFiles(fullColladaFile, conditionsFile, mappingsFile, hints);
            radianceDataFiles = BatchRender(nativeSceneFile, hints);

            % Generate tiff preview
            montageFile = fullfile(renderedScenesDir, [colladaFile conditionString '.tiff']);
            % Tone mapping settings for tiff image
            toneMapFactor = 8.0;  % 4 is good for arealights
            isScale       = true;
            [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScale, hints);

            % Move radiance file to renderedScenes local dir
            system(sprintf('mv %s %s/', radianceDataFiles{1}, renderedScenesDir));
            cd(rootDir);
        end % for j
    end % for i
end


function mappingsFile = generateMappingsFile(rootDir, sampleCount, pixelSamples)

    sections{1} = { ...
        '% Camera position and properties'
        'Collada {'
        '    % Camera geometric object'
        '    % flip handedness to match Blender-Collada output to Mitsuba and PBRT'
        '    Camera:scale|sid=scale = -1 1 1'
        ' '
        '    % Camera FOV'
        '    Camera-camera:optics:technique_common:perspective:xfov = 36'
        ''
        '}'
        ''
        };
     
    sections{2} = { ...
        '% Lights'
        'Generic {'
        '    frontLeftAreaLamp-light:light:point'
        '    frontLeftAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontLeftAreaLamp-light:samplingWeight.float = (pointLightSamplingWeight)'
        '    frontLeftAreaLamp-mesh:light:area'
        '    frontLeftAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        '    frontLeftAreaLamp-mesh:samplingWeight.float = (areaLightSamplingWeight)'
        ''
        '    frontRightAreaLamp-light:light:point'
        '    frontRightAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontRightAreaLamp-light:samplingWeight.float = (pointLightSamplingWeight)'
        '    frontRightAreaLamp-mesh:light:area'
        '    frontRightAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        '    frontRightAreaLamp-mesh:samplingWeight.float = (areaLightSamplingWeight)'
        ''
        '    frontWall-mesh-mesh:light:area'
        '    frontWall-mesh-mesh:intensity.spectrum = (frontWallAreaLightIlluminantSPD)'
        ''
        '    ceiling-mesh-mesh:light:area'
        '    ceiling-mesh-mesh:intensity.spectrum = (ceilingAreaLightIlluminantSPD)'
        '}'
        ''
        };
    
    sampleCountString  = sprintf('    Camera-camera_sampler:sampleCount.integer  = %d', sampleCount);
    pixelSamplesString = sprintf('    Camera-camera_sampler:pixelSamples.integer = %d', pixelSamples);
    sections{3} = { ...
        '% Light Integration and Sampling'
        'Mitsuba {'
        '    % ============== Light integrator section ===================='
        '    integrator:integrator:bdpt'
        '    integrator:maxDepth.integer     = 7'
        '    integrator:sampleDirect.boolean = true'
        '    integrator:lightImage.boolean   = true'
        ''
        '    % ======== Camera sampler settings ============='
        '    Camera-camera_sampler:sampler:ldsampler'
        sampleCountString
        pixelSamplesString
        '}'
        ''
        };
    
    sections{4} = { ...
        '% Materials'
        'Generic {'
        '    mccBabel-1-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-1-material:variant.string = balanced'
        '    mccBabel-1-material:diffuseReflectance.spectrum = mccBabel-1.spd'
        '    mccBabel-1-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-1-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-1-material:alphaU.float = 1.0'
        '    mccBabel-1-material:alphaV.float = 1.0'
        ''
        '    mccBabel-2-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-2-material:variant.string = balanced'
        '    mccBabel-2-material:diffuseReflectance.spectrum = mccBabel-2.spd'
        '    mccBabel-2-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-2-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-2-material:alphaU.float = 1.0'
        '    mccBabel-2-material:alphaV.float = 1.0'
        ''
        '    mccBabel-3-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-3-material:variant.string = balanced'
        '    mccBabel-3-material:diffuseReflectance.spectrum = mccBabel-3.spd'
        '    mccBabel-3-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-3-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-3-material:alphaU.float = 1.0'
        '    mccBabel-3-material:alphaV.float = 1.0'
        ''
        '    mccBabel-4-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-4-material:variant.string = balanced'
        '    mccBabel-4-material:diffuseReflectance.spectrum = mccBabel-4.spd'
        '    mccBabel-4-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-4-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-4-material:alphaU.float = 1.0'
        '    mccBabel-4-material:alphaV.float = 1.0'
        ''
        '    mccBabel-5-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-5-material:variant.string = balanced'
        '    mccBabel-5-material:diffuseReflectance.spectrum = mccBabel-5.spd'
        '    mccBabel-5-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-5-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-5-material:alphaU.float = 1.0'
        '    mccBabel-5-material:alphaV.float = 1.0'
        ''
        '    mccBabel-6-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-6-material:variant.string = balanced'
        '    mccBabel-6-material:diffuseReflectance.spectrum = mccBabel-6.spd'
        '    mccBabel-6-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-6-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-6-material:alphaU.float = 1.0'
        '    mccBabel-6-material:alphaV.float = 1.0'
        ''
        '    mccBabel-7-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-7-material:variant.string = balanced'
        '    mccBabel-7-material:diffuseReflectance.spectrum = mccBabel-7.spd'
        '    mccBabel-7-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-7-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-7-material:alphaU.float = 1.0'
        '    mccBabel-7-material:alphaV.float = 1.0'
        ''
        '    mccBabel-8-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-8-material:variant.string = balanced'
        '    mccBabel-8-material:diffuseReflectance.spectrum = mccBabel-8.spd'
        '    mccBabel-8-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-8-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-8-material:alphaU.float = 1.0'
        '    mccBabel-8-material:alphaV.float = 1.0'
        ''
        '    mccBabel-9-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-9-material:variant.string = balanced'
        '    mccBabel-9-material:diffuseReflectance.spectrum = mccBabel-9.spd'
        '    mccBabel-9-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-9-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-9-material:alphaU.float = 1.0'
        '    mccBabel-9-material:alphaV.float = 1.0'
        ''
        '    mccBabel-10-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-10-material:variant.string = balanced'
        '    mccBabel-10-material:diffuseReflectance.spectrum = mccBabel-10.spd'
        '    mccBabel-10-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-10-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-10-material:alphaU.float = 1.0'
        '    mccBabel-10-material:alphaV.float = 1.0'
        ''
        '    mccBabel-11-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-11-material:variant.string = balanced'
        '    mccBabel-11-material:diffuseReflectance.spectrum = mccBabel-11.spd'
        '    mccBabel-11-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-11-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-11-material:alphaU.float = 1.0'
        '    mccBabel-11-material:alphaV.float = 1.0'
        ''
        '    mccBabel-12-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-12-material:variant.string = balanced'
        '    mccBabel-12-material:diffuseReflectance.spectrum = mccBabel-12.spd'
        '    mccBabel-12-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-12-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-12-material:alphaU.float = 1.0'
        '    mccBabel-12-material:alphaV.float = 1.0'
        ''
        '    mccBabel-13-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-13-material:variant.string = balanced'
        '    mccBabel-13-material:diffuseReflectance.spectrum = mccBabel-13.spd'
        '    mccBabel-13-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-13-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-13-material:alphaU.float = 1.0'
        '    mccBabel-13-material:alphaV.float = 1.0'
        ''
        '    mccBabel-14-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-14-material:variant.string = balanced'
        '    mccBabel-14-material:diffuseReflectance.spectrum = mccBabel-14.spd'
        '    mccBabel-14-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-14-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-14-material:alphaU.float = 1.0'
        '    mccBabel-14-material:alphaV.float = 1.0'
        ''
        '    mccBabel-15-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-15-material:variant.string = balanced'
        '    mccBabel-15-material:diffuseReflectance.spectrum = mccBabel-15.spd'
        '    mccBabel-15-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-15-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-15-material:alphaU.float = 1.0'
        '    mccBabel-15-material:alphaV.float = 1.0'
        ''
        '    mccBabel-16-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-16-material:variant.string = balanced'
        '    mccBabel-16-material:diffuseReflectance.spectrum = mccBabel-16.spd'
        '    mccBabel-16-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-16-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-16-material:alphaU.float = 1.0'
        '    mccBabel-16-material:alphaV.float = 1.0'
        ''
        '    mccBabel-17-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-17-material:variant.string = balanced'
        '    mccBabel-17-material:diffuseReflectance.spectrum = mccBabel-17.spd'
        '    mccBabel-17-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-17-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-17-material:alphaU.float = 1.0'
        '    mccBabel-17-material:alphaV.float = 1.0'
        ''
        '    mccBabel-18-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-18-material:variant.string = balanced'
        '    mccBabel-18-material:diffuseReflectance.spectrum = mccBabel-18.spd'
        '    mccBabel-18-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-18-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-18-material:alphaU.float = 1.0'
        '    mccBabel-18-material:alphaV.float = 1.0'
        ''
        '    mccBabel-19-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-19-material:variant.string = balanced'
        '    mccBabel-19-material:diffuseReflectance.spectrum = mccBabel-19.spd'
        '    mccBabel-19-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-19-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-19-material:alphaU.float = 1.0'
        '    mccBabel-19-material:alphaV.float = 1.0'
        ''
        '    mccBabel-20-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-20-material:variant.string = balanced'
        '    mccBabel-20-material:diffuseReflectance.spectrum = mccBabel-20.spd'
        '    mccBabel-20-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-20-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-20-material:alphaU.float = 1.0'
        '    mccBabel-20-material:alphaV.float = 1.0'       
        ''
        '    mccBabel-21-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-21-material:variant.string = balanced'
        '    mccBabel-21-material:diffuseReflectance.spectrum = mccBabel-21.spd'
        '    mccBabel-21-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-21-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-21-material:alphaU.float = 1.0'
        '    mccBabel-21-material:alphaV.float = 1.0'
        ''
        '    mccBabel-22-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-22-material:variant.string = balanced'
        '    mccBabel-22-material:diffuseReflectance.spectrum = mccBabel-22.spd'
        '    mccBabel-22-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-22-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-22-material:alphaU.float = 1.0'
        '    mccBabel-22-material:alphaV.float = 1.0'    
        ''
        '    mccBabel-23-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-23-material:variant.string = balanced'
        '    mccBabel-23-material:diffuseReflectance.spectrum = mccBabel-23.spd'
        '    mccBabel-23-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-23-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-23-material:alphaU.float = 1.0'
        '    mccBabel-23-material:alphaV.float = 1.0'
        ''
        '    mccBabel-24-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-24-material:variant.string = balanced'
        '    mccBabel-24-material:diffuseReflectance.spectrum = mccBabel-24.spd'
        '    mccBabel-24-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-24-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-24-material:alphaU.float = 1.0'
        '    mccBabel-24-material:alphaV.float = 1.0'  
        ''
        '    pedestalMaterial-material:bsdf:roughplastic'
        '    pedestalMaterial-material:alpha.float = 0.003'
        '    pedestalMaterial-material:diffuseReflectance.spectrum  = (pedestalDiffuseReflectanceSPD)'
        '    pedestalMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    roomMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    roomMaterial-material:variant.string = balanced'
        '    roomMaterial-material:diffuseReflectance.spectrum  = (roomDiffuseReflectanceSPD)'
        '    roomMaterial-material:specularReflectance.spectrum = 300:0 800:0'
        '    roomMaterial-material:ensureEnergyConservation.boolean = true'
        '    roomMaterial-material:alphaU.float = 0.1'
        '    roomMaterial-material:alphaV.float = 0.1'
        ''
        '    backWallMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    backWallMaterial-material:variant.string = balanced'
        '    backWallMaterial-material:diffuseReflectance.spectrum  = (backWallDiffuseReflectanceSPD)'
        '    backWallMaterial-material:specularReflectance.spectrum = (backWallSpecularReflectanceSPD)'
        '    backWallMaterial-material:ensureEnergyConservation.boolean = true'
        '    backWallMaterial-material:alphaU.float = 0.01'
        '    backWallMaterial-material:alphaV.float = 0.01'
        ''
        '    darkCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    darkCheckMaterial-material:variant.string = balanced'
        '    darkCheckMaterial-material:diffuseReflectance.spectrum  = (darkCheckDiffuseReflectanceSPD)'
        '    darkCheckMaterial-material:specularReflectance.spectrum = (darkCheckSpecularReflectanceSPD)'
        '    darkCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    darkCheckMaterial-material:alphaU.float = 0.01'
        '    darkCheckMaterial-material:alphaV.float = 0.01'
        ''
        '    lightCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    lightCheckMaterial-material:variant.string = balanced'
        '    lightCheckMaterial-material:diffuseReflectance.spectrum  = (lightCheckDiffuseReflectanceSPD)'
        '    lightCheckMaterial-material:specularReflectance.spectrum = (lightCheckSpecularReflectanceSPD)'
        '    lightCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    lightCheckMaterial-material:alphaU.float = 0.01'
        '    lightCheckMaterial-material:alphaV.float = 0.01'
        ''
        '}'
        ''
        };
    
    mappingsFile = fullfile(rootDir, '2.Mappings', 'mappings_123.txt');
    
    fid = fopen(mappingsFile, 'w');
    for l = 1:numel(sections)
        for k = 1:numel(sections{l})
            fprintf(fid, '%s\n',sections{l}{k});
        end
    end
    fclose(fid);
end

function conditionsFile = generateConditionsFile(rootDir, imageName, pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD)
    resourcesDir  = fullfile(rootDir, '3.Resources/NewStuff', filesep); 
    conditionKeys = {'imageName', ...
                     'pedestalDiffuseReflectanceSPD', ...
                     'darkCheckDiffuseReflectanceSPD', ...
                     'darkCheckSpecularReflectanceSPD', ...
                     'lightCheckDiffuseReflectanceSPD', ...
                     'lightCheckSpecularReflectanceSPD', ...
                     'roomDiffuseReflectanceSPD', ...
                     'backWallDiffuseReflectanceSPD', ...
                     'backWallSpecularReflectanceSPD', ...
                     'pointLightIlluminantSPD', ...
                     'areaLightIlluminantSPD', ...
                     'frontWallAreaLightIlluminantSPD', ...
                     'ceilingAreaLightIlluminantSPD', ...
                     'areaLightSamplingWeight', ...
                     'pointLightSamplingWeight' ...
                    };
                   
    areaLightSamplingWeight = 1.0;
    pointLightSamplingWeight = 1.0;
    
    conditionValues = {imageName, ... 
                       [resourcesDir 'NeutralDay-Gray_0_4.spd'], ...   % pedestal diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_9.spd'], ...   % light check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % light check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_6.spd'], ...   % room diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_5.spd'], ...    % backwall diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_0.spd'], ...    % backwall specular SPD: NeutralDay-Gray_0_5 for max-effect mirror backwall, NeutralDay-Gray_0_0.spd for matte
                       [resourcesDir pointLightSPD], ...                % point lights SPD: point lights on: NeutralDay.spd / point lights off: NeutralDay_0_0.spd
                       [resourcesDir areaLightSPD], ...                 % area lights SPD:  area lights on: NeutralDay.spd / area lights off: NeutralDay_0_0.spd
                       [resourcesDir frontWallLightSPD], ...            % frontwall lamp SPD: the front wall area light (0.1)
                       [resourcesDir ceilingLightSPD], ...              % ceiling lamp SPD: the ceiling area light (0.1)
                       areaLightSamplingWeight, ...
                       pointLightSamplingWeight ...
                       };
    
    theConditionValues = conditionValues;
    conditionsFile = fullfile(rootDir, '1.Conditions', 'conditions_123.txt');
    conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, theConditionValues);
end


function hints = generateBatchRendererOptions(imageWidth, imageHeight)
    hints = struct(...
        'imageWidth',               imageWidth, ...
        'imageHeight',              imageHeight, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba' ...
        );
end

function fullColladaFile = fullPathToColladaFile(rootDir, filename)
    currentDir = pwd;
    cd(rootDir);
    cd ..
    cd('2.ColladaExports');
    fullColladaFile = fullfile(pwd, [filename '.dae']);
    cd(currentDir);
end

function [rootDir, renderedScenesDir] = getRootAndParentDirs(mfile)
    [rootDir, ~, ~] = fileparts(which(mfile));
    currDir = pwd;
    cd(rootDir);
    cd ..
    parentDir = pwd;
   % renderedScenesDir = fullfile(parentDir, '4.RenderedScenes');
    renderedScenesDir = '/Users1/Shared/Matlab/RT3scenes/Blobbies';
    cd(currDir);
end


