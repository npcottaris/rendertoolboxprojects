function generateSamsungSceneSetInSunRoom

   
    [diffuseReflectanceFunctionFileNames, specularReflectanceFunctionFileNames] = generateSamsungReflectanceFunctions();
    plotReflectanceFunctionsUsed(specularReflectanceFunctionFileNames, diffuseReflectanceFunctionFileNames) ;
    
    % Setup
    computerName = char(java.net.InetAddress.getLocalHost.getHostName);

    if strcmp(computerName, 'Ithaka.lan')
        setpref('RenderToolbox3', 'workingFolder', '/Volumes/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    else
        setpref('RenderToolbox3', 'workingFolder', '/Users1/Shared/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    end
    
    % clean-up temporary disk
    system(sprintf('rm -r -f %s', getpref('RenderToolbox3', 'workingFolder')));
    
    % rendering quality
    renderInHighResQuality = true;
    
    if (renderInHighResQuality)
        renderedImageParams = struct(...
             'width', 880, ...
             'height', 640, ...
             'sampleCount', 8192*4, ...
             'pixelSamples', 1024 ...
             );
    else
        renderedImageParams = struct(...
             'width', 880/2, ...
             'height', 640/2, ...
             'sampleCount', 8192, ... % this determines the grainy-ness quality
             'pixelSamples', 1024 ...
             );
    end
    
    
    [rootDir, renderedScenesDir] = getRootAndParentDirs(mfilename());
    
    
    fprintf('\nRadiance and TIFF data will be saved in ''%s''.\n', renderedScenesDir);
    hints = generateBatchRendererOptions(renderedImageParams.width, renderedImageParams.height);
    ChangeToWorkingFolder(hints);
    
    % Collada file
    colladaFiles = {'BlobbieVeryLowFreq', 'BlobbieLowFreq', 'BlobbieMediumFreq', 'BlobbieHighFreq', 'BlobbieVeryHighFreq', 'BlobbieUltraHighFreq2'};
    colladaFiles = {'BlobbieLowFreq', 'BlobbieMediumFreq', 'BlobbieHighFreq', 'BlobbieVeryHighFreq', 'BlobbieUltraHighFreq2'};
    
    colladaFiles = {'Blobbie9SubsVeryLowFreq', ...
                    'Blobbie9SubsLowFreq', ...
                    'Blobbie9SubsMediumFreq', ...
                    'Blobbie9SubsHighFreq', ...
                    'Blobbie9SubsVeryHighFreq'};
    
    colladaFiles = {'Blobbie8SubsHighFreqMultipleBlobbies', ...
                    'Blobbie8SubsVeryLowFreqMultipleBlobbies' ...           
        };

    colladaFiles = {'Blobbie8SubsVeryLowFreqMultipleBlobbiesOpenRoof' ...           
        };
    
    
    % --------------------- Lighting --------------------------
    areaTypeLights = 1; % [0, 1, 2];
    ceilingLight   = 0; % [0 1];
    % keep front wall at 0 for now
    frontWallLight = 0; % [0 1];
    
    
    outsideLightSPD = 'NeutralDay_0.30.spd';
    
    if (areaTypeLights == 0)
        areaLightSPD   = 'NeutralDay_0.00.spd';
        pointLightSPD  = 'NeutralDay_0.00.spd';
    elseif (areaTypeLights == 1)
        areaLightSPD   = 'NeutralDay_0.30.spd';
        pointLightSPD  = 'NeutralDay_0.00.spd';
    elseif (areaTypeLights == 2)
        areaLightSPD   = 'NeutralDay_0.00.spd';
        pointLightSPD  = 'NeutralDay_0.30.spd';
    else
        error('Unknown area type light');
    end
    
    
    if (frontWallLight == 0)
        frontWallLightSPD = 'NeutralDay_0.00.spd';
    elseif (frontWallLight == 1)
        frontWallLightSPD = 'NeutralDay_0.30.spd';
    else
        error('Unknown front wall light');
    end
    
    
    if (ceilingLight == 0)  
        ceilingLightSPD = 'NeutralDay_0.00.spd'; 
    elseif (ceilingLight == 1)  
        ceilingLightSPD = 'NeutralDay_0.30.spd'; 
    else
        error('Unknown ceiling light');
    end
    
    % --------------------- Lighting --------------------------
    
    % full set
    materialAlphas = [0.005 0.01 0.02 0.04 0.08 0.16 0.32];
    
    % set for rotating blobbies
    materialAlphas = [0.025 0.16 0.32]; %  0.02  0.08  0.32];
    materialAlphas = [0.32 0.16 0.025];
    
    if (1==2)
    if (strcmp(computerName, 'manta.PSYCH.UPENN.EDU'))
        %fprintf(2,'\n----------------------------------------------------------------------------------\nRunning on Manta (24 cores), computing the 2,3 .. specular reflectances.\n----------------------------------------------------------------------------------\n');
        % only do the first of the specular reflectanceFunctions
        %specularReflectanceFunctionFileNames = {specularReflectanceFunctionFileNames};
    else
        fprintf(2,'\n----------------------------------------------------------------------------------\nNot running on Manta (24 cores), so only computing the first specular reflectance.\n----------------------------------------------------------------------------------\n');
        % only do the first of the specular reflectanceFunctions
        specularReflectanceFunctionFileNames = {specularReflectanceFunctionFileNames{1}};
    end
    end
    
    materialSpecularSPDs = specularReflectanceFunctionFileNames;
    materialDiffuseSPDs = diffuseReflectanceFunctionFileNames;
    
    
    % rotation angles in degrees
    rotations = [-20:1:-1 1:1:20];
    rotations = [5:1:20];
    rotations = 0;
    
    sensorXYZ = loadXYZCMFs();
    wattsToLumens = 683;
    
    imNo = 0;
    
    for f = 1:numel(rotations)
        rotationInDegrees = rotations(f);
        % Generate mappings file
        mappingsFile = generateMappingsFile(rootDir, renderedImageParams.sampleCount, renderedImageParams.pixelSamples, rotationInDegrees);
        
    for j = 1:numel(colladaFiles)
        colladaFile = colladaFiles{j};
        fullColladaFile = fullPathToColladaFile(rootDir, colladaFile);
        for k = 1:numel(materialAlphas)
            for l = 1:numel(materialSpecularSPDs)
                for m = 1:numel(materialDiffuseSPDs)
                    % unpack varying  params
                    alpha       = materialAlphas(k);         % alpha of specular reflectance
                    specularSPD = materialSpecularSPDs{l};   % spectrum of specular reflectance (specular reflection magnitude, since it is a flat SPD)
                    diffuseSPD  = materialDiffuseSPDs{m};    % spectrum of diffuse reflectance (color)

                   
                    % Generate unique condition string
                    conditionString = sprintf('_%s___%s___alpha_%3.3f___Lights_area%d_front%d_ceiling%d_rotationAngle_%d', ...
                        specularSPD, diffuseSPD, alpha, areaTypeLights, frontWallLight, ceilingLight, rotationInDegrees);

                    % Generate conditions file
                    imageName = [colladaFile conditionString];
                    matFile = fullfile(renderedScenesDir, sprintf('%s.mat',imageName));
                    
                    if (1==2)
                    if (exist(matFile, 'file'))
                        fprintf('\nFile %s exists. Move to next condition.\n', matFile);
                        continue;
                    else
                        fprintf('\nWill generate %s\n', matFile); 
                    end
                    end
                    
                    conditionsFile = generateConditionsFile(rootDir, imageName, diffuseSPD, specularSPD, alpha, ...
                        pointLightSPD, areaLightSPD, outsideLightSPD, frontWallLightSPD, ceilingLightSPD);

                    % Action
                    nativeSceneFile   = MakeSceneFiles(fullColladaFile, conditionsFile, mappingsFile, hints);
                    radianceDataFiles = BatchRender(nativeSceneFile, hints);

                    % Generate tiff preview
                    montageFile = fullfile(renderedScenesDir, [colladaFile conditionString '.tiff']);
                    % Tone mapping settings for tiff image
                    toneMapFactor = 8.0;  % 4 is good for arealights
                    isScale       = true;
                    [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScale, hints);

 
                    % Move radiance file to renderedScenes local dir
                    system(sprintf('mv %s %s/', radianceDataFiles{1}, renderedScenesDir));
                    cd(rootDir);
                    
                    radianceDataFile = fullfile(renderedScenesDir,strrep(radianceDataFiles{1}, sprintf('%s/renderings/Mitsuba/',getpref('RenderToolbox3', 'workingFolder')), ''));
                    load(radianceDataFile, 'S', 'multispectralImage');
                    XYZimage = MultispectralToSensorImage(multispectralImage, S, sensorXYZ.T, sensorXYZ.S);
                    
                    luminanceMap = XYZimage(:,:,2)*wattsToLumens;
                    minLum = min(luminanceMap(:)); maxLum = max(luminanceMap(:));
                    
                    imNo = imNo + 1; figure(imNo); clf;
                    imagesc(luminanceMap);
                    set(gca, 'CLim', [0 maxLum]);
                    colormap(gray(1024));
                    axis 'image';
                    title(sprintf('Luminance ratio: %2.2f [%2.2f - %2.2f]\n', maxLum/minLum, minLum, maxLum));
                    fprintf('Dynamic range: %2.2f [%2.2f - %2.2f]\n', maxLum/minLum, minLum, maxLum);
                    drawnow;
                    pause(0.3);
                    
                end % for m
            end % for l
        end % for k
    end % for j
    end % for f
end


function mappingsFile = generateMappingsFile(rootDir, sampleCount, pixelSamples, rotationAngleInDegrees)

    pedestalRotationString = sprintf('    pedestal:rotate|sid=rotationZ = 0 0 1 %d', rotationAngleInDegrees);
    
    sections{1} = { ...
        '% Camera position and properties'
        'Collada {'
        '    % Camera geometric object'
        '    % flip handedness to match Blender-Collada output to Mitsuba and PBRT'
        '    Camera:scale|sid=scale = -1 1 1'
        ' '
        '    % Camera FOV'
        '    Camera-camera:optics:technique_common:perspective:xfov = 42'
        ''
        pedestalRotationString
        '}'
        ''
        };
     
    sections{2} = { ...
        '% Lights'
        'Generic {'
        '    frontLeftAreaLamp-light:light:point'
        '    frontLeftAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontLeftAreaLamp-light:samplingWeight.float = (pointLightSamplingWeight)'
        '    frontLeftAreaLamp-mesh:light:area'
        '    frontLeftAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        '    frontLeftAreaLamp-mesh:samplingWeight.float = (areaLightSamplingWeight)'
        ''
        '    frontRightAreaLamp-light:light:point'
        '    frontRightAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontRightAreaLamp-light:samplingWeight.float = (pointLightSamplingWeight)'
        '    frontRightAreaLamp-mesh:light:area'
        '    frontRightAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        '    frontRightAreaLamp-mesh:samplingWeight.float = (areaLightSamplingWeight)'
        ''
        '    outsideAreaLamp-light:light:point'
        '    outsideAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    outsideAreaLamp-light:samplingWeight.float = (pointLightSamplingWeight)'
        '    outsideAreaLamp-mesh:light:area'
        '    outsideAreaLamp-mesh:intensity.spectrum = (outsideLightIlluminantSPD)'
        '    outsideAreaLamp-mesh:samplingWeight.float = (areaLightSamplingWeight)'
        ''
        '}'
        ''
        };
    
    sampleCountString  = sprintf('    Camera-camera_sampler:sampleCount.integer  = %d', sampleCount);
    pixelSamplesString = sprintf('    Camera-camera_sampler:pixelSamples.integer = %d', pixelSamples);
    sections{3} = { ...
        '% Light Integration and Sampling'
        'Mitsuba {'
        '    % ============== Light integrator section ===================='
        '    integrator:integrator:bdpt'
        '    integrator:maxDepth.integer     = 7'
        '    integrator:sampleDirect.boolean = true'
        '    integrator:lightImage.boolean   = true'
        ''
        '    % ======== Camera sampler settings ============='
        '    Camera-camera_sampler:sampler:ldsampler'
        sampleCountString
        pixelSamplesString
        '}'
        ''
        };
    
    sections{4} = { ...
        '% Materials'
        'Generic {'
                '    mccBabel-1-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-1-material:variant.string = balanced'
        '    mccBabel-1-material:diffuseReflectance.spectrum = mccBabel-1.spd'
        '    mccBabel-1-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-1-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-1-material:alphaU.float = 1.0'
        '    mccBabel-1-material:alphaV.float = 1.0'
        ''
        '    mccBabel-2-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-2-material:variant.string = balanced'
        '    mccBabel-2-material:diffuseReflectance.spectrum = mccBabel-2.spd'
        '    mccBabel-2-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-2-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-2-material:alphaU.float = 1.0'
        '    mccBabel-2-material:alphaV.float = 1.0'
        ''
        '    mccBabel-3-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-3-material:variant.string = balanced'
        '    mccBabel-3-material:diffuseReflectance.spectrum = mccBabel-3.spd'
        '    mccBabel-3-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-3-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-3-material:alphaU.float = 1.0'
        '    mccBabel-3-material:alphaV.float = 1.0'
        ''
        '    mccBabel-4-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-4-material:variant.string = balanced'
        '    mccBabel-4-material:diffuseReflectance.spectrum = mccBabel-4.spd'
        '    mccBabel-4-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-4-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-4-material:alphaU.float = 1.0'
        '    mccBabel-4-material:alphaV.float = 1.0'
        ''
        '    mccBabel-5-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-5-material:variant.string = balanced'
        '    mccBabel-5-material:diffuseReflectance.spectrum = mccBabel-5.spd'
        '    mccBabel-5-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-5-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-5-material:alphaU.float = 1.0'
        '    mccBabel-5-material:alphaV.float = 1.0'
        ''
        '    mccBabel-6-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-6-material:variant.string = balanced'
        '    mccBabel-6-material:diffuseReflectance.spectrum = mccBabel-6.spd'
        '    mccBabel-6-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-6-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-6-material:alphaU.float = 1.0'
        '    mccBabel-6-material:alphaV.float = 1.0'
        ''
        '    mccBabel-7-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-7-material:variant.string = balanced'
        '    mccBabel-7-material:diffuseReflectance.spectrum = mccBabel-7.spd'
        '    mccBabel-7-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-7-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-7-material:alphaU.float = 1.0'
        '    mccBabel-7-material:alphaV.float = 1.0'
        ''
        '    mccBabel-8-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-8-material:variant.string = balanced'
        '    mccBabel-8-material:diffuseReflectance.spectrum = mccBabel-8.spd'
        '    mccBabel-8-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-8-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-8-material:alphaU.float = 1.0'
        '    mccBabel-8-material:alphaV.float = 1.0'
        ''
        '    mccBabel-9-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-9-material:variant.string = balanced'
        '    mccBabel-9-material:diffuseReflectance.spectrum = mccBabel-9.spd'
        '    mccBabel-9-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-9-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-9-material:alphaU.float = 1.0'
        '    mccBabel-9-material:alphaV.float = 1.0'
        ''
        '    mccBabel-10-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-10-material:variant.string = balanced'
        '    mccBabel-10-material:diffuseReflectance.spectrum = mccBabel-10.spd'
        '    mccBabel-10-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-10-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-10-material:alphaU.float = 1.0'
        '    mccBabel-10-material:alphaV.float = 1.0'
        ''
        '    mccBabel-11-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-11-material:variant.string = balanced'
        '    mccBabel-11-material:diffuseReflectance.spectrum = mccBabel-11.spd'
        '    mccBabel-11-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-11-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-11-material:alphaU.float = 1.0'
        '    mccBabel-11-material:alphaV.float = 1.0'
        ''
        '    mccBabel-12-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-12-material:variant.string = balanced'
        '    mccBabel-12-material:diffuseReflectance.spectrum = mccBabel-12.spd'
        '    mccBabel-12-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-12-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-12-material:alphaU.float = 1.0'
        '    mccBabel-12-material:alphaV.float = 1.0'
        ''
        '    mccBabel-13-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-13-material:variant.string = balanced'
        '    mccBabel-13-material:diffuseReflectance.spectrum = mccBabel-13.spd'
        '    mccBabel-13-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-13-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-13-material:alphaU.float = 1.0'
        '    mccBabel-13-material:alphaV.float = 1.0'
        ''
        '    mccBabel-14-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-14-material:variant.string = balanced'
        '    mccBabel-14-material:diffuseReflectance.spectrum = mccBabel-14.spd'
        '    mccBabel-14-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-14-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-14-material:alphaU.float = 1.0'
        '    mccBabel-14-material:alphaV.float = 1.0'
        ''
        '    mccBabel-15-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-15-material:variant.string = balanced'
        '    mccBabel-15-material:diffuseReflectance.spectrum = mccBabel-15.spd'
        '    mccBabel-15-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-15-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-15-material:alphaU.float = 1.0'
        '    mccBabel-15-material:alphaV.float = 1.0'
        ''
        '    mccBabel-16-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-16-material:variant.string = balanced'
        '    mccBabel-16-material:diffuseReflectance.spectrum = mccBabel-16.spd'
        '    mccBabel-16-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-16-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-16-material:alphaU.float = 1.0'
        '    mccBabel-16-material:alphaV.float = 1.0'
        ''
        '    mccBabel-17-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-17-material:variant.string = balanced'
        '    mccBabel-17-material:diffuseReflectance.spectrum = mccBabel-17.spd'
        '    mccBabel-17-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-17-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-17-material:alphaU.float = 1.0'
        '    mccBabel-17-material:alphaV.float = 1.0'
        ''
        '    mccBabel-18-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-18-material:variant.string = balanced'
        '    mccBabel-18-material:diffuseReflectance.spectrum = mccBabel-18.spd'
        '    mccBabel-18-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-18-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-18-material:alphaU.float = 1.0'
        '    mccBabel-18-material:alphaV.float = 1.0'
        ''
        '    mccBabel-19-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-19-material:variant.string = balanced'
        '    mccBabel-19-material:diffuseReflectance.spectrum = mccBabel-19.spd'
        '    mccBabel-19-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-19-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-19-material:alphaU.float = 1.0'
        '    mccBabel-19-material:alphaV.float = 1.0'
        ''
        '    mccBabel-20-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-20-material:variant.string = balanced'
        '    mccBabel-20-material:diffuseReflectance.spectrum = mccBabel-20.spd'
        '    mccBabel-20-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-20-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-20-material:alphaU.float = 1.0'
        '    mccBabel-20-material:alphaV.float = 1.0'       
        ''
        '    mccBabel-21-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-21-material:variant.string = balanced'
        '    mccBabel-21-material:diffuseReflectance.spectrum = mccBabel-21.spd'
        '    mccBabel-21-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-21-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-21-material:alphaU.float = 1.0'
        '    mccBabel-21-material:alphaV.float = 1.0'
        ''
        '    mccBabel-22-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-22-material:variant.string = balanced'
        '    mccBabel-22-material:diffuseReflectance.spectrum = mccBabel-22.spd'
        '    mccBabel-22-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-22-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-22-material:alphaU.float = 1.0'
        '    mccBabel-22-material:alphaV.float = 1.0'    
        ''
        '    mccBabel-23-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-23-material:variant.string = balanced'
        '    mccBabel-23-material:diffuseReflectance.spectrum = mccBabel-23.spd'
        '    mccBabel-23-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-23-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-23-material:alphaU.float = 1.0'
        '    mccBabel-23-material:alphaV.float = 1.0'
        ''
        '    mccBabel-24-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    mccBabel-24-material:variant.string = balanced'
        '    mccBabel-24-material:diffuseReflectance.spectrum = mccBabel-24.spd'
        '    mccBabel-24-material:specularReflectance.spectrum = 300:0 800:0'
        '    mccBabel-24-material:ensureEnergyConservation.boolean = true'
        '    mccBabel-24-material:alphaU.float = 1.0'
        '    mccBabel-24-material:alphaV.float = 1.0'  
        ''
        '    blobbieMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    blobbieMaterial-material:variant.string = balanced'
        '    blobbieMaterial-material:diffuseReflectance.spectrum = (objDiffuseReflectanceSPD)'
        '    blobbieMaterial-material:specularReflectance.spectrum = (objSpecularReflectanceSPD)'
        '    blobbieMaterial-material:ensureEnergyConservation.boolean = true'
        '    blobbieMaterial-material:alphaU.float = (objMaterialAlpha)'
        '    blobbieMaterial-material:alphaV.float = (objMaterialAlpha)'
        ''
        '    pedestalMaterial-material:bsdf:roughplastic'
        '    pedestalMaterial-material:alpha.float = 0.003'
        '    pedestalMaterial-material:diffuseReflectance.spectrum  = (pedestalDiffuseReflectanceSPD)'
        '    pedestalMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    shadowingBottomPlaneMaterial-material:bsdf:roughplastic'
        '    shadowingBottomPlaneMaterial-material:alpha.float = 0.1'
        '    shadowingBottomPlaneMaterial-material:diffuseReflectance.spectrum  = (shadowingBottomPlaneDiffuseReflectanceSPD)'
        '    shadowingBottomPlaneMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    shadowingTopPlaneMaterial-material:bsdf:roughplastic'
        '    shadowingTopPlaneMaterial-material:alpha.float = 0.1'
        '    shadowingTopPlaneMaterial-material:diffuseReflectance.spectrum  = (shadowingTopPlaneDiffuseReflectanceSPD)'
        '    shadowingTopPlaneMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    roomMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    roomMaterial-material:variant.string = balanced'
        '    roomMaterial-material:diffuseReflectance.spectrum  = (roomDiffuseReflectanceSPD)'
        '    roomMaterial-material:specularReflectance.spectrum = 300:0 800:0'
        '    roomMaterial-material:ensureEnergyConservation.boolean = true'
        '    roomMaterial-material:alphaU.float = 0.1'
        '    roomMaterial-material:alphaV.float = 0.1'
        ''
        '    backWallMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    backWallMaterial-material:variant.string = balanced'
        '    backWallMaterial-material:diffuseReflectance.spectrum  = (backWallDiffuseReflectanceSPD)'
        '    backWallMaterial-material:specularReflectance.spectrum = (backWallSpecularReflectanceSPD)'
        '    backWallMaterial-material:ensureEnergyConservation.boolean = true'
        '    backWallMaterial-material:alphaU.float = 0.01'
        '    backWallMaterial-material:alphaV.float = 0.01'
        ''
        '    darkCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    darkCheckMaterial-material:variant.string = balanced'
        '    darkCheckMaterial-material:diffuseReflectance.spectrum  = (darkCheckDiffuseReflectanceSPD)'
        '    darkCheckMaterial-material:specularReflectance.spectrum = (darkCheckSpecularReflectanceSPD)'
        '    darkCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    darkCheckMaterial-material:alphaU.float = 0.01'
        '    darkCheckMaterial-material:alphaV.float = 0.01'
        ''
        '    lightCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    lightCheckMaterial-material:variant.string = balanced'
        '    lightCheckMaterial-material:diffuseReflectance.spectrum  = (lightCheckDiffuseReflectanceSPD)'
        '    lightCheckMaterial-material:specularReflectance.spectrum = (lightCheckSpecularReflectanceSPD)'
        '    lightCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    lightCheckMaterial-material:alphaU.float = 0.01'
        '    lightCheckMaterial-material:alphaV.float = 0.01'
        ''
        '}'
        ''
        };
    
    mappingsFile = fullfile(rootDir, '2.Mappings', 'mappings_123.txt');
    
    fid = fopen(mappingsFile, 'w');
    for l = 1:numel(sections)
        for k = 1:numel(sections{l})
            fprintf(fid, '%s\n',sections{l}{k});
        end
    end
    fclose(fid);
end

function conditionsFile = generateConditionsFile(rootDir, imageName, materialDiffuseSPD, materialSpecularSPD, materialAlpha, pointLightSPD, areaLightSPD, outsideLightSPD, frontWallLightSPD, ceilingLightSPD)
    resourcesDir  = fullfile(rootDir, '3.Resources/NewStuff', filesep); 
    conditionKeys = {'imageName', ...
                     'objDiffuseReflectanceSPD', ...
                     'objSpecularReflectanceSPD', ...
                     'objMaterialAlpha', ...
                     'pedestalDiffuseReflectanceSPD', ...
                     'shadowingBottomPlaneDiffuseReflectanceSPD', ...
                     'shadowingTopPlaneDiffuseReflectanceSPD', ...
                     'darkCheckDiffuseReflectanceSPD', ...
                     'darkCheckSpecularReflectanceSPD', ...
                     'lightCheckDiffuseReflectanceSPD', ...
                     'lightCheckSpecularReflectanceSPD', ...
                     'roomDiffuseReflectanceSPD', ...
                     'backWallDiffuseReflectanceSPD', ...
                     'backWallSpecularReflectanceSPD', ...
                     'pointLightIlluminantSPD', ...
                     'areaLightIlluminantSPD', ...
                     'outsideLightIlluminantSPD', ...
                     'areaLightSamplingWeight', ...
                     'pointLightSamplingWeight' ...
                    };
                   
    areaLightSamplingWeight = 1.0;
    pointLightSamplingWeight = 1.0;
    
    conditionValues = {imageName, ... 
                       [resourcesDir materialDiffuseSPD], ...          % blobbie diffuse SPD
                       [resourcesDir materialSpecularSPD], ...         % blobbie specular SPD
                       materialAlpha, ...                              % blobbie alpha
                       [resourcesDir 'NeutralDay-Gray_0_2.spd'], ...   % pedestal diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % shadowingBottomPlaneDiffuseReflectanceSPD
                       [resourcesDir 'NeutralDay-Gray_0_5.spd'], ...   % shadowingTopPlaneDiffuseReflectanceSPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_9.spd'], ...   % light check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % light check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % room diffuse SPD               ------ > used to be: 'NeutralDay-Gray_0_6.spd'
                       [resourcesDir 'NeutralDay-Gray_0_4.spd'], ...    % backwall diffuse SPD          ------ > used to be: 'NeutralDay-Gray_0_5.spd'
                       [resourcesDir 'NeutralDay-Gray_0_0.spd'], ...    % backwall specular SPD: NeutralDay-Gray_0_5 for max-effect mirror backwall, NeutralDay-Gray_0_0.spd for matte
                       [resourcesDir pointLightSPD], ...                % point lights SPD: point lights on: NeutralDay.spd / point lights off: NeutralDay_0_0.spd
                       [resourcesDir areaLightSPD], ...                 % area lights SPD:  area lights on: NeutralDay.spd / area lights off: NeutralDay_0_0.spd
                       [resourcesDir outsideLightSPD], ...
                       areaLightSamplingWeight, ...
                       pointLightSamplingWeight ...
                       };
    
    theConditionValues = conditionValues;
    conditionsFile = fullfile(rootDir, '1.Conditions', 'conditions_123.txt');
    conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, theConditionValues);
end


function plotReflectanceFunctionsUsed(specularReflectanceFunctionFileNames, diffuseReflectanceFunctionFileNames)
    figure(1);
    clf;
    
    [wavelengths, diffuseSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', diffuseReflectanceFunctionFileNames{1})); 
    
    for k = 1:numel(specularReflectanceFunctionFileNames)
        subplot(2,4,k);
        [wavelengths, specularSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', specularReflectanceFunctionFileNames{k})); 
        
        
        plot(wavelengths, specularSPD, 'b-');
        hold on;
        plot(wavelengths, diffuseSPD, 'r-');
        plot(wavelengths, specularSPD + diffuseSPD, 'k--');
        legend('specular', 'diffuse', 'spec + diffuse');
        set(gca, 'YLim', [0 1.2]);
    end
    
    drawnow;
end


function [diffuseReflectanceFunctionFileNames, specularReflectanceFunctionFileNames] = generateSamsungReflectanceFunctions

    materialSpecularSPDs = {...
        'FlatSpecularReflectance_0.60.spd', ...
        'FlatSpecularReflectance_0.30.spd', ...
        'FlatSpecularReflectance_0.15.spd' ...
        };
     materialSpecularSPDs = {...
        'FlatSpecularReflectance_0.60.spd', ...
        'FlatSpecularReflectance_0.15.spd' ...
       };
        
        
    materialDiffuseSPDs = {...
        'NeutralDay_BlueGreen_0.45.spd', ...%   'NeutralDay_BlueGreen_0.50.spd', ...
        'NeutralDay_BlueGreen_0.55.spd', ...%   'NeutralDay_BlueGreen_0.60.spd' ...
        'NeutralDay_BlueGreen_0.65.spd', ...%   'NeutralDay_BlueGreen_0.70.spd', ...
        'NeutralDay_BlueGreen_0.75.spd', ...
        };
     materialDiffuseSPDs = { 'NeutralDay_BlueGreen_0.60.spd' };
    
    for k = 1: numel(materialSpecularSPDs)
        [wavelengths, specularSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', materialSpecularSPDs{k}));
        specularReflectanceFunctionFileNames{k} = sprintf('Samsung_%s',materialSpecularSPDs{k});
        WriteSpectrumFile(wavelengths, specularSPD, fullfile('3.Resources', 'NewStuff', specularReflectanceFunctionFileNames{k})); 
    end
    
    scaling = 0.43;
    for k = 1:numel(materialDiffuseSPDs)
        [wavelengths, diffuseSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', materialDiffuseSPDs{k})); 
        diffuseSPD = diffuseSPD * scaling;
        energy = specularSPD + diffuseSPD;
        [maxEnergy, lambdaIndex] = max(energy);
        if (maxEnergy > 1)
            scaling = (1 - specularSPD(lambdaIndex))/diffuseSPD(lambdaIndex);
            error('Need to scale differently some diffuse functions');
            scaledDiffuseSPD = diffuseSPD * scaling;
        else
            scaledDiffuseSPD = diffuseSPD;
        end    
        diffuseReflectanceFunctionFileNames{k} = sprintf('Samsung_%s',materialDiffuseSPDs{k});
        WriteSpectrumFile(wavelengths, scaledDiffuseSPD, fullfile('3.Resources', 'NewStuff', diffuseReflectanceFunctionFileNames{k})); 
    end
end


function hints = generateBatchRendererOptions(imageWidth, imageHeight)
    hints = struct(...
        'imageWidth',               imageWidth, ...
        'imageHeight',              imageHeight, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba' ...
        );
end

function fullColladaFile = fullPathToColladaFile(rootDir, filename)
    currentDir = pwd;
    cd(rootDir);
    cd ..
    %cd('2.ColladaExports');
    cd('2.ColladaExports/HighDynamicRange');
    fullColladaFile = fullfile(pwd, [filename '.dae']);
    cd(currentDir);
end

function [rootDir, renderedScenesDir] = getRootAndParentDirs(mfile)
    [rootDir, ~, ~] = fileparts(which(mfile));
    currDir = pwd;
    cd(rootDir);
    cd ..
    parentDir = pwd;
%    renderedScenesDir = fullfile(parentDir, '4.RenderedScenes');
    renderedScenesDir = '/Users1/Shared/Matlab/RT3scenes/Blobbies/HighDynamicRange';
    cd(currDir);
end

function sensorXYZ = loadXYZCMFs()
    colorMatchingData = load('T_xyz1931.mat');
    sensorXYZ.S = colorMatchingData.S_xyz1931;
    sensorXYZ.T = colorMatchingData.T_xyz1931;
end

