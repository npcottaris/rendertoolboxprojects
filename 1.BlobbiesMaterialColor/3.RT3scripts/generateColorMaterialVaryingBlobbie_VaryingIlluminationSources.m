function generateColorMaterialVaryingBlobbie_VaryingIlluminationSources

    % Setup
    computerName = char(java.net.InetAddress.getLocalHost.getHostName);
    if strcmp(computerName, 'Ithaka.lan')
        setpref('RenderToolbox3', 'workingFolder', '/Volumes/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    else
        setpref('RenderToolbox3', 'workingFolder', '/Users/Shared/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    end
    
    renderedImageParams = struct(...
        'width', 640, ...
        'height', 480, ...
        'sampleCount', 256 ...
        );
    
    [rootDir, renderedScenesDir] = getRootAndParentDirs(mfilename());
    hints = generateBatchRendererOptions(renderedImageParams.width, renderedImageParams.height);
    ChangeToWorkingFolder(hints);
    
    % Collada file
    colladaFile = 'Blobbie8subs';
    colladaFiles = {'Blobbie9SubsVeryLowFreqShadowed'};
    
    fullColladaFile = fullPathToColladaFile(rootDir, colladaFile);
    
    % Generate mappings file
    mappingsFile = generateMappingsFile(rootDir, renderedImageParams.sampleCount);
    
    areaTypeLights = [0, 1, 2];
    frontWallLight = [0 1];
    ceilingLight = [0 1];
    
    materialAlpha = 0.05;
    
    for k = 1:numel(areaTypeLights)
        if (areaTypeLights(k) == 0)
            pointLightSPD = 'NeutralDay_0.0.spd';
            areaLightSPD  = 'NeutralDay_0.0.spd';
        elseif (areaTypeLights(k) == 1)
            pointLightSPD = 'NeutralDay_0.0.spd';
            areaLightSPD  = 'NeutralDay_0.333.spd';
        else
            pointLightSPD = 'NeutralDay_0.333.spd';
            areaLightSPD  = 'NeutralDay_0.0.spd';
        end
        
        for l = 1:numel(frontWallLight)
            if (frontWallLight(l) == 0)
                frontWallLightSPD = 'NeutralDay_0.0.spd';
            else
                frontWallLightSPD = 'NeutralDay_0.333.spd';
            end
            for m = 1:numel(ceilingLight)
                if (ceilingLight(m) == 0)
                    ceilingLightSPD = 'NeutralDay_0.0.spd';
                else
                    ceilingLightSPD = 'NeutralDay_0.333.spd';
                end
                
                % Generate conditions file
                imageName = [colladaFile '_conds123'];
                conditionsFile = generateConditionsFile(rootDir, imageName, materialAlpha, ...
                    pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD);
                
                % Action
                nativeSceneFile   = MakeSceneFiles(fullColladaFile, conditionsFile, mappingsFile, hints);
                radianceDataFiles = BatchRender(nativeSceneFile, hints);
    
                conditionString = sprintf('_alpha_%3.3f_area%d_front%d_ceiling%d', materialAlpha, areaTypeLights(k), frontWallLight(l), ceilingLight(m));
                montageFile   = fullfile(renderedScenesDir, [colladaFile conditionString '.tiff']);
        
                % Tone mapping settings for tiff image
                toneMapFactor = 4.0;  % 4 is good for arealights only
                isScale       = true;
                [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScale, hints); 
    
                % Move radiance file to renderedScenes local dir
                system(sprintf('mv %s %s/', radianceDataFiles{1}, renderedScenesDir));
                cd(rootDir);
    
            end
        end
    end
    
end

function mappingsFile = generateMappingsFile(rootDir, sampleCount)

    rotationAngleInDegrees = 45;
    pedestalRotationString = sprintf('    pedestal:rotate|sid=rotationZ = 0 0 1 %d', rotationAngleInDegrees);
    blobbieRotationString  = sprintf('    blobbie:rotate|sid=rotationZ = 0 0 1 %d', rotationAngleInDegrees);
    
    sections{1} = { ...
        '% Camera position and properties'
        'Collada {'
        '    % Camera geometric object'
        '    % flip handedness to match Blender-Collada output to Mitsuba and PBRT'
        '    Camera:scale|sid=scale = -1 1 1'
        ' '
        '    % Camera FOV'
        '    Camera-camera:optics:technique_common:perspective:xfov = 36'
        ''
        pedestalRotationString
        blobbieRotationString
        '}'
        ''
        };
     
    sections{2} = { ...
        '% Lights'
        'Generic {'
        '    frontLeftAreaLamp-light:light:point'
        '    frontLeftAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontLeftAreaLamp-mesh:light:area'
        '    frontLeftAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        ''
        '    frontRightAreaLamp-light:light:point'
        '    frontRightAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontRightAreaLamp-mesh:light:area'
        '    frontRightAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        ''
        '    frontWall-mesh-mesh:light:area'
        '    frontWall-mesh-mesh:intensity.spectrum = (frontWallAreaLightIlluminantSPD)'
        ''
        '    ceiling-mesh-mesh:light:area'
        '    ceiling-mesh-mesh:intensity.spectrum = (ceilingAreaLightIlluminantSPD)'
        '}'
        ''
        };
    
    sampleCountString = sprintf('    Camera-camera_sampler:sampleCount.integer  = %d', sampleCount);
    sections{3} = { ...
        '% Light Integration and Sampling'
        'Mitsuba {'
        '    % ============== Light integrator section ===================='
        '    integrator:integrator:bdpt'
        '    integrator:maxDepth.integer     = 7'
        '    integrator:sampleDirect.boolean = true'
        '    integrator:lightImage.boolean   = true'
        ''
        '    % ======== Camera sampler settings ============='
        '    Camera-camera_sampler:sampler:ldsampler'
        sampleCountString
        '}'
        ''
        };
    
    sections{4} = { ...
        '% Materials'
        'Generic {'
        '    blobbieMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    blobbieMaterial-material:variant.string = balanced'
        '    blobbieMaterial-material:diffuseReflectance.spectrum = (objDiffuseReflectanceSPD)'
        '    blobbieMaterial-material:specularReflectance.spectrum = (objSpecularReflectanceSPD)'
        '    blobbieMaterial-material:ensureEnergyConservation.boolean = true'
        '    blobbieMaterial-material:alphaU.float = (objMaterialAlpha)'
        '    blobbieMaterial-material:alphaV.float = (objMaterialAlpha)'
        ''
        '    pedestalMaterial-material:bsdf:roughplastic'
        '    pedestalMaterial-material:alpha.float = 0.003'
        '    pedestalMaterial-material:diffuseReflectance.spectrum  = (pedestalDiffuseReflectanceSPD)'
        '    pedestalMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    roomMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    roomMaterial-material:variant.string = balanced'
        '    roomMaterial-material:diffuseReflectance.spectrum  = (roomDiffuseReflectanceSPD)'
        '    roomMaterial-material:specularReflectance.spectrum = 300:0 800:0'
        '    roomMaterial-material:ensureEnergyConservation.boolean = true'
        '    roomMaterial-material:alphaU.float = 0.1'
        '    roomMaterial-material:alphaV.float = 0.1'
        ''
        '    backWallMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    backWallMaterial-material:variant.string = balanced'
        '    backWallMaterial-material:diffuseReflectance.spectrum  = (backWallDiffuseReflectanceSPD)'
        '    backWallMaterial-material:specularReflectance.spectrum = (backWallSpecularReflectanceSPD)'
        '    backWallMaterial-material:ensureEnergyConservation.boolean = true'
        '    backWallMaterial-material:alphaU.float = 0.01'
        '    backWallMaterial-material:alphaV.float = 0.01'
        ''
        '    darkCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    darkCheckMaterial-material:variant.string = balanced'
        '    darkCheckMaterial-material:diffuseReflectance.spectrum  = (darkCheckDiffuseReflectanceSPD)'
        '    darkCheckMaterial-material:specularReflectance.spectrum = (darkCheckSpecularReflectanceSPD)'
        '    darkCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    darkCheckMaterial-material:alphaU.float = 0.01'
        '    darkCheckMaterial-material:alphaV.float = 0.01'
        ''
        '    lightCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    lightCheckMaterial-material:variant.string = balanced'
        '    lightCheckMaterial-material:diffuseReflectance.spectrum  = (lightCheckDiffuseReflectanceSPD)'
        '    lightCheckMaterial-material:specularReflectance.spectrum = (lightCheckSpecularReflectanceSPD)'
        '    lightCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    lightCheckMaterial-material:alphaU.float = 0.01'
        '    lightCheckMaterial-material:alphaV.float = 0.01'
        ''
        '}'
        ''
        };
    
    mappingsFile = fullfile(rootDir, '2.Mappings', 'mappings_123.txt');
    
    fid = fopen(mappingsFile, 'w');
    for l = 1:numel(sections)
        for k = 1:numel(sections{l})
            fprintf(fid, '%s\n',sections{l}{k});
        end
    end
    fclose(fid);
end

function conditionsFile = generateConditionsFile(rootDir, imageName, materialAlpha, pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD)
    resourcesDir  = fullfile(rootDir, '3.Resources', filesep); 
    conditionKeys = {'imageName', ...
                       'objMaterialAlpha', ...
                       'objDiffuseReflectanceSPD', ...
                       'objSpecularReflectanceSPD', ...
                       'pedestalDiffuseReflectanceSPD', ...
                       'darkCheckDiffuseReflectanceSPD', ...
                       'darkCheckSpecularReflectanceSPD', ...
                       'lightCheckDiffuseReflectanceSPD', ...
                       'lightCheckSpecularReflectanceSPD', ...
                       'roomDiffuseReflectanceSPD', ...
                       'backWallDiffuseReflectanceSPD', ...
                       'backWallSpecularReflectanceSPD', ...
                       'pointLightIlluminantSPD', ...
                       'areaLightIlluminantSPD', ...
                       'frontWallAreaLightIlluminantSPD' ...
                       'ceilingAreaLightIlluminantSPD' ...
                       };
                   
    conditionValues = {imageName, ... 
                       materialAlpha, ...
                       [resourcesDir 'NeutralDay-Acqua_1.0.spd'], ...
                       [resourcesDir 'FlatSpecularReflectance_0.10.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_4.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_9.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_6.spd'], ...
                       [resourcesDir 'NeutralDay-Gray_0_5.spd'] ...
                       [resourcesDir 'NeutralDay-Gray_0_0.spd'] ...   % NeutralDay-Gray_0_5 for max-effect mirror backwall, NeutralDay-Gray_0_0.spd for matte
                       [resourcesDir pointLightSPD] ...        % point lights on: NeutralDay.spd / point lights off: NeutralDay_0_0.spd
                       [resourcesDir areaLightSPD] ...       % area lights on: NeutralDay.spd / area lights off: NeutralDay_0_0.spd
                       [resourcesDir frontWallLightSPD] ...       % the front wall area light (0.1)
                       [resourcesDir ceilingLightSPD] ...       % the ceiling area light (0.1)
                       };
    
    theConditionValues = conditionValues;
    conditionsFile = fullfile(rootDir, '1.Conditions', 'conditions_123.txt');
    conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, theConditionValues);
end

function hints = generateBatchRendererOptions(imageWidth, imageHeight)
    hints = struct(...
        'imageWidth',               imageWidth, ...
        'imageHeight',              imageHeight, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba' ...
        );
end

function fullColladaFile = fullPathToColladaFile(rootDir, filename)
    currentDir = pwd;
    cd(rootDir);
    cd ..
    cd('2.ColladaExports/HighDynamicRange');
    fullColladaFile = fullfile(pwd, [filename '.dae']);
    cd(currentDir);
end

function [rootDir, renderedScenesDir] = getRootAndParentDirs(mfile)
    [rootDir, ~, ~] = fileparts(which(mfile));
    currDir = pwd;
    cd(rootDir);
    cd ..
    parentDir = pwd;
    renderedScenesDir = fullfile(parentDir, '4.RenderedScenes');
    renderedScenesDir = '/Users1/Shared/Matlab/RT3scenes/Blobbies';
    cd(currDir);
end