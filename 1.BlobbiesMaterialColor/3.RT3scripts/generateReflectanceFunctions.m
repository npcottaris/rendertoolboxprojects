function generateReflectanceFunctions

    % Generate illuminants
    % Neutral illuminant (Daylight)
    [S, spd] = ReadSpectrum('D65.spd');
    wavelengthAxis = SToWls(S);
    neutralDay = WriteSpectrumFile(wavelengthAxis, spd, ...
        fullfile('3.Resources', 'NewStuff', 'NeutralDay.spd'));
    figure(1);
    clf;
    plot(wavelengthAxis, spd, 'rs-');
    title('neutral');
    drawnow;
    
    
    for k = 0.0 : 0.1: 1.0
        scaledSPD = k * spd;
        neutralDay = WriteSpectrumFile(wavelengthAxis, scaledSPD, fullfile('3.Resources', 'NewStuff', sprintf('NeutralDay_%2.2f.spd', k)));
    end
    
    
    
    
    RGBs = {[0 1 0], [0 0 1]};
    RGBNames = {'Green', 'Blue'};
        
    illumNames = {'NeutralDay'};
    illuminants = {neutralDay};
    

    nIlluminants    = numel(illuminants);
    nRGBs           = numel(RGBs);
    promotions      = cell(nIlluminants, nRGBs);
    SOuts           = cell(nIlluminants, nRGBs);
    RGBOuts         = cell(nIlluminants, nRGBs);
    dataFiles       = cell(nIlluminants, nRGBs);
    
    hints.renderer = 'Mitsuba';
    figure(2);
    clf;
    for illumIndex = 1:nIlluminants
        for rgbIndex = 1:nRGBs
            [promoted, S, RGB, dataFile] = ...
              PromoteRGBReflectance(RGBs{rgbIndex}, illuminants{illumIndex}, hints);
          
            % add a dc offest across all wavelengths
            promoted = promoted + 0.2;
            promotions{illumIndex, rgbIndex} = promoted;
            SOuts{illumIndex, rgbIndex} = S;
            RGBOuts{illumIndex, rgbIndex} = RGB;
            dataFiles{illumIndex, rgbIndex} = dataFile;
                
            wavelengthAxis = SToWls(S);
            subplot(nRGBs, nIlluminants, (rgbIndex-1)*nIlluminants + illumIndex);
            plot(wavelengthAxis, promoted, 'rs-');
            set(gca, 'XLim', [370 710], 'YLim', [0 1.2]);
            reflectanceFunctionName = sprintf('%s-%s.spd', illumNames{illumIndex}, RGBNames{rgbIndex});
            title(reflectanceFunctionName);
            drawnow;
                
            WriteSpectrumFile(wavelengthAxis, promoted, fullfile('3.Resources', 'NewStuff', reflectanceFunctionName)); 
                
        end
    end
    
    % Now generate mixed versions that vary between green and blue
    [wavelengths, blueSPD]  = ReadSpectrum(fullfile('3.Resources', 'NewStuff', 'NeutralDay-Blue.spd'));
    [wavelengths, greenSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', 'NeutralDay-Green.spd'));
    
    [wavelengths, oldReflectanceFunction] = ReadSpectrum(fullfile('3.Resources', 'OldBlobbieColor.spd'));
    
    figure(3);
    clf;
    mixFactors = 0.10 +[0.0 : 0.05 : 0.80];
    for k = 1:numel(mixFactors)
        mixedSPD = blueSPD * mixFactors(k) + greenSPD * (1-mixFactors(k));
        file = WriteSpectrumFile(wavelengths, mixedSPD, fullfile('3.Resources', 'NewStuff', sprintf('%s_BlueGreen_%2.2f.spd', illumNames{1}, mixFactors(k))));
        [wavelengths, reflectanceFunction] = ReadSpectrum(file);
        subplot(numel(mixFactors),1,k);
        plot(wavelengths, reflectanceFunction, 'rs-');
        hold on;
        plot(wavelengths, oldReflectanceFunction, 'ks');
        hold off;
        title(sprintf('mix factor: %2.2f', mixFactors(k)));
    end
    
    
    
    % Now generate the gray reflectance functions
    RGBs = { ...
            0.0*[1 1 1], 0.1*[1 1 1], 0.2*[1 1 1], 0.3*[1 1 1], 0.4*[1 1 1], 0.5*[1 1 1], ...
            0.6*[1 1 1], 0.7*[1 1 1], 0.8*[1 1 1], 0.9*[1 1 1], 0.95*[1 1 1]...
            };
    RGBNames = {...
            'Gray_0_0', 'Gray_0_1', 'Gray_0_2', 'Gray_0_3', 'Gray_0_4', 'Gray_0_5', ...
            'Gray_0_6', 'Gray_0_7', 'Gray_0_8', 'Gray_0_9', 'Gray_0_95'...
            };
        

    nIlluminants    = numel(illuminants);
    nRGBs           = numel(RGBs);
    promotions      = cell(nIlluminants, nRGBs);
    SOuts           = cell(nIlluminants, nRGBs);
    RGBOuts         = cell(nIlluminants, nRGBs);
    dataFiles       = cell(nIlluminants, nRGBs);

    figure(3);
    clf;
    for illumIndex = 1:nIlluminants
        for rgbIndex = 1:nRGBs
            [promoted, S, RGB, dataFile] = ...
              PromoteRGBReflectance(RGBs{rgbIndex}, illuminants{illumIndex}, hints);
            promotions{illumIndex, rgbIndex} = promoted;
            SOuts{illumIndex, rgbIndex} = S;
            RGBOuts{illumIndex, rgbIndex} = RGB;
            dataFiles{illumIndex, rgbIndex} = dataFile;
                
            wavelengthAxis = SToWls(S);
            subplot(nRGBs, nIlluminants, (rgbIndex-1)*nIlluminants + illumIndex);
            plot(wavelengthAxis, promoted, 'rs-');
            set(gca, 'XLim', [370 710], 'YLim', [0 1.2]);
            reflectanceFunctionName = sprintf('%s-%s.spd', illumNames{illumIndex}, RGBNames{rgbIndex});
            title(reflectanceFunctionName);
            drawnow;
                
            WriteSpectrumFile(wavelengthAxis, promoted, fullfile('3.Resources', 'NewStuff', reflectanceFunctionName)); 
                
        end
    end
    
    
    % Finally the flat specular reflections
    for specularMagnitude = 0.0 : 0.05 : 1.0
        reflectanceFunction = specularMagnitude + wavelengths*0;
        file = WriteSpectrumFile(wavelengthAxis, reflectanceFunction, fullfile('3.Resources', 'NewStuff', sprintf('FlatSpecularReflectance_%2.2f.spd', specularMagnitude)));
    end
    
end