function shortenR1FileNames

    % --------------------- Lighting --------------------------
    areaTypeLights = 0; % [0, 1, 2];
    areaLightSPD   = 'NeutralDay_0.00.spd';
    pointLightSPD  = 'NeutralDay_0.00.spd';
    
    frontWallLight = 0; % [0 1];
    frontWallLightSPD = 'NeutralDay_0.00.spd';
    
    ceilingLight   = 1; % [0 1];
    ceilingLightSPD = 'NeutralDay_0.30.spd';  
    % --------------------- Lighting --------------------------
    
    colladaFile = 'Blobbie8subs';
    materialAlphas = [0.001 0.02 0.05 0.10 0.15 0.20 0.40];
    materialSpecularSPDs = {'R1_FlatSpecularReflectance_0.30.spd'};
    materialLambdas = [0.45 0.50 0.55 0.60 0.65 0.70 0.75];
    
    for m = 1:numel(materialLambdas)
        materialDiffuseSPDs{m} = sprintf('R1_NeutralDay_BlueGreen_%2.2f.spd', materialLambdas(m));
    end
    
    index = 0;
    for k = 1:numel(materialAlphas)
        for l = 1:numel(materialSpecularSPDs)
            for m = 1:numel(materialDiffuseSPDs)
                
                % unpack varying  params
                alpha       = materialAlphas(k);         % alpha of specular reflectance
                specularSPD = materialSpecularSPDs{l};   % spectrum of specular reflectance (specular reflection magnitude, since it is a flat SPD)
                diffuseSPD  = materialDiffuseSPDs{m};    % spectrum of diffuse reflectance (color)
                
                conditionString = sprintf('_R1____%s____%s___alpha_%3.3f___Lights_area%d_front%d_ceiling%d', ...
                    diffuseSPD, specularSPD, alpha, areaTypeLights, frontWallLight, ceilingLight);
                
                lambda = materialLambdas(m);
                newConditionString = sprintf('R1_Alpha_%2.4f__Lambda_%2.3f', alpha,lambda);
                
                oldTiffFileName = [colladaFile conditionString '.tiff'];
                newTiffFileName = [newConditionString '.tiff'];
                
                oldMATFileName = [colladaFile conditionString '.mat'];
                newMATFileName = [newConditionString '.mat'];
                
                index = index + 1;
                disp(sprintf('[%d]. %s\n%s\n\n', index, oldTiffFileName, newTiffFileName));

                system(sprintf('cp ../4.RenderedScenes/%s ../4.RenderedScenes/R1set/%s', oldTiffFileName, newTiffFileName));
                system(sprintf('cp ../4.RenderedScenes/%s ../4.RenderedScenes/R1set/%s', oldMATFileName, newMATFileName));
            end
        end
    end
                
end