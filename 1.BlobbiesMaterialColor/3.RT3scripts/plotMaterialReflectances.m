function plotMaterialReflectances
    
    materialDiffuseSPDs = {...
      %  'NeutralDay_BlueGreen_0.50.spd', ...
      %  'NeutralDay_BlueGreen_0.55.spd', ...
        'NeutralDay_BlueGreen_0.60.spd' ...
      %  'NeutralDay_BlueGreen_0.65.spd', ...
      %  'NeutralDay_BlueGreen_0.70.spd', ...
      %  'NeutralDay_BlueGreen_0.75.spd', ...
        };
    
    materialSpecularSPDs = { ...
        'FlatSpecularReflectance_0.00.spd', ...
        'FlatSpecularReflectance_0.05.spd', ...
        'FlatSpecularReflectance_0.10.spd', ...
        'FlatSpecularReflectance_0.15.spd', ...
        'FlatSpecularReflectance_0.20.spd', ...
        'FlatSpecularReflectance_0.30.spd', ...
        'FlatSpecularReflectance_0.40.spd' ...
        'FlatSpecularReflectance_0.50.spd', ...
        'FlatSpecularReflectance_0.60.spd' ...exit
        colordef black
            area(wavelengths, specularSPD, 'FaceColor', [0.7 0.7 0.7], 'EdgeColor', [1 1 1]);
            hold on;
            area(wavelengths, diffuseSPD, 'FaceColor',[0.3 .7 .6], 'EdgeColor', [0.3 1 1]);
            plot(wavelengths, specularSPD, 'w--', 'LineWidth', 2.0);
            plot(wavelengths, diffuseSPD+specularSPD, 'r-');
            set(gca, 'YLim', [0.01 1.0], 'XLim', [min(wavelengths)+1 max(wavelengths)-1]);
            set(gca, 'XTick', [400:50:750], 'YTick', [0:0.2:1.0]);
            ylabel('reflectance', 'color', 'w');
            xlabel('wavelength', 'Color', 'w');
            if (l > 1)
                set(gca, 'YTickLabel', []);
                ylabel('');
            end
            grid on;
            box on;
            h = legend('specular', 'diffuse');
            set(h, 'TextColor', 'k')
            
        end
    end
   
   set(gcf, 'Color', 'k');
    
    % Set fonts for all axes, legends, and titles
    NicePlot.setFontSizes(hFig, 'FontSize', 12); 
    
    NicePlot.exportFigToPDF('Reflectances.pdf',hFig,300);
end

function FilledAreaPlot(x,y, faceColor, edgeColor, XLims, YLims)
    px = [x(1), reshape(x, [1 length(x)]), x(end)]; % make closed patch
    py = [0, reshape(y, [1 length(y)]), 0];
    pz = -10*eps*ones(1,length(x)+2);
    patch(px,py,pz,'FaceColor',faceColor,'EdgeColor',edgeColor);
    if (~notDefined('XLims'))
        set(gca, 'XLim', XLims);
    end
    if (~notDefined('YLims'))
        set(gca, 'YLim', YLims);
    end
    set(gca, 'FontSize', 12, 'FontName', 'Helvetica');
end

