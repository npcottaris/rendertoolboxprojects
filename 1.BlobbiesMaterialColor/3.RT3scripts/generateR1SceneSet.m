function generateR1SceneSet

    reflectanceFunctionFileNames = generateR1reflectanceFunctions();
    plotReflectanceFunctionsUsed(reflectanceFunctionFileNames) ;
    
    % Setup
    computerName = char(java.net.InetAddress.getLocalHost.getHostName);
    if strcmp(computerName, 'Ithaka.lan')
        setpref('RenderToolbox3', 'workingFolder', '/Volumes/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    else
        setpref('RenderToolbox3', 'workingFolder', '/Users/Shared/SDXC_64GB/ScratchDisks/RenderToolbox3ScratchDisk');
    end
    
    renderedImageParams = struct(...
        'width', 960/3, ...
        'height', 720/3, ...
        'sampleCount', 1024/8, ...
        'pixelSamples', 32 ...
        );
    
     renderedImageParams = struct(...
         'width', 960, ...
         'height', 720, ...
         'sampleCount', 1024, ...
         'pixelSamples', 2048 ...
         );
    
    [rootDir, renderedScenesDir] = getRootAndParentDirs(mfilename());
    hints = generateBatchRendererOptions(renderedImageParams.width, renderedImageParams.height);
    ChangeToWorkingFolder(hints);
    
    % Collada file
    colladaFile = 'Blobbie8subs';
    fullColladaFile = fullPathToColladaFile(rootDir, colladaFile);
    
    % Generate mappings file
    mappingsFile = generateMappingsFile(rootDir, renderedImageParams.sampleCount, renderedImageParams.pixelSamples);
    
    
    % --------------------- Lighting --------------------------
    areaTypeLights = 0; % [0, 1, 2];
    areaLightSPD   = 'NeutralDay_0.00.spd';
    pointLightSPD  = 'NeutralDay_0.00.spd';
    
    frontWallLight = 0; % [0 1];
    frontWallLightSPD = 'NeutralDay_0.00.spd';
    
    ceilingLight   = 1; % [0 1];
    ceilingLightSPD = 'NeutralDay_0.30.spd';  
    % --------------------- Lighting --------------------------
    
    materialAlphas = [0.007 0.02 0.05 0.10 0.15 0.20 0.40];
    materialSpecularSPDs = {reflectanceFunctionFileNames{1}};
    materialDiffuseSPDs = {reflectanceFunctionFileNames{2:end}};
    

    for k = 1:numel(materialAlphas)
        for l = 1:numel(materialSpecularSPDs)
            for m = 1:numel(materialDiffuseSPDs)
                % unpack varying  params
                alpha       = materialAlphas(k);         % alpha of specular reflectance
                specularSPD = materialSpecularSPDs{l};   % spectrum of specular reflectance (specular reflection magnitude, since it is a flat SPD)
                diffuseSPD  = materialDiffuseSPDs{m};    % spectrum of diffuse reflectance (color)
            
                % Generate unique condition string
                conditionString = sprintf('_R1____%s____%s___alpha_%3.3f___Lights_area%d_front%d_ceiling%d', ...
                    diffuseSPD, specularSPD, alpha, areaTypeLights, frontWallLight, ceilingLight);
                
                % Generate conditions file
                imageName = [colladaFile conditionString];
                conditionsFile = generateConditionsFile(rootDir, imageName, diffuseSPD, specularSPD, alpha, ...
                    pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD);

                % Action
                nativeSceneFile   = MakeSceneFiles(fullColladaFile, conditionsFile, mappingsFile, hints);
                radianceDataFiles = BatchRender(nativeSceneFile, hints);

                % Generate tiff preview
                montageFile = fullfile(renderedScenesDir, [colladaFile conditionString '.tiff']);
                % Tone mapping settings for tiff image
                toneMapFactor = 4.0;  % 4 is good for arealights
                isScale       = true;
                [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScale, hints);
                
                % Move radiance file to renderedScenes local dir
                system(sprintf('mv %s %s/', radianceDataFiles{1}, renderedScenesDir));
                cd(rootDir);
            end % for m
        end % for l
    end % for k
end


function mappingsFile = generateMappingsFile(rootDir, sampleCount, pixelSamples)

    rotationAngleInDegrees = 45;
    pedestalRotationString = sprintf('    pedestal:rotate|sid=rotationZ = 0 0 1 %d', rotationAngleInDegrees);
    blobbieRotationString  = sprintf('    blobbie:rotate|sid=rotationZ = 0 0 1 %d', -rotationAngleInDegrees);
    
    sections{1} = { ...
        '% Camera position and properties'
        'Collada {'
        '    % Camera geometric object'
        '    % flip handedness to match Blender-Collada output to Mitsuba and PBRT'
        '    Camera:scale|sid=scale = -1 1 1'
        ' '
        '    % Camera FOV'
        '    Camera-camera:optics:technique_common:perspective:xfov = 36'
        ''
        blobbieRotationString
        pedestalRotationString
        '}'
        ''
        };
     
    sections{2} = { ...
        '% Lights'
        'Generic {'
        '    frontLeftAreaLamp-light:light:point'
        '    frontLeftAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontLeftAreaLamp-mesh:light:area'
        '    frontLeftAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        ''
        '    frontRightAreaLamp-light:light:point'
        '    frontRightAreaLamp-light:intensity.spectrum  = (pointLightIlluminantSPD)'
        '    frontRightAreaLamp-mesh:light:area'
        '    frontRightAreaLamp-mesh:intensity.spectrum = (areaLightIlluminantSPD)'
        ''
        '    frontWall-mesh-mesh:light:area'
        '    frontWall-mesh-mesh:intensity.spectrum = (frontWallAreaLightIlluminantSPD)'
        ''
        '    ceiling-mesh-mesh:light:area'
        '    ceiling-mesh-mesh:intensity.spectrum = (ceilingAreaLightIlluminantSPD)'
        '}'
        ''
        };
    
    sampleCountString  = sprintf('    Camera-camera_sampler:sampleCount.integer  = %d', sampleCount);
    pixelSamplesString = sprintf('    Camera-camera_sampler:pixelSamples.integer = %d', pixelSamples);
    sections{3} = { ...
        '% Light Integration and Sampling'
        'Mitsuba {'
        '    % ============== Light integrator section ===================='
        '    integrator:integrator:bdpt'
        '    integrator:maxDepth.integer     = 7'
        '    integrator:sampleDirect.boolean = true'
        '    integrator:lightImage.boolean   = true'
        ''
        '    % ======== Camera sampler settings ============='
        '    Camera-camera_sampler:sampler:ldsampler'
        sampleCountString
        pixelSamplesString
        '}'
        ''
        };
    
    sections{4} = { ...
        '% Materials'
        'Generic {'
        '    blobbieMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    blobbieMaterial-material:variant.string = balanced'
        '    blobbieMaterial-material:diffuseReflectance.spectrum = (objDiffuseReflectanceSPD)'
        '    blobbieMaterial-material:specularReflectance.spectrum = (objSpecularReflectanceSPD)'
        '    blobbieMaterial-material:ensureEnergyConservation.boolean = true'
        '    blobbieMaterial-material:alphaU.float = (objMaterialAlpha)'
        '    blobbieMaterial-material:alphaV.float = (objMaterialAlpha)'
        ''
        '    pedestalMaterial-material:bsdf:roughplastic'
        '    pedestalMaterial-material:alpha.float = 0.003'
        '    pedestalMaterial-material:diffuseReflectance.spectrum  = (pedestalDiffuseReflectanceSPD)'
        '    pedestalMaterial-material:ensureEnergyConservation.boolean = true'
        ''
        '    roomMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    roomMaterial-material:variant.string = balanced'
        '    roomMaterial-material:diffuseReflectance.spectrum  = (roomDiffuseReflectanceSPD)'
        '    roomMaterial-material:specularReflectance.spectrum = 300:0 800:0'
        '    roomMaterial-material:ensureEnergyConservation.boolean = true'
        '    roomMaterial-material:alphaU.float = 0.1'
        '    roomMaterial-material:alphaV.float = 0.1'
        ''
        '    backWallMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    backWallMaterial-material:variant.string = balanced'
        '    backWallMaterial-material:diffuseReflectance.spectrum  = (backWallDiffuseReflectanceSPD)'
        '    backWallMaterial-material:specularReflectance.spectrum = (backWallSpecularReflectanceSPD)'
        '    backWallMaterial-material:ensureEnergyConservation.boolean = true'
        '    backWallMaterial-material:alphaU.float = 0.01'
        '    backWallMaterial-material:alphaV.float = 0.01'
        ''
        '    darkCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    darkCheckMaterial-material:variant.string = balanced'
        '    darkCheckMaterial-material:diffuseReflectance.spectrum  = (darkCheckDiffuseReflectanceSPD)'
        '    darkCheckMaterial-material:specularReflectance.spectrum = (darkCheckSpecularReflectanceSPD)'
        '    darkCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    darkCheckMaterial-material:alphaU.float = 0.01'
        '    darkCheckMaterial-material:alphaV.float = 0.01'
        ''
        '    lightCheckMaterial-material:material:anisoward'
        '    % energy balanced at all angled (no energy loss)'
        '    lightCheckMaterial-material:variant.string = balanced'
        '    lightCheckMaterial-material:diffuseReflectance.spectrum  = (lightCheckDiffuseReflectanceSPD)'
        '    lightCheckMaterial-material:specularReflectance.spectrum = (lightCheckSpecularReflectanceSPD)'
        '    lightCheckMaterial-material:ensureEnergyConservation.boolean = true'
        '    lightCheckMaterial-material:alphaU.float = 0.01'
        '    lightCheckMaterial-material:alphaV.float = 0.01'
        ''
        '}'
        ''
        };
    
    mappingsFile = fullfile(rootDir, '2.Mappings', 'mappings_123.txt');
    
    fid = fopen(mappingsFile, 'w');
    for l = 1:numel(sections)
        for k = 1:numel(sections{l})
            fprintf(fid, '%s\n',sections{l}{k});
        end
    end
    fclose(fid);
end

function conditionsFile = generateConditionsFile(rootDir, imageName, materialDiffuseSPD, materialSpecularSPD, materialAlpha, pointLightSPD, areaLightSPD, frontWallLightSPD, ceilingLightSPD)
    resourcesDir  = fullfile(rootDir, '3.Resources/NewStuff', filesep); 
    conditionKeys = {'imageName', ...
                     'objDiffuseReflectanceSPD', ...
                     'objSpecularReflectanceSPD', ...
                     'objMaterialAlpha', ...
                     'pedestalDiffuseReflectanceSPD', ...
                     'darkCheckDiffuseReflectanceSPD', ...
                     'darkCheckSpecularReflectanceSPD', ...
                     'lightCheckDiffuseReflectanceSPD', ...
                     'lightCheckSpecularReflectanceSPD', ...
                     'roomDiffuseReflectanceSPD', ...
                     'backWallDiffuseReflectanceSPD', ...
                     'backWallSpecularReflectanceSPD', ...
                     'pointLightIlluminantSPD', ...
                     'areaLightIlluminantSPD', ...
                     'frontWallAreaLightIlluminantSPD' ...
                     'ceilingAreaLightIlluminantSPD' ...
                    };
                   
    conditionValues = {imageName, ... 
                       [resourcesDir materialDiffuseSPD], ...          % blobbie diffuse SPD
                       [resourcesDir materialSpecularSPD], ...         % blobbie specular SPD
                       materialAlpha, ...                              % blobbie alpha
                       [resourcesDir 'NeutralDay-Gray_0_4.spd'], ...   % pedestal diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % dark check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_9.spd'], ...   % light check diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_1.spd'], ...   % light check specular SPD
                       [resourcesDir 'NeutralDay-Gray_0_6.spd'], ...   % room diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_5.spd'] ...    % backwall diffuse SPD
                       [resourcesDir 'NeutralDay-Gray_0_0.spd'] ...    % backwall specular SPD: NeutralDay-Gray_0_5 for max-effect mirror backwall, NeutralDay-Gray_0_0.spd for matte
                       [resourcesDir pointLightSPD] ...                % point lights SPD: point lights on: NeutralDay.spd / point lights off: NeutralDay_0_0.spd
                       [resourcesDir areaLightSPD] ...                 % area lights SPD:  area lights on: NeutralDay.spd / area lights off: NeutralDay_0_0.spd
                       [resourcesDir frontWallLightSPD] ...            % frontwall lamp SPD: the front wall area light (0.1)
                       [resourcesDir ceilingLightSPD] ...              % ceiling lamp SPD: the ceiling area light (0.1)
                       };
    
    theConditionValues = conditionValues;
    conditionsFile = fullfile(rootDir, '1.Conditions', 'conditions_123.txt');
    conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, theConditionValues);
end


function plotReflectanceFunctionsUsed(reflectanceFunctionFileNames)
    figure(1);
    clf;
    
    [wavelengths, specularSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', reflectanceFunctionFileNames{1})); 
    
    for k = 2:numel(reflectanceFunctionFileNames)
        subplot(2,4,k);
        [wavelengths, diffuseSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', reflectanceFunctionFileNames{k})); 
        
        
        plot(wavelengths, specularSPD, 'k--');
        hold on;
        plot(wavelengths, diffuseSPD, 'r-');
        plot(wavelengths, specularSPD + diffuseSPD, 'b-');
        legend('specular', 'diffuse', 'spec + diffuse');
    end
    
end

function reflectanceFunctionFileNames = generateR1reflectanceFunctions

    materialSpecularSPD = 'FlatSpecularReflectance_0.30.spd';
    materialDiffuseSPDs = {...
        'NeutralDay_BlueGreen_0.45.spd', ...
        'NeutralDay_BlueGreen_0.50.spd', ...
        'NeutralDay_BlueGreen_0.55.spd', ...
        'NeutralDay_BlueGreen_0.60.spd' ...
        'NeutralDay_BlueGreen_0.65.spd', ...
        'NeutralDay_BlueGreen_0.70.spd', ...
        'NeutralDay_BlueGreen_0.75.spd', ...
        };
    
    [wavelengths, specularSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', materialSpecularSPD));
    reflectanceFunctionFileNames{1} = sprintf('R1_%s',materialSpecularSPD);
    WriteSpectrumFile(wavelengths, specularSPD, fullfile('3.Resources', 'NewStuff', reflectanceFunctionFileNames{1})); 
     
    for k = 1:numel(materialDiffuseSPDs)
        [wavelengths, diffuseSPD] = ReadSpectrum(fullfile('3.Resources', 'NewStuff', materialDiffuseSPDs{k})); 
        energy = specularSPD + diffuseSPD;
        [maxEnergy, lambdaIndex] = max(energy);
        if (maxEnergy > 1)
            scaling = (1 - specularSPD(lambdaIndex))/diffuseSPD(lambdaIndex);
            scaledDiffuseSPD = diffuseSPD * scaling;
        else
            scaledDiffuseSPD = diffuseSPD;
        end    
        reflectanceFunctionFileNames{1+k} = sprintf('R1_%s',materialDiffuseSPDs{k});
        WriteSpectrumFile(wavelengths, scaledDiffuseSPD, fullfile('3.Resources', 'NewStuff', reflectanceFunctionFileNames{1+k})); 
    end
 
end


function hints = generateBatchRendererOptions(imageWidth, imageHeight)
    hints = struct(...
        'imageWidth',               imageWidth, ...
        'imageHeight',              imageHeight, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba' ...
        );
end

function fullColladaFile = fullPathToColladaFile(rootDir, filename)
    currentDir = pwd;
    cd(rootDir);
    cd ..
    cd('2.ColladaExports');
    fullColladaFile = fullfile(pwd, [filename '.dae']);
    cd(currentDir);
end

function [rootDir, renderedScenesDir] = getRootAndParentDirs(mfile)
    [rootDir, ~, ~] = fileparts(which(mfile));
    currDir = pwd;
    cd(rootDir);
    cd ..
    parentDir = pwd;
    renderedScenesDir = fullfile(parentDir, '4.RenderedScenes');
    cd(currDir);
end
