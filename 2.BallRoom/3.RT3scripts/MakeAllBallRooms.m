function MakeAllBallRooms

    [rootDir, ~] = fileparts(which(mfilename()));
    workingFolder = '/Volumes/SDXC_128GB/RT3Caches';
    workingFolder = '/Users/nicolas/RT3Caches';
    
    sceneNames = {...
        'FancyBallroom_Depth22cm' ...
        'FancyBallroom_Depth26cm' ...
        'FancyBallroom_Depth30cm' ...
        'FancyBallroom_Depth34cm' ... 
        'FancyBallroom_Depth38cm'
        };
    
    % These must match the settings in Blender/Python for expected geometry
    viewingDistance = 76.4;
    cameraFOV = 33;
    cameraElevation = 19.0;
    cameraWidthToHeightAspectRatio = 1.45;
    displayPanelDims = [51.7988 32.3618];
    
    for k = numel(sceneNames):-1:1
        MakeBallRoom(rootDir, workingFolder, sceneNames{k}, viewingDistance, cameraFOV, cameraElevation, cameraWidthToHeightAspectRatio, displayPanelDims);
    end
    
end


function MakeBallRoom(rootDir, workingFolder, sceneName, viewingDistance, cameraFOV, cameraElevation, cameraWidthToHeightAspectRatio, displayPanelDims)
    
    
    colladaDir = strrep(rootDir,'3.RT3scripts', '2.ColladaExports');
    
    parentSceneFile = sprintf('%s/%s.dae', colladaDir, sceneName);
    conditionsFile  = sprintf('%s/1.Conditions/FancyBallroomConditions.txt', rootDir);
    mappingsFile    = sprintf('%s/2.Mappings/FancyBallroomMappings.txt', rootDir);
    
    fprintf('Collada file: %s\nOutput in %s\n', parentSceneFile, workingFolder);
    
    % The image size to generate. This can be scaled up/down in StereoViewController
    desiredImageWidthInPixels = 1024;
    desiredImageHeightInPixels =  round(desiredImageWidthInPixels/cameraWidthToHeightAspectRatio);
    
    stimulusWidthInCm = 2*viewingDistance*tan(cameraFOV/2/180*pi);
    stimulusHeightInCm = stimulusWidthInCm / cameraWidthToHeightAspectRatio;
    
    if (stimulusWidthInCm > displayPanelDims(1))
        error('Stimulus width (%2.2f) will be larger than display width (%2.2f). Reduce stimulus width!', stimulusWidthInCm, displayPanelDims(1));
    end
    if (stimulusHeightInCm > displayPanelDims(2))
        error('Stimulus height (%2.2f) be larger than display height (%2.2f). Reduce stimulus height!', stimulusHeightInCm, displayPanelDims(2));
    end
    
    if (stimulusWidthInCm <= displayPanelDims(1)) && (stimulusHeightInCm <= displayPanelDims(2))
        fprintf('Stimulus size [%2.1f %2.1f] will fit in display panel [%2.1f %2.1f]\n', stimulusWidthInCm, stimulusHeightInCm, displayPanelDims(1), displayPanelDims(2));
    end
    
    % Choose batch renderer options.
    hints = struct(...
        'imageWidth',               desiredImageWidthInPixels, ...
        'imageHeight',              desiredImageHeightInPixels, ...
        'isCaptureCommandResults',  false, ... % Display renderer output (warnings, progress, etc.), live in Command Window
        'renderer',                 'Mitsuba', ...
        'workingFolder',            fullfile(workingFolder, mfilename()) ...
        );
    
    ChangeToWorkingFolder(hints);

    isScaleGamma = true;
    toneMapFactor = 15;
    
    eyeLabel   = {'Left','Right'};
    eyeXpos    = [ -3.2 3.2];
    
    % match the settings in the python file for exact geometry
    
    cameraDepthPos = -viewingDistance;
    
    stereoView = containers.Map(eyeLabel,eyeXpos);

    for eyeLabel = keys(stereoView)
  
        fprintf('Generating %s eye view\n', char(eyeLabel));
        conditionKeys = {'eyeXpos', 'eyeYpos', 'distance', 'fov'};
        conditionValues = {stereoView(char(eyeLabel)), cameraElevation, cameraDepthPos, cameraFOV};  
        conditionsFile = WriteConditionsFile(conditionsFile, conditionKeys, conditionValues);
    
        nativeSceneFiles = MakeSceneFiles(parentSceneFile, conditionsFile, mappingsFile, hints);
        radianceDataFiles = BatchRender(nativeSceneFiles, hints);

        montageName = sprintf('%s%s', sceneName, char(eyeLabel));
        montageFile = [montageName '.tiff'];
    
        [SRGBMontage, XYZMontage] = MakeMontage(radianceDataFiles, montageFile, toneMapFactor, isScaleGamma, hints);
        ShowXYZAndSRGB([], SRGBMontage, montageName);
    end
    
    fprintf('Results available at %s\n', hints.workingFolder);
end
