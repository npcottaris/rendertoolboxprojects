# Script to generate the fancy BallRoom
# 
# 10/10/2015  npc  Wrote it. 


def generateMaterials(scene):
	from mathutils import Vector

	materials = {};

    # -the room material
	params = { 'name'              : 'roomMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['room'] = scene.generateMaterialType(params);

    # the pedestal material
	params = { 'name'              : 'pedestalMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.8, 0.8, 0.8)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['pedestal'] = scene.generateMaterialType(params);


	# the ball materials
	params = { 'name'              : 'redBallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((1.0, 0.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['redBall'] = scene.generateMaterialType(params);

	params = { 'name'              : 'greenBallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 1.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['greenBall'] = scene.generateMaterialType(params);

	params = { 'name'              : 'blueBallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 0.0, 1.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['blueBall'] = scene.generateMaterialType(params);


	params = { 'name'              : 'yellowBallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((1.0, 1.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['yellowBall'] = scene.generateMaterialType(params);

	params = { 'name'              : 'targetBallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['targetBall'] = scene.generateMaterialType(params);

  # -the backwall material
	params = { 'name'              : 'backWallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['backWall'] = scene.generateMaterialType(params);


  # -the frontwall material
	params = { 'name'              : 'frontWallMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 0.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['frontWall'] = scene.generateMaterialType(params);

	# -the dark tilematerial
	params = { 'name'              : 'darkTileMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 1.0,
               'diffuse_color'     : Vector((0.2, 0.2, 0.2)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
	materials['darkTile'] = scene.generateMaterialType(params);

	return(materials);

def generateScene(sceneParams):
	import sys
	import imp
	import bpy
	import random
	from math import floor, cos, sin, tan, sqrt, atan2, atan, pow, pi
	from mathutils import Vector



	# ------------------------------ SCENE LAYOUT-----------------------------
	roomWidth      = 50;
	roomHeight     = 50;
	roomDepth      = 38; #22 26 30 # 34 # 38;
	# aperture width and height are computed via StereoRigDesigner.app with sceneWidth = 42, sceneHeight = 30, roomDepth = 20;
	apertureWidth  = 16.0;
	apertureHeight =  15; # 15.75;

	viewingDistance     = 76.4;
	cameraHorizontalFOV = 33.0;
	cameraElevation     = 19;  # the eye level
	cameraWidthToHeightAspectRatio = 1.45; 

	roomWallThickness = 1.0;

	apertureDepthPosition = -roomDepth+roomWallThickness/2;
	apertureHorizontalFOV = 2*atan(apertureWidth/(2*(viewingDistance-roomDepth)))/pi*180;
	print('aperture horizontalFOV: {} deg'.format(apertureHorizontalFOV))
	print('aperture dimensions   :{}cm x {}cm'.format(apertureWidth, apertureHeight))
  	# ------------------------------ SCENE MANAGER SETUP -----------------------------

	print('ToolboxDir = {}'.format(sceneParams['toolboxDirectory']));
	# Append the path to my custom Python scene toolbox to the Blender path
	sys.path.append(sceneParams['toolboxDirectory']);

	# Import the custom scene toolbox module
	import SceneUtilsV1;
	imp.reload(SceneUtilsV1);

	# Initialize a sceneManager
	sceneName = "{}_Depth{}cm".format(sceneParams['sceneName'], roomDepth);
	params = { 'name'               : sceneName,                 # name of new scene
               'erasePreviousScene' : True,                      # erase old scene
               'sceneWidthInPixels' : 640*2,                     # pixels along the horizontal-dimension
               'sceneHeightInPixels': 480*2,                     # pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0,                       # arbitrary units
               'sceneGridSpacing'   : 10.0,                      # set the spacing between grid lines to 10
               'sceneGridLinesNum'  : 20,                        # display 20 grid lines
              };
	scene = SceneUtilsV1.sceneManager(params);



	# Set the random seed
	random.seed(923431)

	# ---------------------------------- MATERIALS -----------------------------------
  	# Generate the materials
	materials = generateMaterials(scene)


  	# ------------------------- ENCLOSING ROOM ---------------------------
	roomLocation = Vector((0,-roomDepth/2, 0.0));

	roomParams = {'floorName'             : 'floor',
                'backWallName'          : 'backWall',
                'frontWallName'         : 'frontWall',
                'leftWallName'          : 'leftWall',
                'rightWallName'         : 'rightWall',
                'ceilingName'           : 'ceiling',
                'floorMaterialType'     : materials['room'],
                'backWallMaterialType'  : materials['backWall'],
                'frontWallMaterialType' : materials['frontWall'],
                'leftWallMaterialType'  : materials['room'],
                'rightWallMaterialType' : materials['room'],
                'ceilingMaterialType'   : materials['room'],
                'roomWidth'             : roomWidth,
                'roomDepth'             : roomDepth,
                'roomHeight'            : roomHeight,
                'roomLocation'          : roomLocation,
                'wallThickness'			: roomWallThickness,      # a wall thickness is necessary to bore out the window
              };
	roomBox = scene.addRoom(roomParams);


	
	# make window opening in front
	apertureParams = { 'name': 'windowAperture',
 					 'scaling':  Vector((apertureWidth/2,roomParams['wallThickness']*2,apertureHeight/2)),
 					 'rotation': Vector((0,0,0)), 
 					 'location': Vector((0, apertureDepthPosition, cameraElevation)),    # aperture at same height as eye-level
 					 'material': materials['room'],
 	};
	theWindowAperture = scene.addCube(apertureParams);
	scene.boreOut(roomBox['frontWallPlane'], theWindowAperture, True);


	# ---------------------------------BACKWALL 3D TILES ----------------------------
	checkSize = 2.0;

	tileParams = { 	'name': '',
 					'scaling':  Vector((checkSize/2,0.15,checkSize/2)),
 					'rotation': Vector((0,0,0)), 
 					'location': Vector((0,0,0)),
 					'material': materials['room'],
 	};

	materialParams = { 'name'      : '',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 1.0,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 

	checksXNum = int(roomWidth/checkSize);
	checksZNum = int(roomHeight/checkSize);
	for x in range(0, checksXNum+1):
		for z in range(0,checksZNum+1):
			if (x + z)%2 < 1:
				#material = darkGrayMaterial;
				grayLevel = 0.15 + 0.2*random.random();
				extrusion = 2.1 + (random.random()-0.5)*0.7;
			else:
				grayLevel = 0.2 + 0.3*random.random();
				#material = lightGrayMaterial;
				extrusion = 2.1 + (random.random()-0.5)*0.7;
			xx = (x-checksXNum/2)*checkSize;
			zz = (z-checksZNum/2)*checkSize;
			tileParams['name'] = 'tileAt{}-{}'.format(x,z);
			tileParams['location'] = Vector((xx,-extrusion,zz+roomHeight/2));
			materialParams['name'] = 'materialAt{}-{}'.format(x,z); 
			materialParams['diffuse_color'] = Vector((grayLevel, grayLevel, grayLevel));
			tileParams['material'] = scene.generateMaterialType(materialParams);
			scene.addCube(tileParams);

	# ------------ ADD FLOOR PEDESTAL WHERE BALL SHADOWS WILL BE PROJECTED-------------------
	pedestalParams = { 	'name': 'floorPedestal',
 					'scaling':  Vector((roomWidth*0.9/2,roomDepth*0.9/2,0.25)),
 					'rotation': Vector((0,0,0)), 
 					'location': Vector((0,-roomDepth/2+roomParams['wallThickness'],11)),
 					'material': materials['pedestal'],
 	};
	scene.addCube(pedestalParams);


	# -------- ADD FLOOR TILES ----
	

	floorTileSize = 4.0;
	floorTilesXNum = int(roomWidth/floorTileSize);
	floorTilesYNum = int(roomWidth/floorTileSize);

	floorTileParams = { 	'name': '',
 					'scaling':  Vector((floorTileSize/2,0.15,floorTileSize/2)),
 					'rotation': Vector((pi/2,0,0)), 
 					'location': Vector((0,0,0)),
 					'material': materials['room'],
 	};

	for x in range(0,floorTilesXNum+1):
		for y in range(0, floorTilesYNum+1):
			if (x + y)%2 < 1:
				xx = (x-floorTilesXNum/2)*floorTileSize;
				yy = (y+0.5)*floorTileSize;
				floorTileParams['name'] = 'floorTileAt{}-{}'.format(x,y);
				floorTileParams['location'] = Vector((xx,-roomDepth+roomParams['wallThickness']*2+yy, pedestalParams['location'].z+0.175));
				floorTileParams['material']  = materials['darkTile'];
				scene.addCube(floorTileParams);

	# ------------ ADD BACK WALL PEDESTAL WHERE BALL SHADOWS WILL BE PROJECTED-------------------
	pedestalParams = { 	'name': 'backWallPedestal',
 					'scaling':  Vector((9,9,0.25)),
 					'rotation': Vector((pi/2,0,0)), 
 					'location': Vector((0,-3.0,cameraElevation)),
 					'material': materials['pedestal'],
 	};
	scene.addCube(pedestalParams);


	# -------------- ADD THE BALLS -----------------
	sphereParams = { 'name'     : '',
                     'scaling'  : Vector((2.0, 2.0, 2.0)), 
                     'location' : Vector((0,0,0)),
                     'material' : materials['room'],
               };
  # red ball
	sphereParams['name'] = 'redBall';
	sphereParams['location'] = Vector((-6.3, -7, 25.5));
	sphereParams['material'] = materials['redBall']; 
	theRedBall = scene.addSphere(sphereParams);

	# blue ball
	sphereParams['name'] = 'blueBall';
	sphereParams['location'] = Vector((theRedBall.location.x+2.4, theRedBall.location.y-11, theRedBall.location.z-12));
	sphereParams['material'] = materials['blueBall']; 
	theBlueBall = scene.addSphere(sphereParams);

	# yellow ball
	sphereParams['name'] = 'yellowBall';
	sphereParams['location'] = Vector((6, theRedBall.location.y+1, theBlueBall.location.z+2.0));
	sphereParams['material'] = materials['yellowBall']; 
	theYellowBall = scene.addSphere(sphereParams);

	# center ball
	sphereParams['name'] = 'centralBall';
	sphereParams['location'] = Vector((0, theRedBall.location.y, cameraElevation+0.5));
	sphereParams['material'] = materials['targetBall']; 
	theCentralBall = scene.addSphere(sphereParams);

	# green ball
	sphereParams['name'] = 'greenBall';
	sphereParams['location'] = Vector((5.3, theBlueBall.location.y+2, theRedBall.location.z-1.5));
	sphereParams['material'] = materials['greenBall']; 
	theGreenBall = scene.addSphere(sphereParams);


  # ---------------------------------- CAMERA -------------------------------------
  # Define our cameraType
	nearClipDistance = 0.1;
	farClipDistance  = 1000;
	
	renderedImageWidthInCm  = 2*viewingDistance * tan(cameraHorizontalFOV/2*pi/180);
	renderedImageHeightInCm = renderedImageWidthInCm/cameraWidthToHeightAspectRatio;
	print('***-->rendered image width  :{}cm'.format(renderedImageWidthInCm));
	print('***-->rendered image height :{}cm'.format(renderedImageHeightInCm));
  

	params = {'clipRange'            : Vector((nearClipDistance, farClipDistance)),
              'fieldOfViewInDegrees' : cameraHorizontalFOV,   # horizontal FOV
              'widthToHeightAspectRatio' : cameraWidthToHeightAspectRatio, 
              'pixelSamplesAlongWidth': 512,  # this only affects the Blender rendering, nothing else
              'drawSize'             : 2,     # camera wireframe size, this only affects the Blender rendering, nothing else
             };
	cameraType = scene.generateCameraType(params);
 
  # Add the camera
	cameraDistance = -viewingDistance;  # negative distance is towards us, positive is away from us
	cameraRotationInDeg = 0;
	theta  = cameraRotationInDeg * (pi/180)
	cameraHorizPosition = cameraDistance * sin(theta);
	cameraDepthPosition = cameraDistance * cos(theta);

	params = {  'name'          :'Camera', 
                'cameraType'    : cameraType,
                'location'      : Vector((cameraHorizPosition, cameraDepthPosition, cameraElevation)),     
                'lookAt'        : Vector((0, 0, cameraElevation)),
                'showName'      : True,
          };          
	mainCamera = scene.addCameraObject(params);

  # ---------------------------------- LIGHTS -------------------------------------
  # Generate the area lamp model
	params = {'name'  : 'areaLampModel', 
              'color' : Vector((1,1,1)), 
              'fallOffDistance': 120,
              'width1': 2, # sceneParams['areaLampSize'],
              'width2': 2, # sceneParams['areaLampSize']
              }
	brightLight = scene.generateAreaLampType(params);

    # - Add the FRONT left area lamp
	lightElevation = roomHeight-10;

	lightDepthPosition = -16; #-10;-roomDepth/2
	lightHorizPosition = -7;

	leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	leftAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'frontLeftAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : leftAreaLampPosition, 
              'lookAt'   : leftAreaLampLooksAt
          };
	frontLeftAreaLamp = scene.addLampObject(params);


	# - Add the FRONT right area lamp
	lightHorizPosition = -lightHorizPosition; 
	rightAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation           # elevation (z-coord)
                           ));
	rightAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'frontRightAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : rightAreaLampPosition, 
              'lookAt'   : rightAreaLampLooksAt
          };
	frontRightAreaLamp = scene.addLampObject(params);


	# - Add the REAR left area lamp
	lightDepthPosition = lightDepthPosition+10; 
	lightHorizPosition = -7;
	

	leftAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	leftAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'rearLeftAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : leftAreaLampPosition, 
              'lookAt'   : leftAreaLampLooksAt
          };
	rearLeftAreaLamp = scene.addLampObject(params);


	# - Add the REAR right area lamp
	lightHorizPosition = -lightHorizPosition;
	

	rightAreaLampPosition = Vector((
                            lightHorizPosition,      # horizontal position (x-coord)
                            lightDepthPosition,      # depth position (y-coord)
                            lightElevation       	   # elevation (z-coord)
                           ));
	rightAreaLampLooksAt = Vector((
                             lightHorizPosition,      # horizontal position (x-coord)
                             lightDepthPosition,      # depth position (y-coord)
                             0       # elevation (z-coord)
                           ));

	params = {'name'     : 'rearRightAreaLamp', 
              'model'    : brightLight, 
              'showName' : True, 
              'location' : rightAreaLampPosition, 
              'lookAt'   : rightAreaLampLooksAt
          };
	rearRightAreaLamp = scene.addLampObject(params);

    # ---------------------------------- ACTION ! -------------------------------------

    # Finally, export collada file
	scene.exportToColladaFile(sceneParams['exportsDirectory']);

	print('All done !')




# ------------------------------- main() ----------------------------
import os
import sys
import argparse
from math import pi

baseDir = '/Users/Shared/Matlab/Toolboxes'
toolboxDir='{}/RenderToolbox3/Utilities/BlenderPython'.format(baseDir)
print(toolboxDir)
homeDir = '/Users/nicolas/Documents/1.code/3.RenderToolboxProjects/2.BallRoom'
exportsDir = '{}/2.ColladaExports'.format(homeDir)

params = {'toolboxDirectory' : toolboxDir,
		  'exportsDirectory' : exportsDir,
				 'sceneName' : 'FancyBallroom',
		 };

generateScene(params);