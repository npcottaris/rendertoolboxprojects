#!/usr/local/bin/python3.3
# Script to recreate the Xiao et al (2012) scene.
# 
# 6/3/2013  npc  Wrote it. First rough, unpolished version.
# 6/4/2013  npc  Cleaned up a bit. Added comments. 
#                Added textured background wall with
#                tiles of random extrusion and gray level
#

#Import the Blender Python API
import bpy 

#Import Blender vector math utilities
from mathutils import Vector, Euler

#Import the standard Python math library
import math 

import random

# Method that defines the position of all objects in the scene
def defineObjectPlacement():
	global FloorVerticalPosInCm, FloorHorizontalPosInCm;
	global Y_OFFSET_IN_CM;

	global DistanceFromObserverToBackWall, BackWallWidthInCm, BackWallHeightInCm, BackWallHorizontalPosInCm, BackWallVerticalPosInCm;
	global BackWallGrayRegionWidthInCm, BackWallGrayRegionHeightInCm, BackWallGrayRegionHorizontalPosInCm, BackWallGrayRegionVerticalPosInCm ;
	global SideWallLengthInCm, SideWallHeightInCm;
	global LeftWallHorizontalPosInCm, LeftWallVerticalPosInCm;
	global RightWallHorizontalPosInCm, RightWallVerticalPosInCm;

	global AreaLightsVerticalPosInCm;
	global LeftAreaLightsHorizontalPosInCm;
	global RightAreaLightsHorizontalPosInCm;
	global RearAreaLightsDistanceFromObserverInCm;
	global FrontAreaLightsDistanceFromObserverInCm;

	global PointSourceLightVerticalPosInCm
	global PointSourceLightsDistanceFromObserverInCm
	global LeftPointSourceLightHorizontalPosInCm
	global RightPointSourceLightHorizontalPosInCm

	global CeilingHorizontalPosInCm, CeilingVerticalPosInCm;

	global UpperLeftSphereHorizontalPosInCm,  UpperLeftSphereVerticalPosInCm,  UpperLeftSphereDistanceToObserverInCm;
	global UpperRightSphereHorizontalPosInCm, UpperRightSphereVerticalPosInCm, UpperRightSphereDistanceToObserverInCm;
	global LowerLeftSphereHorizontalPosInCm,  LowerLeftSphereVerticalPosInCm,  LowerLeftSphereDistanceToObserverInCm;
	global LowerRightSphereHorizontalPosInCm, LowerRightSphereVerticalPosInCm, LowerRightSphereDistanceToObserverInCm;
	global CenterSphereHorizontalPosInCm, CenterSphereVerticalPosInCm, CenterSphereDistanceToObserverInCm;

	global ApertureOpeningWidthInCm, ApertureOpeningHeigtInCm;
	global ApertureHorizontalPosInCm, ApertureDistanceToObserverInCm;
	global ApertureTotalWidthInCm, ApertureTotalHeightInCm, BottomApertureHeightInCm, TopApertureHeightInCm;

	global cameraLooksAtHorizontalPosInCm, cameraLooksAtVerticalPosInCm;

	# In Xiao et al, the floor is at a height -6.0 cm.
	# This is with respect to the eyes which are at a height of 0 cm
	# For 3D viewing purposes we offset all the height measurement by 6.0
	# so that nothing is below the below the 0 level. This is the purpose of the Y_OFFSET
	Y_OFFSET_IN_CM = 7.0;

	FloorVerticalPosInCm   = -6.0 + Y_OFFSET_IN_CM;
	FloorHorizontalPosInCm = 0.0;

	# In Xiao et al, the center of the test objects is at (0.2, 2.3, 72.1)
	# So make the camera look at 2.3 (along the height dimension, which is the z-coord in blender)
	cameraLooksAtHorizontalPosInCm = 0.0;
	cameraLooksAtVerticalPosInCm = Y_OFFSET_IN_CM + 2.3;

	# BackWall at x = 0.0 cm, y = 76.3 cm, z = -6cm, 
	# Backwall dimensions (all in cm)
	# Distance from observer to back wall
	DistanceFromObserverToBackWall 	= 76.3;
	BackWallWidthInCm  				= 36.0;
	BackWallHeightInCm 				= 35.0; 
	BackWallHorizontalPosInCm 		=  0.0;
	BackWallVerticalPosInCm 		= BackWallHeightInCm/2.0 + FloorVerticalPosInCm;

	BackWallGrayRegionWidthInCm 		= 10.8;
	BackWallGrayRegionHeightInCm 		= 11.7;
	BackWallGrayRegionHorizontalPosInCm =  0.4;
	BackWallGrayRegionVerticalPosInCm 	= BackWallGrayRegionHeightInCm/2.0 + 2.1 + FloorVerticalPosInCm;

	SideWallLengthInCm = 20.8;
	SideWallHeightInCm = BackWallHeightInCm;

	LeftWallHorizontalPosInCm  	= -16.6;
	RightWallHorizontalPosInCm 	=  16.6;
	LeftWallVerticalPosInCm    	= SideWallHeightInCm/2.0  + FloorVerticalPosInCm;
	RightWallVerticalPosInCm   	= LeftWallVerticalPosInCm;

	CeilingHorizontalPosInCm 	= 0.0;
	CeilingVerticalPosInCm   	= BackWallHeightInCm + FloorVerticalPosInCm;

	UpperLeftSphereHorizontalPosInCm  		= -4.7;
	UpperLeftSphereVerticalPosInCm    		=  6.7 + Y_OFFSET_IN_CM;
	UpperLeftSphereDistanceToObserverInCm 	= 74.7;

	UpperRightSphereHorizontalPosInCm 		=  5.7;
	UpperRightSphereVerticalPosInCm   		=  6.5 + Y_OFFSET_IN_CM;
	UpperRightSphereDistanceToObserverInCm 	= 66.4;

	LowerLeftSphereHorizontalPosInCm 		= -4.1;
	LowerLeftSphereVerticalPosInCm   		= -4.4 + Y_OFFSET_IN_CM;
	LowerLeftSphereDistanceToObserverInCm 	= 67.3;

	LowerRightSphereHorizontalPosInCm 		=  4.8;
	LowerRightSphereVerticalPosInCm   		= -3.8 + Y_OFFSET_IN_CM;
	LowerRightSphereDistanceToObserverInCm 	= 74.2;

	CenterSphereHorizontalPosInCm 			=  0.0;
	CenterSphereVerticalPosInCm   			=  2.0 + Y_OFFSET_IN_CM;
	CenterSphereDistanceToObserverInCm 		= 72.0;

	ApertureOpeningWidthInCm 			= 15.2;
	ApertureOpeningHeigtInCm 			= 14.5;
	ApertureDistanceToObserverInCm 		= 55.4;

	ApertureHorizontalPosInCm 			= 0.0;
	ApertureTotalWidthInCm 				= ApertureOpeningWidthInCm * 3;
	ApertureTotalHeightInCm 			= BackWallHeightInCm;
	BottomApertureHeightInCm 			= 2;
	TopApertureHeightInCm 				= 20.5;

	# Positions of area lights
	AreaLightsVerticalPosInCm         			= 26.0 + FloorVerticalPosInCm;
	LeftAreaLightsHorizontalPosInCm   			= -7.0;
	RightAreaLightsHorizontalPosInCm  			=  7.0;
	RearAreaLightsDistanceFromObserverInCm  	= 70.4;
	FrontAreaLightsDistanceFromObserverInCm 	= 57.9;

	# Positions of point source lights
	PointSourceLightVerticalPosInCm 			= 25.0 + FloorVerticalPosInCm;
	PointSourceLightsDistanceFromObserverInCm 	= 60.4;
	LeftPointSourceLightHorizontalPosInCm  		= -6.0;
	RightPointSourceLightHorizontalPosInCm 		=  6.0;



# Method to reposition the camera and rotate it so that it points at a target position
def pointCameraToTarget(camera, cameraPosVectorInCm, targetPosVectorInCm):
	# position camera
	camera.location =  cameraPosVectorInCm; 
	# compute camera rotation angles
	deltaVector = targetPosVectorInCm - camera.location;
	dr = math.sqrt(math.pow(deltaVector.x,2.0) + math.pow(deltaVector.y,2.0));
	xRad = math.pi/2.0 + math.atan2(deltaVector.z, dr);
	zRad = math.atan2(deltaVector.y, deltaVector.x) - math.pi/2.0;
	angles = (xRad, 0, zRad);
	# Set the camera's Euler angles so that it points to the desired location
	camera.rotation_euler = Euler(angles, 'XYZ');


# Method to configure camera for parallel off-axis rendering
def configureCamera(camera, scene):
	# scale the camera (just for illustration purposes)
	cameraScaleFactor = 1;
	camera.scale = cameraScaleFactor * Vector((1,1,1));

	# set camera field of view to 33 degrees
	fieldOfViewInDegrees = 33.0;
	scene.camera.data.angle = math.pi/180.0 * fieldOfViewInDegrees;
	print('camera field of view set to {} degrees'.format(fieldOfViewInDegrees));

	# position the camera for parallel off-axis rendering
	global cameraLooksAtHorizontalPosInCm, cameraLooksAtVerticalPosInCm;
	cameraPosVectorInCm = Vector((cameraLooksAtHorizontalPosInCm, 0.0,  cameraLooksAtVerticalPosInCm));
	targetPosVectorInCm = Vector((cameraLooksAtHorizontalPosInCm, DistanceFromObserverToBackWall, cameraLooksAtVerticalPosInCm));
	pointCameraToTarget(camera, cameraPosVectorInCm, targetPosVectorInCm);


# Method to set the the scene system to Metric, 
# and the positional units in centimeters
def setSystemUnits(scene):
	# Set units system to METRIC
	scene.unit_settings.system = 'METRIC';
	# Set the unit scale to 1.0 cm
	scene.unit_settings.scale_length = 1.0; #/100.0;

	# Now search for the grid_space parameter
	# areas is the defining the layout of the Blender Window 
	print('Will search {} screens'.format(len(bpy.data.screens)));
	for screen in bpy.data.screens:
		print('  Screen {} has {} areas'.format(screen.name, len(screen.areas)));
		for area in screen.areas:
			print('   Area is of type {}'.format(area.type));
			# Find an area with type VIEW_3D. Such an area 
			# will be a subclass of SpaceView3D where grid_scale is defined
			if area.type == 'VIEW_3D':
				# search through the spaces to find spaces with type VIEW_3D
				print('    Will search {} spaces in current area'.format(len(area.spaces)))
				for space in area.spaces:
					if space.type == 'VIEW_3D':
						print('    >> Setting grid scale')
						# make grid spacing equal to 10 cm
						space.grid_scale = 10*scene.unit_settings.scale_length;
						# 20 grid lines, i.e., 2 meters in each direction
						space.grid_lines = 16;


# Method to change the GUI so that we only see the 3D scene
def goToFullScreenMode():
	for window in bpy.context.window_manager.windows:
	    screen = window.screen
	    for area in screen.areas:        
	        if area.type == 'VIEW_3D':
	            for region in area.regions:
	                if region.type == 'WINDOW':
	                    override = {'window': window, 'screen': screen, 'area': area, 'region': region}
	                    # enable for the window to be full size
	                    bpy.ops.screen.screen_full_area(override)# Works!
	                    #bpy.ops.screen.back_to_previous(override)# Works!
	                    #bpy.ops.view3d.view_orbit(override)# Works!
	                    #bpy.ops.view3d.view_pan(override)# Now works!
	                    #bpy.ops.view3d.zoom(override, delta=5, mx=0, my=0)# Now works!


# Method to set the output image size.
# Here, we should also set the rendering method
def setRenderedImageSize(scene, sizeX, sizeY):
	scene.render.resolution_x = sizeX;
	scene.render.resolution_y = sizeY;


# Method to remove all objects from the scene (except for the camera)
def removeAllObjects():
	for object in bpy.data.objects:
		if not(object.name == 'Camera'):
			removeObjectFromScene(object);


# Method to remove an object from the scene
def removeObjectFromScene(objectToRemove):
	bpy.context.scene.objects.unlink(objectToRemove)
	bpy.data.objects.remove(objectToRemove);


# Method to create a plane with a desired scale, rotation, and location
def createPlane(scalingVector, rotationVector, locationVectorInCm):
	# Create plane. By default, the plane is placed at the origin (0,0,0)
	bpy.ops.mesh.primitive_plane_add();
	obj = bpy.context.active_object;
	# The following transformations have to be executed in this order
	# (1) scale to desired size
	obj.scale = scalingVector;
	# (2) rotate to desired angle
	obj.rotation_euler = rotationVector;
	# (3) move to desired location
	obj.location = locationVectorInCm;
	# do not hide
	obj.hide = False;
	return(obj);


# Method to create a plane with a desired scale and location
def createSphere(scalingVector, locationVectorInCm):
	# Create sphere
	bpy.ops.mesh.primitive_uv_sphere_add();
	obj = bpy.context.active_object;
	# First scale to desired size
	obj.scale = scalingVector;
	# Then move to desired location
	obj.location = locationVectorInCm;
	# do not hide
	obj.hide = False;
	return(obj);

# Method to make a material with desired color, diffuse and specular properties
def makeMaterial(name, diffuseColor, specularColor, diffuseIntensity, specularIntensity, alpha):
	mat = bpy.data.materials.new(name);
	mat.diffuse_color      = diffuseColor;
	# Options for diffuse shaders: Minnaert, Fresnel, Toon, Oren-Nayar, Lambert
	mat.diffuse_shader     = 'LAMBERT';
	mat.diffuse_intensity  = diffuseIntensity;
	mat.specular_color     = specularColor;
	# Options for specular shaders: CookTorr, Phong, Blinn, Toon, WardIso
	mat.specular_shader    = 'WARDISO'
	mat.specular_intensity = specularIntensity;
	mat.alpha = alpha;
	mat.ambient = 1;
	return(mat);

# Method to set a given material to an object.
def setMaterial(object, mat):
	object.data.materials.append(mat);

# Method to create a lamp type
def createLamp(type):
	# possible lamp types are 'POINT’, ‘SUN’, ‘SPOT’, ‘HEMI’, ‘AREA’]
	lamp = bpy.data.lamps.new('myLamp', type);
	if type == 'AREA':
		lamp.size = 1;
		lamp.energy = 0.3;
	else:
		lamp.energy = 1.0;
	
	lamp.color = (1.0, 1.0, 1.0);
	return(lamp);


# Method to make the Achrom Forced Choice room as it appears in Xiao et al (2012)
def makeRoom(scene):

	# Generate the different materials that we will use
	diffuseColor = (0.6, 0.6, 0.6);
	specularColor = (0.9,0.9,0.9);
	diffuseIntensity = 0.1;
	specularIntensity = 0.0;
	whiteMaterial = makeMaterial('WhiteMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0.5,0.5,0.5);
	specularColor = (0.5,0.5,0.5);
	grayMaterial = makeMaterial('GrayMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0.,0.,0.);
	specularColor = (0.,0.,0.);
	blackMaterial = makeMaterial('GrayMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0.2,0.2,0.2);
	specularColor = (0.2,0.2,0.2);
	darkGrayMaterial = makeMaterial('GrayMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0.7,0.7,0.7);
	specularColor = (0.7,0.7,0.7);
	lightGrayMaterial = makeMaterial('GrayMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	specularIntensity = 1.0;
	diffuseIntensity = 0.7;
	diffuseColor = (1,0,0);
	specularColor = (1,1,1);
	redMaterial = makeMaterial('RedMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0,1,0);
	specularColor = (1,1,1);
	greenMaterial = makeMaterial('GreenMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (1,1,0);
	specularColor = (1,1,1);
	yellowMaterial = makeMaterial('YellowMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	diffuseColor = (0,0,1);
	specularColor = (1,1,1);
	blueMaterial = makeMaterial('BlueMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);

	
	# Generate the back wall
	global FloorVerticalPosInCm
	global DistanceFromObserverToBackWall, BackWallWidthInCm, BackWallHeightInCm;
	global BackWallHorizontalPosInCm, BackWallVerticalPosInCm;

	locationVectorInCm  = Vector((BackWallHorizontalPosInCm, DistanceFromObserverToBackWall, BackWallVerticalPosInCm));
	scalingVector       = Vector((BackWallWidthInCm/2, BackWallHeightInCm/2, 0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((math.pi/2.0, 0.0, 0.0));
	backWallObject      = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(backWallObject, whiteMaterial);


	# Generate uniform dark gray square in the back wall
	global BackWallGrayRegionWidthInCm, BackWallGrayRegionHeightInCm;
	global BackWallGrayRegionHorizontalPosInCm;
	global BackWallGrayRegionVerticalPosInCm;
	locationVectorInCm  = Vector((BackWallGrayRegionHorizontalPosInCm, DistanceFromObserverToBackWall-0.4, BackWallGrayRegionVerticalPosInCm));
	scalingVector       = Vector((BackWallGrayRegionWidthInCm/2, BackWallGrayRegionHeightInCm/2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((math.pi/2.0, 0.0, 0.0));
	backWallGrayRegionObject = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(backWallGrayRegionObject, grayMaterial);


	# Generate the checkerboard pattern on the back wall by adding square planes of alternating intensity
	# Also we are alternating the amount of extrusion to create a more realistic 3D effect
	xo = BackWallGrayRegionHorizontalPosInCm;
	yo = BackWallGrayRegionVerticalPosInCm;
	multiCheckFactor = 3;
	checksNum = 8*multiCheckFactor;
	dx = BackWallWidthInCm / (10*multiCheckFactor);
	dy = dx;
	for x in range(0,checksNum+1):
		for y in range(2,checksNum):
			if (x + y)%2 < 1:
				#material = darkGrayMaterial;
				grayLevel = 0.15 + 0.2*random.random();
				extrusion = 0.1; #0.2+(random.random()-0.5)*0.15;
			else:
				grayLevel = 0.2 + 0.3*random.random();
				#material = lightGrayMaterial;
				extrusion = 0.2 # 0.2+(random.random()-0.5)*0.15;
			xx = (x - checksNum/2)*dx;
			yy = (y - checksNum/2)*dy;
			locationVectorInCm = Vector((xo+xx, DistanceFromObserverToBackWall-extrusion, yo + yy));
			scalingVector      = Vector((dx/2, dy/2,0)) * scene.unit_settings.scale_length;
			obj = createPlane(scalingVector, rotationVector, locationVectorInCm);

			diffuseColor  = grayLevel * Vector((1.0,1.0,1.0));
			specularColor = diffuseColor;
			randomGrayMaterial = makeMaterial('randomGrayMaterial',diffuseColor, specularColor, diffuseIntensity, specularIntensity, 1);
			setMaterial(obj, randomGrayMaterial);


	# Now the side walls
	global SideWallLengthInCm, SideWallHeightInCm;
	global LeftWallHorizontalPosInCm,  LeftWallVerticalPosInCm;
	global RightWallHorizontalPosInCm, RightWallVerticalPosInCm;

	# Left side wall
	locationVectorInCm  = Vector((LeftWallHorizontalPosInCm, DistanceFromObserverToBackWall-SideWallLengthInCm/2.0, LeftWallVerticalPosInCm));
	scalingVector       = Vector((SideWallLengthInCm/2, SideWallHeightInCm/2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((math.pi/2.0, 0.0, math.pi/2.0));
	leftWallObject      = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(leftWallObject, whiteMaterial);

	# Right side wall
	locationVectorInCm  = Vector((RightWallHorizontalPosInCm, DistanceFromObserverToBackWall-SideWallLengthInCm/2.0, RightWallVerticalPosInCm));
	scalingVector       = Vector((SideWallLengthInCm/2, SideWallHeightInCm/2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((math.pi/2.0, 0.0, math.pi/2.0));
	rightWallObject     = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(rightWallObject, whiteMaterial);

	# Floor
	global FloorHorizontalPosInCm, FloorVerticalPosInCm;
	locationVectorInCm  = Vector((FloorHorizontalPosInCm, DistanceFromObserverToBackWall-SideWallLengthInCm/2.0, FloorVerticalPosInCm));
	scalingVector       = Vector((BackWallWidthInCm/2, SideWallLengthInCm/2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((0,0, 0.0));
	floorObject         = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(floorObject, lightGrayMaterial);


	# Create X-shaped patten on the floor
	# Long stripe  
	StripeWidthInCm     = BackWallWidthInCm / 6;
	StripeLengthInCm    = SideWallLengthInCm * 1.4;
	locationVectorInCm  = Vector((FloorHorizontalPosInCm, DistanceFromObserverToBackWall+3.7-StripeLengthInCm/2.0, 0.05 + FloorVerticalPosInCm));
	scalingVector       = Vector((StripeWidthInCm/2, StripeLengthInCm/2,0)) * scene.unit_settings.scale_length;
	# rotation vector for first stripe
	rotationVector      = Vector((0,0,math.pi/3.0));
	floorStripe1Object  = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(floorStripe1Object, darkGrayMaterial);
	# rotation vector for second stripe
	rotationVector      = Vector((0,0,-math.pi/3.0));
	floorStripe2Object         = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(floorStripe2Object, darkGrayMaterial);


	# Ceiling
	global CeilingHorizontalPosInCm, CeilingVerticalPosInCm;
	locationVectorInCm  = Vector((CeilingHorizontalPosInCm, DistanceFromObserverToBackWall-SideWallLengthInCm/2.0, CeilingVerticalPosInCm));
	scalingVector       = Vector((BackWallWidthInCm/2, SideWallLengthInCm/2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((0,0,0));
	ceilingObject       = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(ceilingObject, whiteMaterial);


	# Spheres
	sphereRadiusInCm   = 3.2/2.0;
	scalingVector      = sphereRadiusInCm * Vector((1.0,1.0,1.0));

	# upper-left sphere
	global UpperLeftSphereHorizontalPosInCm, UpperLeftSphereVerticalPosInCm, UpperLeftSphereDistanceToObserverInCm;
	locationVectorInCm      = Vector((UpperLeftSphereHorizontalPosInCm, UpperLeftSphereDistanceToObserverInCm, UpperLeftSphereVerticalPosInCm));
	upperLeftSphereObject   = createSphere(scalingVector, locationVectorInCm);
	setMaterial(upperLeftSphereObject, redMaterial);

	# upper-right sphere
	global UpperRightSphereHorizontalPosInCm, UpperRightSphereVerticalPosInCm, UpperRightSphereDistanceToObserverInCm;
	locationVectorInCm      = Vector((UpperRightSphereHorizontalPosInCm, UpperRightSphereDistanceToObserverInCm, UpperRightSphereVerticalPosInCm));
	upperRightSphereObject  = createSphere(scalingVector, locationVectorInCm);
	setMaterial(upperRightSphereObject, greenMaterial);

	# lower-left sphere
	global LowerLeftSphereHorizontalPosInCm,  LowerLeftSphereVerticalPosInCm,  LowerLeftSphereDistanceToObserverInCm;
	locationVectorInCm      = Vector((LowerLeftSphereHorizontalPosInCm, LowerLeftSphereDistanceToObserverInCm, LowerLeftSphereVerticalPosInCm));
	lowerLeftSphereObject   = createSphere(scalingVector, locationVectorInCm);
	setMaterial(lowerLeftSphereObject, blueMaterial);

	# lower-right sphere
	global LowerRightSphereHorizontalPosInCm, LowerRightSphereVerticalPosInCm, LowerRightSphereDistanceToObserverInCm;
	locationVectorInCm      = Vector((LowerRightSphereHorizontalPosInCm, LowerRightSphereDistanceToObserverInCm, LowerRightSphereVerticalPosInCm));
	lowerRightSphereObject  = createSphere(scalingVector, locationVectorInCm);
	setMaterial(lowerRightSphereObject, yellowMaterial);

	# center sphere
	global CenterSphereHorizontalPosInCm, CenterSphereVerticalPosInCm, CenterSphereDistanceToObserverInCm;
	locationVectorInCm      = Vector((CenterSphereHorizontalPosInCm, CenterSphereDistanceToObserverInCm,  CenterSphereVerticalPosInCm));
	centerSphereObject      = createSphere(scalingVector, locationVectorInCm);
	setMaterial(centerSphereObject, greenMaterial);

	# Aperture 
	global  ApertureOpeningWidthInCm, ApertureOpeningHeigtInCm
	global  ApertureHorizontalPosInCm, ApertureDistanceToObserverInCm;
	global  ApertureTotalWidthInCm, ApertureTotalHeightInCm, BottomApertureHeightInCm, TopApertureHeightInCm;

	# Left and right side scaling and rotation
	scalingVector       = Vector((ApertureOpeningWidthInCm/2, ApertureTotalHeightInCm /2,0)) * scene.unit_settings.scale_length;
	rotationVector      = Vector((math.pi/2.0, 0.0, 0.0));

	# Left component of aperture
	locationVectorInCm  = Vector((ApertureHorizontalPosInCm-ApertureOpeningWidthInCm, ApertureDistanceToObserverInCm, ApertureTotalHeightInCm /2.0 + FloorVerticalPosInCm));
	apertureLeftObject  = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(apertureLeftObject, blackMaterial);

	# Right component of aperture
	locationVectorInCm  = Vector((ApertureHorizontalPosInCm+ApertureOpeningWidthInCm, ApertureDistanceToObserverInCm, ApertureTotalHeightInCm/2.0 + FloorVerticalPosInCm));
	apertureRightObject  = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(apertureRightObject, blackMaterial);

	# Bottom component of aperture
	locationVectorInCm  = Vector((ApertureHorizontalPosInCm, ApertureDistanceToObserverInCm, -BottomApertureHeightInCm/2.0 + FloorVerticalPosInCm));
	scalingVector       = Vector((ApertureTotalWidthInCm/2, BottomApertureHeightInCm/2, 0)) * scene.unit_settings.scale_length;
	apertureBottomObject  = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(apertureBottomObject, blackMaterial);

	# Top component of aperture 
	locationVectorInCm  = Vector((ApertureHorizontalPosInCm, ApertureDistanceToObserverInCm, TopApertureHeightInCm/2.0 + ApertureOpeningHeigtInCm + FloorVerticalPosInCm));
	scalingVector       = Vector((ApertureTotalWidthInCm/2, TopApertureHeightInCm/2,0)) * scene.unit_settings.scale_length;
	apertureTopObject  = createPlane(scalingVector, rotationVector, locationVectorInCm);
	setMaterial(apertureTopObject, blackMaterial);



# main()

# Remove all objects other than the camera
removeAllObjects();

# Rename scene 
AFCRoomSceneObject = bpy.context.screen.scene;
AFCRoomSceneObject.name = 'Achrom Forced Choice Room';

# Set the system to metric units to cm, rotation angles in degrees 
setSystemUnits(AFCRoomSceneObject);

# Set the scene rendering size
setRenderedImageSize(AFCRoomSceneObject, 1920, 1200);

# Set the positions of all objects in the AchromForcedChoice scene
defineObjectPlacement();

# Configure the camera
configureCamera(AFCRoomSceneObject.camera, AFCRoomSceneObject);

# Let there be light !
# Create an area lamp
areaLamp = createLamp('AREA');

# Make the four area lamps and position them
rearLeftAreaLightObject = bpy.data.objects.new('RearLeftAreaLight', areaLamp);
rearLeftAreaLightObject.location = Vector((LeftAreaLightsHorizontalPosInCm, RearAreaLightsDistanceFromObserverInCm, AreaLightsVerticalPosInCm));
bpy.context.scene.objects.link(rearLeftAreaLightObject);

rearRightAreaLightObject = bpy.data.objects.new('RearRightAreaLight', areaLamp);
rearRightAreaLightObject.location = Vector((RightAreaLightsHorizontalPosInCm, RearAreaLightsDistanceFromObserverInCm, AreaLightsVerticalPosInCm));
bpy.context.scene.objects.link(rearRightAreaLightObject);

frontLeftAreaLightObject = bpy.data.objects.new('FrontLeftAreaLight', areaLamp);
frontLeftAreaLightObject.location = Vector((LeftAreaLightsHorizontalPosInCm, FrontAreaLightsDistanceFromObserverInCm, AreaLightsVerticalPosInCm));
bpy.context.scene.objects.link(frontLeftAreaLightObject);

frontRightAreaLightObject = bpy.data.objects.new('FrontRightAreaLight', areaLamp);
frontRightAreaLightObject.location = Vector((RightAreaLightsHorizontalPosInCm, FrontAreaLightsDistanceFromObserverInCm, AreaLightsVerticalPosInCm));
bpy.context.scene.objects.link(frontRightAreaLightObject);

# Create a point light source lamp
pointSourceLamp = createLamp('POINT');

leftPointLightObject = bpy.data.objects.new('LeftPointLight', pointSourceLamp);
leftPointLightObject.location = Vector((LeftPointSourceLightHorizontalPosInCm , PointSourceLightsDistanceFromObserverInCm, PointSourceLightVerticalPosInCm));
bpy.context.scene.objects.link(leftPointLightObject);

rightPointLightObject = bpy.data.objects.new('RightPointLight', pointSourceLamp);
rightPointLightObject.location = Vector((RightPointSourceLightHorizontalPosInCm , PointSourceLightsDistanceFromObserverInCm, PointSourceLightVerticalPosInCm));
bpy.context.scene.objects.link(rightPointLightObject);


# Make the room and its contents
makeRoom(AFCRoomSceneObject);

# select the center camera (object mode)
bpy.ops.object.select_all(action='DESELECT')
bpy.context.scene.objects.active = bpy.context.scene.camera
bpy.context.scene.camera.select = True;

# Get scene 
scene = bpy.data.scenes[0];
fileName = '/Users/nicolas/Documents/1.Code/2.RenderToolboxImagery/1.Projects/2.BallRoom/2.ColladaExports/BallRoom.dae';
#The transrotloc option is necessary for RT3 to successfully parse the collada file
bpy.ops.wm.collada_export(filepath=fileName, export_transformation_type_selection='transrotloc');

#goToFullScreenMode();
