# select scene to generate
scene = 'CheckerShadow';
scene = 'Reset';
scene = 'Test2';
scene = 'GilChirst-PerpendicularPlanes';
scene = 'Blobbie'
#scene = 'BlobbieSunRoom'
scene = 'ColorMaterialVaryingBlobbies'
scene = 'BallRoom'
scene = 'Blobbierium'
scene = 'BlobbieriumHighDensity'
scene = 'BlobbieriumDifferentSizesAndShapes'


rootDir = '/Users/nicolas/Documents/1_developer/1_python/RTprojects/rendertoolboxprojects';

if scene == 'Blobbie':
	path = '{}/NIHgrantFigs/BlobbyObject/0.SceneGenerationPythonScripts'.format(rootDir);
	pythonScriptNameName = 'BlobbieInSunRoom.py'

elif scene == 'Reset':
    path = '{}/Reset'.format(rootDir);
    pythonScriptNameName = 'eraseScene.py';

elif scene == 'Test2':
    path = '{}/Reset'.format(rootDir);
    pythonScriptNameName = 'test2.py';
    
elif scene == 'CheckerShadow':
    path = '{}/RenderToolboxDemoScenes/CheckerShadow/Python'.format(rootDir);
    pythonScriptName = 'CheckerShadow.py';
    
elif scene == 'GilChirst-PerpendicularPlanes':
    path = '{}/RenderToolboxDemoScenes/Gilchrist/Python/'.format(rootDir);
    pythonScriptName = 'GilchristScene_PerpendicularPlanes.py';
    
elif scene == 'ColorMaterialVaryingBlobbies':
    path = '{}/1.Projects/1.BlobbiesMaterialColor/1.Python/'.format(rootDir);
    pythonScriptName = 'generateColorMaterialVaryingBlobbies.py'

elif scene == 'BallRoom':
    path = '{}/2.BallRoom/1.Python/'.format(rootDir);
    pythonScriptName = 'generateBallRoom2.py'
   
elif scene == 'Blobbierium':
    path = '{}/5.Blobbierium/1.Python/'.format(rootDir);
    pythonScriptName = 'generateBlobbierium.py'

elif scene == 'BlobbieriumHighDensity':
    path = '{}/5.Blobbierium/1.Python/'.format(rootDir);
    pythonScriptName = 'generateBlobbieriumHighDensity.py'

elif scene == 'BlobbieriumDifferentSizesAndShapes':
    path = '{}/5.Blobbierium/1.Python/'.format(rootDir);
    pythonScriptName = 'generateBlobbieriumDifferentSizesAndShapes.py'
       
filename = '{}/{}'.format(path, pythonScriptName);
exec(compile(open(filename).read(), filename, 'exec'));