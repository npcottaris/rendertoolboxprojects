# Script to create Gilchrist's  perpendicular-planes display scene.
# 
# 10/7/2013  npc  Wrote it. First version.
# 10/11/2013 npc  Second version in which we make sure that the mondrians
#                 in the left/right plane have identical distribution of 
#                 gray levels. Also last mondrian put down should be near
#                 the middle and should have a material with the least 
#                 reflectance in the right side (bright side) and the max
#                 reflectance in the left side (dark side). Also increased
#                 the size of the box/mondrians by 30%
# 11/6/2013  npc  Third version using the SceneUtilsV1 library that is included 
#                 with RenderToolbox3


# Helper method that generates all scene materials and returns them in a dictionary
def generateMaterials(scene):
  from mathutils import Vector

  params = { 'name'              : 'red material',
             'diffuse_shader'    : 'LAMBERT',
             'diffuse_intensity' : 1.0,
             'diffuse_color'     : Vector((1.0, 0.0, 0.0)),
             'specular_shader'   : 'WARDISO',
             'specular_intensity': 1.0,
             'specular_color'    : Vector((1.0, 0., 0.)),
             'alpha'             : 1.0,
        };        
  redMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'floor material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.25,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
        };        
  floorMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'back wall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
        };        
  backWallMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'front wall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
        };        
  frontWallMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'left sidewall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
        };        
  leftWallMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'right sidewall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.25,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
        };        
  rightWallMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'ceiling material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
            'alpha'             : 1.0,
             };        
  ceilingMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'left plane material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.03,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
            'alpha'             : 1.0,
        };
  leftPlaneMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'right plane material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
            'alpha'             : 1.0,
        };
  rightPlaneMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'reflecting panel material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
            'alpha'             : 1.0,
        };        
  reflectingPanelMaterialType = scene.generateMaterialType(params);


  params = {'name'              : 'lower target material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1, 1, 1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
            'alpha'             : 1.0,
        };        
  lowerTargetMaterialType = scene.generateMaterialType(params);

  params = {'name'              : 'upper target material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.03,
            'diffuse_color'     : Vector((1, 1, 1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
            'alpha'             : 1.0,
        };        
  upperTargetMaterialType = scene.generateMaterialType(params);


  # Assemble all generated materials into a dictionary
  materialsDict = {
              'redMaterialType'             : redMaterialType,
				      'floorMaterialType'           : floorMaterialType,
              'backWallMaterialType'        : backWallMaterialType,
              'frontWallMaterialType'       : frontWallMaterialType,
              'leftWallMaterialType'        : leftWallMaterialType,
              'rightWallMaterialType'       : rightWallMaterialType,
              'ceilingMaterialType'         : ceilingMaterialType,
              'leftPlaneMaterialType'       : leftPlaneMaterialType,
              'rightPlaneMaterialType'      : rightPlaneMaterialType,
              'reflectingPanelMaterialType' : reflectingPanelMaterialType,
              'lowerTargetMaterialType'     : lowerTargetMaterialType,
              'upperTargetMaterialType'     : upperTargetMaterialType,
               };
  # and return the dictionary
  return(materialsDict);


# Method that builds the scene
def generateScene(sceneParams):
    # Basic imports
    import sys
    import imp
    import bpy
    from math import pi, floor, cos, sin
    from mathutils import Vector

    # Append the path to my custom Python scene toolbox to the Blender path
    sys.path.append(sceneParams['toolboxDirectory']);
 
    # Import the custom scene toolbox module
    import SceneUtilsV1;
    imp.reload(SceneUtilsV1);


    # scene geometry
    # room dimensions
    roomDimensions = Vector((
                          90,    # x - length (horizontal)
                          56,    # y - length (depth)
                          47,    # z - length (vertical)
                          ));

    roomLocation = Vector((
                          0,                    # x - coord (horizontal)
                         10,                    # y - coord (depth)
                         -roomDimensions.z/2,    # z - coord (vertical)
                          ));

    # perpendicular planes dimensions
    planeWidth  = 18;
    planeHeight = 18; 

    # target dimensions
    targetWidth     = 2.65;
    targetHeight    = 2.4;

    planePosition = Vector((0,6,0));

    # Initialize a sceneManager
    params = { 'name'               : sceneParams['sceneName'],  # name of new scene
               'erasePreviousScene' : True,                      # erase old scene
               'sceneWidthInPixels' : 1024,                      # 1920 pixels along the horizontal-dimension
               'sceneHeightInPixels': 768,                       # 1200 pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0/100.0,                 # set unit scale to 1.0 cm
               'sceneGridSpacing'   : 10.0/100.0,                # set the spacing between grid lines to 10 cm
               'sceneGridLinesNum'  : 20,                        # display 20 grid lines
              };

    scene = SceneUtilsV1.sceneManager(params);

    # Generate the materials
    materialsDict = generateMaterials(scene); 

    # Generate the area lamp model
    params = {'name'  : 'areaLampModel', 
              'color' : Vector((1,1,1)), 
              'fallOffDistance': 30,
              'width1': 19,
              'width2': 15
              }
    brightLight100 = scene.generateAreaLampType(params);

    # - Add the lamp
    rightAreaLampPosition = Vector((
                             roomDimensions.x/2*0.95,      # horizontal position (x-coord)
                             -11,                          # depth position (y-coord)
                             roomDimensions.z/2            # elevation (z-coord)
                           ));
    rightAreaLampLooksAt = Vector((
                            -rightAreaLampPosition.x,      # horizontal position (x-coord)
                             rightAreaLampPosition.y,      # depth position (y-coord)
                             rightAreaLampPosition.z       # elevation (z-coord)
                           ));

    params = {'name'     : 'rightAreaLamp', 
              'model'    : brightLight100, 
              'showName' : True, 
              'location' : rightAreaLampPosition + roomLocation, 
              'lookAt'   : rightAreaLampLooksAt + roomLocation,
          };
    rightAreaLamp = scene.addLampObject(params);



    # Define our camera
    nearClipDistance = 0.1;
    farClipDistance  = 100;
    params = {'clipRange'            : Vector((nearClipDistance ,  farClipDistance)),
              'fieldOfViewInDegrees' : 36,   # horizontal FOV
              'drawSize'             : 2,    # camera wireframe size
             };
    cameraType = scene.generateCameraType(params);
 
    # Add the camera
    cameraPosition = Vector((
                           0.0,                  # horizontal position (x-coord)
                         -76.4,                  # depth position (y-coord)
                             0,                  # elevation (z-coord)
                           ));
    cameraLooksAt = Vector((
                          cameraPosition.x,      # horizontal position (x-coord)
                          0,                     # depth position (y-coord)
                          cameraPosition.z       # elevation (z-coord)
                           ));

    params = {  'name'          :'Camera', 
                'cameraType'    : cameraType,
                'location'      : cameraPosition,     
                'lookAt'        : cameraLooksAt,
                'showName'      : True,
          };          
    mainCamera = scene.addCameraObject(params);



    # Add the reflecting panel
    reflectingPanelHeight = roomDimensions.z;
    reflectingPanelWidth  = roomDimensions.y/1.6;
    params = {'name'        : 'ReflectingPanel',
              'scaling'     : Vector((reflectingPanelHeight, reflectingPanelWidth, 0.1)),
              'rotation'    : Vector((0, pi/2, pi/4)),
              'location'    : Vector((-roomDimensions.x/2+cos(pi/4)*reflectingPanelWidth/2, -15, reflectingPanelHeight/2)) + roomLocation,
              'material'    : materialsDict['reflectingPanelMaterialType'],
              'flipNormal'  : False,
            };
    scene.addPlanarQuad(params); 



    sideLabel = ['Left', 'Right'];
    parentObj = [0, 0];

    # Add the perpendicular planes structure
    for sideIndex in range(0,2):
        # 1. create parent object
        params = {'name'       : '{} base plate'.format(sideLabel[sideIndex]),
                  'vertices'   : [(-1, 0, 0),(1, 0, 0),(1, 0, 0),(-1, 0, 0)],
                  'scaling'    : Vector((1, 1, 1)),
                  'rotation'   : Vector((0, 0, 0)),
                  'location'   : Vector((0,0,0)),
                  'material'   : materialsDict['redMaterialType'],
                  'flipNormal' : False,
                };
        parentObj[sideIndex] = scene.addPlanarQuad(params);

        # 2. add the background plane
        params = {'name'        : '{} background plane'.format(sideLabel[sideIndex]),
                  'scaling'     : Vector((planeWidth, planeHeight, 0.1)),
                  'rotation'    : Vector((pi/2, 0, 0)),
                  'location'    : Vector((0.0, 0.0, 0.0)),
                  'material'    : materialsDict['rightWallMaterialType'],
                  'flipNormal'  : False,
          };
        backPlane        = scene.addPlanarQuad(params);
        backPlane.parent = parentObj[sideIndex];


        # 3a. configure the lower target whose parent is the left plane
        if sideLabel[sideIndex] == 'Left':
          targetXpos = planeWidth/2+targetWidth;  
          targetYpos = -planeHeight/4 - targetHeight/8;
          upperLinearPerspectiveDistortion =  1.0+0.02;
          lowerLinearPerspectiveDistortion = -0.8+0.05;
          params = {'name'       : 'lower target',
                    'vertices'   : [(-1, -1, 0),(1, lowerLinearPerspectiveDistortion, 0),(1, upperLinearPerspectiveDistortion, 0),(-1, 1, 0)],
                    'scaling'    : Vector((targetWidth, targetHeight, 0.1)),
                    'rotation'   : Vector((pi/2, 0, 0)),
                    'location'   : Vector((targetXpos, -0.05, targetYpos)),
                    'material'   : materialsDict['lowerTargetMaterialType'],
                    'flipNormal' : False,
                };
        # 3b. configurethe lower target whose parent is the right plane
        elif sideLabel[sideIndex] == 'Right':
          # zoffset the target (move it forwards a little but) to avoid the dark vertical line on upper target
          zoffset = -0.02;
          targetXpos = -(planeWidth/2+targetWidth);  
          targetYpos =  planeHeight/4+ targetHeight/8;
          upperLinearPerspectiveDistortion =  0.8-0.05;
          lowerLinearPerspectiveDistortion = -1.0-0.02;
          params = {'name'       : 'upper target',
                    'vertices'   : [(-1, lowerLinearPerspectiveDistortion, 0),(1, -1, 0),(1, 1, 0),(-1, upperLinearPerspectiveDistortion, 0)],
                    'scaling'    : Vector((targetWidth, targetHeight, 0.1)),
                    'rotation'   : Vector((pi/2, 0, 0)),
                    'location'   : Vector((targetXpos, zoffset, targetYpos)),
                    'material'   : materialsDict['upperTargetMaterialType'],
                    'flipNormal' : False,
                };

        # add the target to the scene
        target = scene.addPlanarQuad(params);
        target.parent = parentObj[sideIndex];




        # transform the parent objects in order to place the two arms
        if sideLabel[sideIndex] == 'Left':
          rotationVector = Vector((0, 0, -pi/4))
          positionVector = Vector((-planeWidth/2*sin(pi/4), planeHeight/4*cos(pi/4), roomDimensions.z/2));
        else:
          rotationVector = Vector((0, 0, pi/4))
          # z-offset the right wing (send it backwards a little bit) to avoid the vertical bright line in lower target
          zoffset = 0.02;
          positionVector = Vector((planeWidth/2*sin(pi/4), planeHeight/4*cos(pi/4)+zoffset, roomDimensions.z/2));
        
        # rotate and translate parent objects
        parentObj[sideIndex].rotation_euler = rotationVector;
        parentObj[sideIndex].location       = positionVector + planePosition + roomLocation;




    # Add the enclosing room
    params = { 'floorName'             : 'floor',
               'backWallName'          : 'backWall',
               'frontWallName'         : 'frontWall',
               'leftWallName'          : 'leftWall',
               'rightWallName'         : 'rightWall',
               'ceilingName'           : 'ceiling',
               'floorMaterialType'     : materialsDict['floorMaterialType'],
               'backWallMaterialType'  : materialsDict['backWallMaterialType'],
               'frontWallMaterialType' : materialsDict['frontWallMaterialType'],
               'leftWallMaterialType'  : materialsDict['leftWallMaterialType'],
               'rightWallMaterialType' : materialsDict['rightWallMaterialType'],
               'ceilingMaterialType'   : materialsDict['ceilingMaterialType'],
               'roomWidth'             : roomDimensions.x,
               'roomDepth'             : roomDimensions.y,
               'roomHeight'            : roomDimensions.z,
               'roomLocation'          : roomLocation,
               'wallThickness'         : 0.3,
              }
    enclosingRoomSurfaces = scene.addRoom(params);

    # make an elliptical opening in the front wall of the enclosing room
    circleSeparation = 2;
    params = {'name'       : 'boringObject',
              'scaling'    : Vector((14,14,14)),
              'rotation'   : Vector((0, 0, 0)),
              'location'   : Vector((-circleSeparation, -roomDimensions.y/2, roomDimensions.z/2)) + roomLocation,
              'material'   : materialsDict['rightWallMaterialType'],
              'flipNormal' : False,
            };
    boringObject = scene.addSphere(params);
    scene.boreOut(enclosingRoomSurfaces['frontWallPlane'], boringObject, True);

    params['location'] = Vector((circleSeparation, -roomDimensions.y/2, roomDimensions.z/2)) + roomLocation;
    boringObject = scene.addSphere(params);
    scene.boreOut(enclosingRoomSurfaces['frontWallPlane'], boringObject, True);


    # Finally, export collada file
    scene.exportToColladaFile(sceneParams['exportsDirectory']);


# ------- main() ------


rootDirectory    = '/Users/cottaris/Documents/1.Code/1.Python/1.BlenderScenes/Gilchrist';
exportsDirectory = '{}'.format(rootDirectory);
toolboxDirectory = '/Users/Shared/Matlab/Toolboxes/RenderToolbox3/BlenderPythonSceneUtils';

sceneParams = { 'sceneName'        : 'Gilchrist_PerpendicularPlanes',
                'exportsDirectory' : '/Users/cottaris/Documents/2.Presentations/PythonDrivenBlender/ColladaExports',
                'toolboxDirectory' : '/Users/Shared/Matlab/Toolboxes/RenderToolbox3/BlenderPythonSceneUtils'
             };

generateScene(sceneParams);
