# Script to create Gilchrist's  perpendicular-planes display scene.
# 
# 10/7/2013  npc  Wrote it. First version.
# 10/11/2013 npc  Second version in which we make sure that the mondrians
#                 in the left/right plane have identical distribution of 
#                 gray levels. Also last mondrian put down should be near
#                 the middle and should have a material with the least 
#                 reflectance in the right side (bright side) and the max
#                 reflectance in the left side (dark side). Also increased
#                 the size of the box/mondrians by 30%

# Setup paths
# MacPro
rootDirectory    = '/Users/Nicolas/Documents/1.Code/PythonDrivenBlender-RT3pipeline';
# iMac 
rootDirectory    = '/Users/Shared/Matlab/Nicolas/Code/PythonDrivenBlender-RT3pipeline';
# retina MacBookPro
rootDirectory    = '/Users/Shared/Matlab/Nicolas/PythonDrivenBlender-RT3pipeline';


toolboxDirectory = '{}/BlenderPythonScripts/SceneToolbox'.format(rootDirectory);
exportsDirectory = '{}/Exports'.format(rootDirectory);
mitsubaPath      = '{}/MitsubaLatestBinary/Mitsuba.app/Contents/MacOS/'.format(rootDirectory);

if not("bpy" in locals()):
    import bpy
    import sys
    sys.path.append(toolboxDirectory);
    # Import mathutils, math, and random modules
    from mathutils import *
    from math import *
    from random import * 
    # Import my scene utils module
    import SceneUtils
    # Import my man-made objec generator nodule
    import ManMadeObjectGenerators
  
import imp
imp.reload(SceneUtils);
imp.reload(ManMadeObjectGenerators);  

 
planeIsArticulated = True;

if planeIsArticulated:
    sceneName = 'PerpendicularPlanesScene_Version2';
else:
    sceneName = 'PerpendicularPlaneSceneNoArticulation_Version2';
    
# Instantiate a SceneManager object
sceneParams = { 'name'               : sceneName,            # name of new scene
                'erasePreviousScene' : True,                 # erase old scene
                'sceneWidthInPixels' : 1920,                 # 1920 pixels along the horizontal-dimension
                'sceneHeightInPixels': 1200,                 # 1200 pixels along the vertical-dimension
                'sceneUnitScale'     : 1.0/100.0,            # set unit scale to 1.0 cm
                'sceneGridSpacing'   : 10.0/100.0,           # set the spacing between grid lines to 10 cm
                'sceneGridLinesNum'  : 20,                   # display 20 grid lines
                'MitsubaBinaryPath'  : mitsubaPath,
                };
roomScene = SceneUtils.SceneManager(sceneParams);


# ---------- Generate our camera type --------------
nearClipDistance = 0.1;
farClipDistance  = 100;
params = {  'clipRange'             : Vector((nearClipDistance , farClipDistance)),
            'fieldOfViewInDegrees'  : 36,   # horizontal FOV
            'drawSize'              : 2,    # camera wireframe size
         };
cameraType = roomScene.generateCameraType(params);

# -------------- Add a camera object ------------------
cameraHorizPosition =  0.0;
cameraDepthPosition = -76.5;
cameraElevation = 0.0;

# parallel off-axis camera setup
params = {  'name'          :'Camera', 
            'cameraType'    : cameraType,
            'location'      : Vector((cameraHorizPosition, cameraDepthPosition, cameraElevation)),     
            'lookAt'        : Vector((cameraHorizPosition,  # x-position (horizontal)
                                      0,    # y-position (depth) 
                                      cameraElevation,  # z-position (vertical)
                                      )),
            'showName'      : True,
          };          
roomScene.addCameraObject(params);



# Generate materials
params = {  'name'              : 'red material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 1.0,
            'diffuse_color'     : Vector((1.0, 0.0, 0.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 1.0,
            'specular_color'    : Vector((1.0, 0., 0.)),
        };        
redMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'back wall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
backWallMaterialType = roomScene.generateMaterialType(params);


params = {  'name'              : 'floor material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.25,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
floorMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'right sidewall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.25,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
rightSideWallMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'left sidewall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
leftSideWallMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'ceiling material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
ceilingMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'front wall material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.0,
            'diffuse_color'     : Vector((1.0, 1.0, 1.0)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((0.8, 0.8, 0.8)),
        };        
frontWallMaterialType = roomScene.generateMaterialType(params);




planeMaterialsList = [0 for k in range(0, 2)];

params = {  'name'              : 'left plane material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.03,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
        };        
planeMaterialsList[0] = roomScene.generateMaterialType(params);

params = {  'name'              : 'right plane material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
        };        
planeMaterialsList[1] = roomScene.generateMaterialType(params);


params = {  'name'              : 'reflecting panel material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1,1,1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
        };        
reflectingPanelMaterialType = roomScene.generateMaterialType(params);



params = {  'name'              : 'lower target material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.9,
            'diffuse_color'     : Vector((1, 1, 1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
        };        
lowerTargetMaterialType = roomScene.generateMaterialType(params);

params = {  'name'              : 'upper target material',
            'diffuse_shader'    : 'LAMBERT',
            'diffuse_intensity' : 0.03,
            'diffuse_color'     : Vector((1, 1, 1)),
            'specular_shader'   : 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color'    : Vector((1,1,1)),
        };        
upperTargetMaterialType = roomScene.generateMaterialType(params);


mondrianMaterialsNum = 10;
mondrianMaterialsList = [0 for k in range(0, mondrianMaterialsNum)];

for k in range(0, mondrianMaterialsNum):
    randomGray = random();
    materialName = 'mondrian material {}'.format(k);
    print('Generating material named: {}'.format(materialName));
    
    params = {  'name'              : materialName,
                'diffuse_shader'    : 'LAMBERT',
                'diffuse_intensity' : k/10+0.05,
                'diffuse_color'     : Vector((1,1,1)),
                'specular_shader'   : 'WARDISO',
                'specular_intensity': 0.0,
                'specular_color'    : Vector((0.8, 0.8, 0.5)),
            };        
    mondrianMaterialsList[k] = roomScene.generateMaterialType(params);

# -------- Generate our area lamp type -------------------
# set mitsuba lamp modifiers

lampIsAreaLamp = True;
lampIsPointLamp = False;
lampIsSpotLamp = False;
if lampIsAreaLamp:
    mitsubaLampModifiers = {};
    mitsubaLampModifiers = {
                            'intensity'     : 1,
                            'samplingWeight': 1,
                        };
    params = {  'name'                : 'areaLamp',
                'color'               : Vector((1.0, 1.0, 1.0)),
                'width1'              : 38/2,
                'width2'              : 30/2,
                'fallOffDistance'     : 30,
                'mitsubaLampModifiers': mitsubaLampModifiers,
            };
    lampType = roomScene.generateAreaLampType(params);
    
if lampIsPointLamp:
    params = {  'name'                : 'pointLamp',
                'color'               : Vector((1.0, 1.0, 1.0)),
                'fallOffType'         : 'INVERSE_LINEAR',
                'fallOffDistance'     : 40,
            };
    lampType = roomScene.generatePointLampType(params);       

if lampIsSpotLamp:
    params = {  'name'                : 'spotLamp',
                'color'               : Vector((1.0, 1.0, 1.0)),
                'fallOffType'         : 'INVERSE_LINEAR',
                'fallOffDistance'     : 240,
                'spotSizeInDegrees'   : 90,
            };
    lampType = roomScene.generateSpotLampType(params);   



# scale everything according to ratio of our viewing distance (76.4 cm) to that one of Gilchrist (42 cm)
scaleFactor     = 76.4/42;
boxEnlargementFactor = 1.3;
planeWidth      = 11.0*boxEnlargementFactor*scaleFactor;
planeDepth      = 11.0*boxEnlargementFactor*scaleFactor;
targetWidth     = 4.5*boxEnlargementFactor*scaleFactor;
targetHeight        = 4.0*boxEnlargementFactor*scaleFactor;
lampHorizPosition   =  24*scaleFactor;
lampDepthPosition   = -6*scaleFactor;

backWallDepthPosition = -20*scaleFactor;
roomWidth             =  50*scaleFactor;
roomHeight            =  26*scaleFactor;
roomDepth             =  31*scaleFactor;

# Generate the scene
# 1. Add the room lamp
params = {  'name'          : 'LeftLampObject', 
            'type'          : lampType,
            'location'      : Vector((lampHorizPosition, lampDepthPosition, cameraElevation)),
            'lookAt'        : Vector((-lampHorizPosition, lampDepthPosition, cameraElevation)),   # look at display cross
            'showName'      : True,
          };          
roomScene.addLampObject(params);


# 2. Add the backwall
params = {  'name'        : 'BackWall',
            'scaling'     : Vector((roomWidth, roomHeight, 0.1)),
            'rotation'    : Vector((pi/2, 0, 0)),
            'location'    : Vector((0.0, -backWallDepthPosition/2, 0)),
            'materialType': backWallMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params);      


# 3. Add the floor
params = {  'name'        : 'Floor',
            'scaling'     : Vector((roomWidth, roomDepth, 0.1)),
            'rotation'    : Vector((0, 0, 0.0)),
            'location'    : Vector((0.0, -roomDepth/2-backWallDepthPosition/2, -roomHeight/2)),
            'materialType': floorMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params); 

# 4. Add the right side wall
params = {  'name'        : 'Right Side Wall',
            'scaling'     : Vector((roomHeight, roomDepth, 0.1)),
            'rotation'    : Vector((0, pi/2, 0)),
            'location'    : Vector((roomWidth/2, -roomDepth/2-backWallDepthPosition/2, 0)),
            'materialType': rightSideWallMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params); 


# 5. Add the left side wall
params = {  'name'        : 'Left Side Wall',
            'scaling'     : Vector((roomHeight, roomDepth, 0.1)),
            'rotation'    : Vector((0, pi/2, 0)),
            'location'    : Vector((-roomWidth/2, -roomDepth/2-backWallDepthPosition/2, 0)),
            'materialType': leftSideWallMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params); 


# 6. Add the ceiling
params = {  'name'        : 'Ceiling',
            'scaling'     : Vector((roomWidth, roomDepth, 0.1)),
            'rotation'    : Vector((0, 0, 0.0)),
            'location'    : Vector((0.0, -roomDepth/2-backWallDepthPosition/2, roomHeight/2)),
            'materialType': ceilingMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params); 


# 7. Add the reflecting panel
reflectingPanelHeight = roomHeight;
reflectingPanelWidth  = roomDepth/1.6;
params = {  'name'        : 'ReflectingPanel',
            'scaling'     : Vector((reflectingPanelHeight, reflectingPanelWidth, 0.1)),
            'rotation'    : Vector((0, pi/2, pi/4)),
            'location'    : Vector((-roomWidth/2+cos(pi/4)*reflectingPanelWidth/2, -15, 0)),
            'materialType': reflectingPanelMaterialType,
            'flipNormal'  : False,
          };
roomScene.addPlaneObject(params); 



  
basePlate = [0 for k in range(0,2)];
sideName  = ['Left', 'Right'];

for side in range(0,2):
    # create parent object for left arm
    params = {  'name'        : '{} base plate'.format(sideName[side]),
                    'vertices'    : [(-1, -0.0, 0),(1, -0.0, 0),(1, 0, 0),(-1, 0, 0)],
                    'scaling'     : Vector((1, 1, 1)),
                    'rotation'    : Vector((0, 0, 0)),
                    'location'    : Vector((0,0,0)),
                    'materialType': redMaterialType,
                    'flipNormal'  : False,
                };
    basePlate[side] = roomScene.addQuadObject(params);
    
    # add background plane
    params = {  'name'        : '{} background plane'.format(sideName[side]),
                'scaling'     : Vector((planeWidth, planeDepth, 0.1)),
                'rotation'    : Vector((pi/2, 0, 0)),
                'location'    : Vector((0.0, 0.0, 0.0)),
                'materialType': planeMaterialsList[side],
                'flipNormal'  : False,
          };
    obj= roomScene.addPlaneObject(params);
    obj.parent = basePlate[side];

    
    # add articulation planes
    if planeIsArticulated:
        mondrianChipsNum = 50;
        for k in range(0,mondrianChipsNum):
            mondrianWidth  = boxEnlargementFactor*(1.5+random()*1)*scaleFactor;
            mondrianHeight = boxEnlargementFactor*(1.5+random()*1)*scaleFactor;
            maxMondrianSize = max(mondrianWidth,mondrianHeight);
            xPos = (random()-0.5)*(planeWidth-maxMondrianSize);
            yPos = (random()-0.5)*(planeWidth-maxMondrianSize);
            depthPos = -random()*0.1;
            materialIndex = k % mondrianMaterialsNum;
            if (sideName[side] == 'Left') and (k == mondrianChipsNum-1):
                # make it square
                mondrianWidth = 4.0;
                mondrianHeight = 4.0
                # place it at the center (slightly higher)
                xPos = 0.0;
                yPos = 0.1;
                # shortest depth position so it is not hidden by other chips
                depthPos = -0.105
                # assign it the brightest material
                materialIndex = mondrianMaterialsNum-1;
            
            if (sideName[side] == 'Right') and (k == mondrianChipsNum-1):
                # make it square
                mondrianWidth = 4.0;
                mondrianHeight = 4.0;
                # place it at the center (slightly lower)
                xPos = 0.0;
                yPos = -0.1;
                # shortest depth position so it is not hidden by other chips
                depthPos = -0.105
                # assign it the darkest material
                materialIndex = 0;
                    
            params = {  'name'        : '{} mondrian plane no {}'.format(sideName[side], k),
                        'scaling'     : Vector((mondrianWidth, mondrianHeight, 0.1)),
                        'rotation'    : Vector((pi/2, 0, 0)),
                        'location'    : Vector((xPos, depthPos, yPos)),
                        'materialType': mondrianMaterialsList[materialIndex],
                        'flipNormal'  : False,
                    };
            obj = roomScene.addPlaneObject(params);
            obj.parent = basePlate[side];
        
    if sideName[side] == 'Left':
        # add lower target plane
        # xoffset the target (move it towards the left a bit) to avoid the bright vertical line 
        xoffset = -0.0;
        targetXpos = planeWidth/2+targetWidth/2 + xoffset;  
        targetYpos = -3.0*scaleFactor*boxEnlargementFactor;
        upperLinearPerspectiveDistortion = 1.0+0.02;
        lowerLinearPerspectiveDistortion = -0.8+0.05;
        params = {  'name'        : 'lower target',
                    'vertices'    : [(-1, -1, 0),(1, lowerLinearPerspectiveDistortion, 0),(1, upperLinearPerspectiveDistortion, 0),(-1, 1, 0)],
                    'scaling'     : Vector((targetWidth, targetHeight, 0.1)),
                    'rotation'    : Vector((pi/2, 0, 0)),
                    'location'    : Vector((targetXpos, -0.05, targetYpos)),
                    'materialType': lowerTargetMaterialType,
                    'flipNormal'  : False,
                };
    else:
        # add upper target plane
        xoffset = 0;
        # zoffset the target (move it forwards a little but) to avoid the dark vertical line on upper target
        zoffset = -0.02;
        targetXpos = -(planeWidth/2+targetWidth/2 + xoffset);
        targetYpos = 3.0*scaleFactor*boxEnlargementFactor;
        upperLinearPerspectiveDistortion = 0.8-0.05;
        lowerLinearPerspectiveDistortion = -1.0-0.02;
        params = {  'name'        : 'upper target',
                    'vertices'    : [(-1, lowerLinearPerspectiveDistortion, 0),(1, -1, 0),(1, 1, 0),(-1, upperLinearPerspectiveDistortion, 0)],
                    'scaling'     : Vector((targetWidth, targetHeight, 0.1)),
                    'rotation'    : Vector((pi/2, 0, 0)),
                    'location'    : Vector((targetXpos, zoffset, targetYpos)),
                    'materialType': upperTargetMaterialType,
                    'flipNormal'  : False,
                };
            
    obj = roomScene.addQuadObject(params);
    obj.parent = basePlate[side];


    # transform the parent objects in order to place the two arms
    if sideName[side] == 'Left':
        rotationVector = Vector((0, 0, -pi/4))
        positionVector = Vector((-planeWidth/4*sin(pi/4), planeDepth/4*cos(pi/4), cameraElevation));
    else:
        rotationVector = Vector((0, 0, pi/4))
        # z-offset the right wing (send it backwards a little bit) to avoid the vertical bright line in lower target
        zoffset = 0.1;
        positionVector = Vector((planeWidth/4*sin(pi/4), planeDepth/4*cos(pi/4)+zoffset, cameraElevation));
        
    xformParams = { 'sizeVector':     Vector((1,1,1)),
                    'rotationVector': rotationVector,
                    'positionVector': positionVector,
                };
    roomScene.transformObject(basePlate[side], xformParams);
    #basePlate[side].hide = True;
    #basePlate[side].hide_render = True;


# Export collada file
roomScene.exportToColladaFile(exportsDirectory);  