def generateScene(sceneParams): 
    # Basic imports
    import sys
    import imp
    import bpy
    from math import pi, floor
    from mathutils import Vector


    # Append the path to my custom Python scene toolbox to the Blender path
    sys.path.append(sceneParams['toolboxDirectory']);
 
    # Import the custom scene toolbox module
    import SceneUtilsV1;
    imp.reload(SceneUtilsV1);
 
    # Initialize a sceneManager
    params = { 'name'               : sceneParams['sceneName'],  # name of new scene
               'erasePreviousScene' : True,                      # erase old scene
               'sceneWidthInPixels' : 1024,                      # 1920 pixels along the horizontal-dimension
               'sceneHeightInPixels': 768,                       # 1200 pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0/100.0,                 # set unit scale to 1.0 cm
               'sceneGridSpacing'   : 10.0/100.0,                # set the spacing between grid lines to 10 cm
               'sceneGridLinesNum'  : 20,                        # display 20 grid lines
              };

    scene = SceneUtilsV1.sceneManager(params);

    # Generate the materials
    # -the cylinder material
    params = { 'name'              : 'cylinderMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.0, 1.0, 0.0)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.1,
               'specular_color'    : Vector((0.0, 1.0, 0.0)),
               'alpha'             : 1.0
             }; 
    cylinderMaterialType = scene.generateMaterialType(params);

    # -the room material
    params = { 'name'              : 'roomMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.6, 0.6, 0.6)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((1.0, 1.0, 1.0)),
               'alpha'             : 1.0
             }; 
    roomMaterialType = scene.generateMaterialType(params);

    # -the cloth material
    params = { 'name'              : 'clothMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.5,
               'diffuse_color'     : Vector((0.7, 0.0, 0.1)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((0.7, 0.0, 0.1)),
               'alpha'             : 1.0
             }; 
    clothMaterialType = scene.generateMaterialType(params);

    # -the dark check material
    params = { 'name'              : 'darkCheckMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.1,
               'diffuse_color'     : Vector((0.5, 0.5, 0.5)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((0.5, 0.5, 0.5)),
               'alpha'             : 1.0
             }; 
    darkCheckMaterialType = scene.generateMaterialType(params);


    # -the light check material
    params = { 'name'              : 'lightCheckMaterial',
               'diffuse_shader'    : 'LAMBERT',
               'diffuse_intensity' : 0.7,
               'diffuse_color'     : Vector((0.55, 0.55, 0.40)),
               'specular_shader'   : 'WARDISO',
               'specular_intensity': 0.0,
               'specular_color'    : Vector((0.5, 0.5, 0.5)),
               'alpha'             : 1.0
             }; 
    lightCheckMaterialType = scene.generateMaterialType(params);

    checkBoardMaterialsList = [lightCheckMaterialType, darkCheckMaterialType];


    # Generate the area lamp model
    params = {'name'  : 'areaLampModel', 
              'color' : Vector((1,1,1)), 
              'fallOffDistance': 120,
              'width1': 20,
              'width2': 15
              }
    brightLight100 = scene.generateAreaLampType(params);
 
    # - Add the left area lamp
    leftAreaLampPosition = Vector((
                            -56,      # horizontal position (x-coord)
                             56,      # depth position (y-coord)
                             16       # elevation (z-coord)
                           ));
    leftAreaLampLooksAt = Vector((
                            -71,      # horizontal position (x-coord)
                             71,      # depth position (y-coord)
                             20       # elevation (z-coord)
                           ));

    params = {'name'     : 'leftAreaLamp', 
              'model'    : brightLight100, 
              'showName' : True, 
              'location' : leftAreaLampPosition, 
              'lookAt'   : -leftAreaLampLooksAt
          };
    leftAreaLamp = scene.addLampObject(params);

    # - Add the front area lamp
    frontAreaLampPosition = Vector((
                              0,      # horizontal position (x-coord)
                            -50,      # depth position (y-coord)
                             50       # elevation (z-coord)
                           ));
    frontAreaLampLooksAt = Vector((
                              0,      # horizontal position (x-coord)
                            -90,      # depth position (y-coord)
                             90       # elevation (z-coord)
                           ));

    params = {'name'     : 'frontAreaLamp', 
              'model'    : brightLight100, 
              'showName' : True, 
              'location' : frontAreaLampPosition, 
              'lookAt'   : -frontAreaLampLooksAt
          };
    frontAreaLamp = scene.addLampObject(params);



    # Define our camera
    nearClipDistance = 0.1;
    farClipDistance  = 300;
    params = {'clipRange'            : Vector((nearClipDistance ,  farClipDistance)),
              'fieldOfViewInDegrees' : 36,   # horizontal FOV
              'drawSize'             : 2,    # camera wireframe size
             };
    cameraType = scene.generateCameraType(params);
 
    # Add the camera
    cameraHorizPosition = -57;
    cameraDepthPosition = -74;
    cameraElevation = 45;
    params = {  'name'          :'Camera', 
                'cameraType'    : cameraType,
                'location'      : Vector((cameraHorizPosition, cameraDepthPosition, cameraElevation)),     
                'lookAt'        : Vector((-13,-17,10)),
                'showName'      : True,
          };          
    mainCamera = scene.addCameraObject(params);


    # Define the checkerboard geometry
    boardThickness    = 2.5;
    boardHalfWidth    = 14;
    tilesAlongEachDim = 4;  # this must be even number
    boardIsDimpled    = True;

    # compute checker size
    N                 = floor(tilesAlongEachDim/2);
    deltaX            = boardHalfWidth/N;
    deltaY            = deltaX;

    # Add the checks of the checkerboard
    tileParams = {'name'    : '',
                  'scaling' : Vector((deltaX/2, deltaY/2, boardThickness/2)),
                  'rotation': Vector((0,0,0)), 
                  'location': Vector((0,0,0)),
                  'material': checkBoardMaterialsList[0]
                  };
 
    # Modify the checks if the board is dimpled
    if boardIsDimpled:
        sphereParams = { 'name'     : 'theSphere',
                         'scaling'  : Vector((1.0, 1.0, 1.0))*deltaX/2, 
                         'location' : Vector((0,0,0)),
                         'material' : checkBoardMaterialsList[0],
               };
        indentation = 0.09;

    for ix in list(range(-N,N+1)):
       for iy in list(range(-N,N+1)): 
            tileParams['name']     = 'floorTileAt({0:1d},{1:2d})'.format(ix,iy);
            tileParams['location'] =  Vector((ix*deltaX, iy*deltaY, boardThickness*0.5));
            tileParams['material'] =  checkBoardMaterialsList[(ix+iy)%2];
            theTile = scene.addCube(tileParams);
            if boardIsDimpled:
                theSphere          = scene.addSphere(sphereParams);
                theSphere.location = theTile.location + Vector((0,0,deltaX/2*(1-indentation)));
                scene.boreOut(theTile, theSphere, True);


    # Add the cylinder (outer shell)
    cylinderWidth  = 6.2;
    cylinderHeight = 11;
    params = { 'name'    : 'The cylinder',
               'scaling' : Vector((cylinderWidth, cylinderWidth, cylinderHeight)),
               'rotation': Vector((0,0,0)), 
               'location': Vector((-9.4, 9.4, cylinderHeight/2+boardThickness)),
               'material': cylinderMaterialType,
             }; 
    theCylinder = scene.addCylinder(params);

    # Generate the cylinder core (inner shell)
    deltaHeight = 1.4;
    cylinderWidth  *= 0.85;
    cylinderHeight -= deltaHeight;
    params = { 'name'    : 'The cylinder core',
               'scaling' : Vector((cylinderWidth, cylinderWidth, cylinderHeight)),
               'rotation': Vector((0,0,0)), 
               'location': Vector((-9.4, 9.4, cylinderHeight/2 + deltaHeight+boardThickness)),
               'material': cylinderMaterialType,
             }; 
    theCylinderCore = scene.addCylinder(params);
    scene.boreOut(theCylinder, theCylinderCore, True);


    # Add the cloth
    xBinsNum = 501;  
    yBinsNum = 501;
    elevationMap = SceneUtilsV1.createRandomGaussianBlobsMap(xBinsNum, yBinsNum);
    
    params = { 'name'         : 'The Cloth',
               'scale'        : Vector((boardHalfWidth*4.0, boardHalfWidth*4.0, boardThickness*.4)),
               'rotation'     : Vector((0,0,0)),
               'location'     : Vector((0,0, 0.05)), 
               'xBinsNum'     : xBinsNum,
               'yBinsNum'     : yBinsNum,
               'elevationMap' : elevationMap,
               'material'     : clothMaterialType,
               };
    theCloth = scene.addElevationMapObject(params);


    # Add the enclosing room
    params = {  'floorName'             : 'floor',
                'backWallName'          : 'backWall',
                'frontWallName'         : 'frontWall',
                'leftWallName'          : 'leftWall',
                'rightWallName'         : 'rightWall',
                'ceilingName'           : 'ceiling',
                'floorMaterialType'     : roomMaterialType,
                'backWallMaterialType'  : roomMaterialType,
                'frontWallMaterialType' : roomMaterialType,
                'leftWallMaterialType'  : roomMaterialType,
                'rightWallMaterialType' : roomMaterialType,
                'ceilingMaterialType'   : roomMaterialType,
                'roomWidth'     : 180,
                'roomDepth'     : 180,
                'roomHeight'    : 100,
                'roomLocation'  : Vector((0,0,0))
              }
    scene.addRoom(params);

    # Finally, export collada file
    scene.exportToColladaFile(sceneParams['exportsDirectory']);


# ------- main() ------


rootDirectory    = '/Users/nicolas/Documents/0.RTB3projects/NIHgrantFigs/BlobyObject';
exportsDirectory = '{}'.format(rootDirectory);
toolboxDirectory = '/Users/Shared/Matlab/Toolboxes/RenderToolbox3/Utilities/BlenderPython';

sceneParams = { 'sceneName'        : 'CheckerShadowNew',
                'exportsDirectory' : exportsDirectory,
                'toolboxDirectory' : toolboxDirectory,
             };

generateScene(sceneParams);


