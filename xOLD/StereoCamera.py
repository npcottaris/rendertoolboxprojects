#!/usr/local/bin/python3.3

#Import the Blender Python API
import bpy 

#Import Blender vector math utilities
from mathutils import Vector, Euler

#Import the standard Python math library
import math 


def createScenes():
	
	# delete left and right camera scenes if exists 
	try:
		bpy.data.scenes.remove(bpy.data.scenes['Left Camera Scene'])
		bpy.data.scenes.remove(bpy.data.scenes['Right Camera Scene'])
	except:
		pass

	center_scene = bpy.context.screen.scene;
	center_cam   = bpy.context.scene.camera;
	tmp_cam      = bpy.context.scene.camera;
	center_cam_name = bpy.context.scene.camera.name;


	# stero cam separation in 1/1000 Blender units
	stereo_camera_separation = 150;

	# Add left camera
	left_cam = bpy.data.cameras.new('L_'+center_cam_name)
	left_cam_obj = bpy.data.objects.new('L_'+center_cam_name, left_cam);
	left_cam_obj.location = -(stereo_camera_separation/1000)/2,0,0
	left_cam_obj.rotation_euler = (0.0,0.0,0.0) # reset
	bpy.context.scene.objects.link(left_cam_obj);

	# Add right camera
	right_cam = bpy.data.cameras.new('R_'+center_cam_name)
	right_cam_obj = bpy.data.objects.new('R_'+center_cam_name, right_cam)
	right_cam_obj.location = (stereo_camera_separation/1000)/2,0,0
	right_cam_obj.rotation_euler = (0.0,0.0,0.0) # reset
	bpy.context.scene.objects.link(right_cam_obj) 


	# add the left/right camera and zero-parallax-plane as child
	left_cam_obj.parent = center_cam
	right_cam_obj.parent = center_cam


	# Create Left Scene
	bpy.ops.scene.new(type='LINK_OBJECTS')
	left_scene = bpy.context.scene
	left_scene.name = "Left Camera Scene"
	left_scene.camera = bpy.data.objects["L_"+center_cam_name]
	left_scene.background_set = center_scene

	# Create Right Scene
	bpy.ops.scene.new(type='LINK_OBJECTS')
	right_scene = bpy.context.scene
	right_scene.name = "Right Camera Scene"
	right_scene.camera = bpy.data.objects["R_"+center_cam_name]
	right_scene.background_set = center_scene

	# Create Right Scene
	#bpy.ops.scene.new(type='LINK_OBJECTS')
	#other_scene = bpy.context.scene
	#other_scene.name = "Other Camera Scene"
	#other_scene.background_set = center_scene


	# back to the center scene
	bpy.context.screen.scene = center_scene

	# select the center camera (object mode)
	bpy.ops.object.select_all(action='DESELECT')
	bpy.context.scene.camera = tmp_cam
	bpy.context.scene.objects.active = tmp_cam
	tmp_cam.select = True;


# main
createScenes();