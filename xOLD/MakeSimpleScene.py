#!/usr/local/bin/python3.3

#Import the Blender Python API
import bpy 

#Import Blender vector math utilities
from mathutils import Vector, Euler

#Import the standard Python math library
import math 

# Method to remove all objects of the current scene
def removeAllObjects():
	print('---------------------------------------')
	print('Found {} objects'.format(len(bpy.data.objects)))
	for object in bpy.data.objects:
		print('Removing object {} from old scene'.format(object.name))
		bpy.context.scene.objects.unlink(object)
		bpy.data.objects.remove(object)
	print('---------------------------------------')

def removeAllScenes():
	print('Found {} scenes'.format(len(bpy.data.scenes)));
	if len(bpy.data.scenes) > 0:
		for scene in bpy.data.scenes:
			for object in scene.objects:
				print('Removing object{} from scene{}'.format(object.name, scene.name))
				bpy.context.scene.objects.unlink(object);
				bpy.data.objects.remove(object);
			print('Removing scene: {}'.format(scene.name));
			if not(scene.name == bpy.data.scenes[-1].name):
				bpy.data.scenes.remove(scene);


	# Now delete the last scene
	print('Deleting last scene');
	bpy.ops.scene.delete();



def makeNewSceneByLinikingToMainScene(name):
	# creatr a new scene and bring it into context
	bpy.ops.scene.new(type='LINK_OBJECTS');
	bpy.context.scene.name = name;
	

# Method to set the current scene system to Metric, 
# the positional units in centimeters, and
# the rotational units in degrees
def setSystemUnits(scene):
	# Set units system to METRIC
	scene.unit_settings.system='METRIC';
	# Set the unit scale to 1.0 cm
	scene.unit_settings.scale_length=1/100;
	# Set the rotation units
	scene.unit_settings.system_rotation = 'DEGREES';

def setSceneSize(scene, sizeX, sizeY):
	scene.render.resolution_x = sizeX;
	scene.render.resolution_x = sizeY;

# Method to add a camera
def addCamera(name, cameraPosVectorInCm, fieldOfViewInDegrees):
	# Add a new camera
	bpy.ops.object.camera_add();

	# The object we just added becomes the currently active object,
	# so we can access it via bpy.context.active_object
	cameraObject = bpy.context.active_object;
	cameraObject.name = name;

	# Set camera translation
	cameraObject.location = cameraPosVectorInCm;
	# Set the camera size (only useful for visualizing it)
	cameraObject.scale = 20 * Vector((1, 1, 1));
	# Set camera fov in degrees
	cameraObject.data.angle = fieldOfViewInDegrees;


def pointCameraToTarget(camera, targetPosVectorInCm):
	deltaVector = targetPosVectorInCm - camera.location;
	dr = math.sqrt(math.pow(deltaVector.x,2.0) + math.pow(deltaVector.y,2.0));
	xRad = math.pi/2.0 + math.atan2(deltaVector.z, dr);
	zRad = math.atan2(deltaVector.y, deltaVector.x) - math.pi/2.0;
	angles = (xRad, 0, zRad);
	camera.rotation_euler = Euler(angles, 'XYZ');


# Method to add a cube of a given size at a given position in the current scene
def addCubeObject(name, sizeInCm, positionVectorInCm):
	# scale the size according to the unit_settings.scale_length
	sizeVectorInCm = sizeInCm * Vector((1, 1, 1)) * bpy.context.scene.unit_settings.scale_length;
	
	# Generate default cube at (0,0,0) and add it to the scene
	cubeObject = bpy.ops.mesh.primitive_cube_add(location=(0,0,0));
	# The object we just added becomes the currently active object,
	# so we can access it via bpy.context.active_object
	cubeObject = bpy.context.active_object;
	# Scale to desired size
	cubeObject.scale = sizeVectorInCm;
	# Move to desired location
	cubeObject.location = positionVectorInCm;
	# Give it a name for easy retrieval
	cubeObject.name = name;

	position = list(positionVectorInCm);
	# Report what happened
	print('Added a cube named [ {:>8s} ] at position <x,y,z> = <{:>+6.1f}, {:>+6.1f}, {:>+6.1f}>'.format(name, position[0], position[1], position[2]));


# Main
# Go through all objects, unlink them and remove them
removeAllScenes();


if True:
	# Rename the current empty scene as our cyclopean scene
	cyclopeanScene = bpy.context.scene;
	cyclopeanScene.name = 'AchromForcedChoiceRoom';
	# Set sytem units for the cyclopean scene
	setSystemUnits(cyclopeanScene);
	setSceneSize(cyclopeanScene,1920,1200);


	# Add objects to the scene
	# First, we are adding 5 identical cubes at 5 different positions
	sizeInCm = 10;
	positionInCm = Vector((50,0,-20));
	addCubeObject('cubeA', sizeInCm, positionInCm);

	positionInCm = Vector((-50,0,-20));
	addCubeObject('cubeB', sizeInCm, positionInCm);

	positionInCm = Vector((0,-20,-20));
	addCubeObject('cubeC', sizeInCm, positionInCm);

	positionInCm = Vector((0, 20, -20));
	addCubeObject('cubeD', sizeInCm, positionInCm);

	# Add cube at (x,y) = (0,0) subspended at z = + 20cm
	positionInCm = Vector((0, 0, 0));
	addCubeObject('cubeE', sizeInCm, positionInCm);

	positionInCm = Vector((0, 0, 30));
	addCubeObject('cubeF', sizeInCm, positionInCm);


	# Specify inter-ocular distance
	interOcularSeparationInCm = 7.6 * 4;
	fieldOfViewInDegrees = 90;

	# switch to Cyclopean Scene
	bpy.context.screen.scene=bpy.data.scenes['AchromForcedChoiceRoom'];

	# Generate the cyclopean camera
	cameraPosVectorInCm = Vector((0.0, -76.3, 20.0));  # 20 cm above the floor, 76.3 behind
	addCamera('Cyclopean Camera', cameraPosVectorInCm, fieldOfViewInDegrees);
	cyclopeanCameraObject = bpy.data.objects['Cyclopean Camera']; 
	# specify where the camera is looking at. 
	targetPosVectorInCm = Vector((0.0, 0.0, 0.0));
	pointCameraToTarget(cyclopeanCameraObject, targetPosVectorInCm);
	#bpy.data.scenes['AchromForcedChoiceRoom'].camera = bpy.data.objects['Cyclopean Camera']; 


	bpy.ops.object.camera_add();
	leftCameraObject = bpy.context.active_object;
	leftCameraObject.name = 'Left Camera';
	leftCameraObject.location = Vector((-interOcularSeparationInCm/2.0* bpy.context.scene.unit_settings.scale_length, 0.0, 0.0));
	leftCameraObject.scale = Vector((1, 1, 1));
	leftCameraObject.data.angle = 90;


	bpy.ops.object.camera_add();
	rightCameraObject = bpy.context.active_object;
	rightCameraObject.name = 'Right Camera';
	rightCameraObject.location = Vector((interOcularSeparationInCm/2.0* bpy.context.scene.unit_settings.scale_length, 0.0, 0.0));
	rightCameraObject.scale = Vector((1, 1, 1));
	rightCameraObject.data.angle = 90;



	# Make left and right cameras children of the cyclopean camera so that they all move together
	leftCameraObject.parent = cyclopeanCameraObject;
	# Make left and right cameras children of the cyclopean camera so that they all move together
	rightCameraObject.parent = cyclopeanCameraObject;


	# Create the left- and right-eye view scenes by copying the cyclopean scene
	makeNewSceneByLinikingToMainScene('AchromForcedChoiceRoom-Left');
	
	# Create the left- and right-eye view scenes by copying the cyclopean scene
	makeNewSceneByLinikingToMainScene('AchromForcedChoiceRoom-Right');




	# switch to Left Scene
	bpy.context.screen.scene=bpy.data.scenes['AchromForcedChoiceRoom-Left'];

	bpy.data.scenes['AchromForcedChoiceRoom-Left'].camera  = bpy.data.objects['Left Camera'];

	# switch to Right Scene
	bpy.context.screen.scene=bpy.data.scenes['AchromForcedChoiceRoom-Right'];
	


	# Assign cameras to scenes	
	bpy.data.scenes['AchromForcedChoiceRoom-Right'].camera = bpy.data.objects['Right Camera'];
	

	cyclopeanCameraObject.select = True;
	leftCameraObject.select = False;
	rightCameraObject.select = False;


	# Make the cyclopean scene current
	bpy.context.screen.scene=bpy.data.scenes['AchromForcedChoiceRoom'];
