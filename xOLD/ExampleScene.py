# Import the Blender-Python API module
import bpy

# Import mathutils, math, and random modules
from mathutils import *
from math import *
from random import *

class BlenderScene:
    # Method to initialize the BlenderScene object
    def __init__(self, params):
        print('\nInitializing new scene');
        # check whether we need to remove objects from an existing scene
        if params['eraseOLDscene']:
            self.removeAllObjects();
            #self.deleteSceneObjects();
        # set the camera position
        self.positionCamera();
        # set the scene name
        bpy.context.screen.scene.name  = params['name'];    
        # set scene dimensions
        bpy.context.screen.scene.render.resolution_x = params['sceneWidthInPixels'];
        bpy.context.screen.scene.render.resolution_y = params['sceneHeightInPixels'];
        # set the unit system to Metric and the unit scale to 1.0 cm 
        bpy.context.screen.scene.unit_settings.system = 'METRIC';
        bpy.context.screen.scene.unit_settings.scale_length = params['sceneUnitScale'];
        # set the grid spacing and the number of grid lines
        self.setGrid(params['sceneGridSpacing'], params['sceneGridLinesNum']);
        
        print('\nInitialized "{}" scene'.format(bpy.context.screen.scene.name));
        
    # Method to remove a single oject in an existing scene
    def removeObjectFromScene(self, objectToRemove):
        bpy.context.scene.objects.unlink(objectToRemove);
        bpy.data.objects.remove(objectToRemove);

    # Method to remove all objects in an existing scene
    def removeAllObjects(self):
        for object in bpy.data.objects:
            if not(object.name == 'Camera2222'):
                print('Existing object "{}", will be removed from old scene ("{}")'.format(object.name, bpy.context.scene.name));
                self.removeObjectFromScene(object);  
            else:
                print('Existing object "{}", will not be removed from old scene ("{}")'.format(object.name, bpy.context.scene.name));
               
    # Alternate way to remove all objects
    def deleteSceneObjects(self):
        # gather all objects whose type is 'MESH'
        namesOfObjectsForDeletion = [item.name for item in bpy.data.objects if item.type == 'MESH'];
        # select these objects
        for objectName in namesOfObjectsForDeletion:
            bpy.data.objects[objectName].select = True;
            print('Will delete {} object'.format(objectName));
        # delete all selected objects
        bpy.ops.object.delete();
        # finally remove all meshes
        for item in bpy.data.meshes:
            print('Will delete {}  mesh'.format(item.name));
            bpy.data.meshes.remove(item);

    # Method to set the grid spacing and the number of grid lines      
    def setGrid(self, gridSpacing, gridLinesNum):
        # Search all Blender windows to find the grid_space parameter
        # print('Will search {} Blender windows'.format(len(bpy.data.screens)));
        foundIt = False;
        for screen in bpy.data.screens:
            # print('  Screen {} has {} areas'.format(screen.name, len(screen.areas)));
            # Loop through all areas to find one who's type is 'VIEW_3D'
            # Such an area is a subclass of SpaceView3D, in which grid params are defined
            for area in screen.areas:
                # print('   Area is of type {}'.format(area.type));
                if area.type == 'VIEW_3D':
                    # search through the spaces to find spaces with type VIEW_3D
                    # print('    Will search {} spaces in current area'.format(len(area.spaces)))
                    for space in area.spaces:
                        if space.type == 'VIEW_3D':
                            #print('    >> Setting grid scale')
                            foundIt = True;
                            space.grid_scale = gridSpacing;
                            space.grid_lines = gridLinesNum;
        if not(foundIt):
            print('Did not find any "VIEW_3D" space in which the grid is defined');

    # Method to set the camera position and orientation
    def positionCamera(self):
        # Add camera
        camOBJ = bpy.data.cameras.new('Camera');
        # Field of View
        camOBJ.angle = pi/2;
        camOBJ.draw_size = 10;
        camOBJ.sensor_width = 63;
        camOBJ.sensor_height = 20;
        camOBJ.angle_x = pi/2;
        camOBJ.angle_y = pi/4;
        
        # Projection type
        camOBJ.type = 'PERSP';
        
        cameraOBJ = bpy.data.objects.new('My camera', camOBJ);
        
        cameraOBJ.select = True;
        cameraOBJ.rotation_euler = (pi/2,0,0);
        cameraOBJ.location = (0,-70,20);
       
        # Set camera fov in degrees
        #fieldOfViewInDegrees = 35;
        #bpy.context.screen.scene.camera.data.angle = fieldOfViewInDegrees/180*pi;
        
        cameraPos = Vector((0.0,   # along horizontal (red, x) axis
                            70.0,  # along depth (green, y) axis
                            30,    # along vertical (blue,z) axis
                            ));
        targetPos = Vector((0.0,  # along horizontal (red, x) axis
                            0.0,  # along depth (green, y) axis
                            0,    # along vertical (blue,z) axis
                            ));
         
        # compute camera rotation so that it points to the targetPos
        deltaVector = cameraPos - targetPos;
        dr = sqrt(pow(deltaVector.x,2.0) + pow(deltaVector.y,2.0));
        xRad = atan2(deltaVector.z, dr);
        zRad = atan2(deltaVector.y, deltaVector.x);
        anglesVector = Vector(( xRad, # rotation around horizontal (red, x) axis       
                                0,    # rotation around depth (green, y) axis
                                zRad  # rotation around vertical (blue, z) axis 
                               ));
        print('angles = {}'.format(anglesVector))
        
        bpy.context.screen.scene.objects.link(cameraOBJ);
        

    # Method to shutdown the BlenderScene object           
    def shutdown(self):
        print('Bye now ... \n');
        
# -------- end of BlenderClass definition -------

# ------------ Convenience methods -------------
# Method to apply a spatial transformation to an object 
def doSpatialTrasform(obj,params):
    # Scale to desired size
    obj.scale = 0.5*params['sizeVector'];
    # Rotate to desired angle
    obj.rotation_euler = params['rotationVector'];
    # Move to desired location
    obj.location = params['positionVector'];
    # Return plane
    return(obj);
    
# Method to add a YZ plane of arbitrary YZ dimensions and arbitrary 3D position
def addPrincipalAxesPlane(name, principalAxes, dimensions, position3D):
    thickness = 0.0;
    # 3D rotation
    if principalAxes == 'YZ':
        horizAxisRotation = pi/2;    # rotation around horizontal (red, x) axis
        depthAxisRotation = 0;       # rotation around depth (green, y) axis
        vertAxisRotation =  pi/2;    # rotation around vertical (blue, z) axis
    elif principalAxes == 'XZ':
        horizAxisRotation = pi/2;    # rotation around horizontal (red, x) axis
        depthAxisRotation = 0;       # rotation around depth (green, y) axis
        vertAxisRotation =  0;       # rotation around vertical (blue, z) axis
    elif principalAxes == 'XY':
        horizAxisRotation = 0;       # rotation around horizontal (red, x) axis
        depthAxisRotation = 0;       # rotation around depth (green, y) axis
        vertAxisRotation =  0;       # rotation around vertical (blue, z) axis
    else:
        pass;
    
    # Add a plane mesh. By default, the plane is placed at the origin (0,0,0)
    bpy.ops.mesh.primitive_plane_add();
    # Get a reference to the plane just created.
    obj = bpy.context.active_object;
    # Give it desired name
    obj.name = name;
    # do not hide the plane
    obj.hide = False;
    
    # Transform added plane object so that it appears as desired
    xformParams = { 'sizeVector':     Vector((dimensions.x, dimensions.y, 0)),
                    'rotationVector': Vector((horizAxisRotation, depthAxisRotation, vertAxisRotation)),
                    'positionVector': position3D};
    obj = doSpatialTrasform(obj, xformParams);
    return(obj);

# Method to make the room enclosure
def addRoomEnclosureNEW(roomWidth, roomHeight, roomDepth, roomOffset):
    pass;
    
    
# Method to make the room enclosure
def addRoomEnclosure(roomWidth, roomHeight, roomDepth, roomOffset):
    # Add left- & right-side walls
    dimensions = Vector((roomDepth, roomHeight));
    # 3D position - right side wall
    center = Vector((roomWidth/2,  # along horizontal (red, x) axis
                     roomDepth/2,  # along depth (green, y) axis
                     roomHeight/2, # along vertical (blue,z) axis
                    ));
    rightSideWallObj = addPrincipalAxesPlane('RightSideWall', 'YZ', dimensions, center + roomOffset);
    # 3D position - left side wall
    center.x = -center[0];
    leftSideWallObj = addPrincipalAxesPlane('LeftSideWall',  'YZ', dimensions, center + roomOffset);

    # Add back-side wall
    dimensions = Vector((roomWidth, roomHeight));
    # 3D position
    center = Vector((0,            # along horizontal (red, x) axis
                     roomDepth,    # along depth (green, y) axis
                     roomHeight/2, # along vertical (blue,z) axis
                    ));
    backSideWallOBJ = addPrincipalAxesPlane('BackSideWall', 'XZ', dimensions, center + roomOffset);

    # Add floor & ceiling
    dimensions = Vector((roomWidth, roomDepth));
    # 3D position - floor
    center = Vector((0,            # along horizontal (red, x) axis
                     roomDepth/2,  # along depth (green, y) axis
                     0,            # along vertical (blue,z) axis
                    ));
    floorOBJ   = addPrincipalAxesPlane('Floor', 'XY', dimensions, center + roomOffset);
    # 3D position - ceiling
    center.z  = roomHeight;
    ceilingOBJ = addPrincipalAxesPlane('Ceiling', 'XY', dimensions, center + roomOffset);
    
    
# Method to add a new type of mesh (house)
def addHouse():
    # create house mesh 
    vertices = [(4, 2, 0), (4, -2, 0), (-4, -2, 0), (-4, 2, 0), (4, 2, 4), (4, -2, 4), (-4, -2, 4), (-4, 2, 4), (4, 0, 6), (-4, 0, 6)];
    faces = [(0, 1, 2, 3), (8, 9, 6, 5), (0, 4, 8, 5, 1), (1, 5, 6, 2), (2, 6, 9, 7, 3), (4, 0, 3, 7), (4, 7, 9, 8)] ;
    meshObj = bpy.data.meshes.new("Simple House Mesh") 
    meshObj.from_pydata(vertices, [], faces) 
    meshObj.validate() 
    meshObj.update() 

    # create house object
    obj = bpy.data.objects.new("Simple House OBJ", meshObj);
    
    # Transform houseso that it appears as desired
    horizontalScaling = 20;
    verticalScaling   = 20;
    depthScaling = 20;
    
    horizAxisRotation = 0;
    depthAxisRotation = 0;
    vertAxisRotation = pi/4;
    
    horizPosition = 0;
    vertPosition = 20;
    depthPosition = 30;
    position3D = Vector((horizPosition, depthPosition, vertPosition));
    xformParams = { 'sizeVector':     Vector((horizontalScaling, depthScaling, verticalScaling)),
                    'rotationVector': Vector((horizAxisRotation, depthAxisRotation, vertAxisRotation)),
                    'positionVector': position3D};
    obj = doSpatialTrasform(obj, xformParams);
    
    bpy.context.scene.objects.link(obj) 
    bpy.context.scene.update();
    return(obj);

     
# ---------- main() program begins here ---------

# Create a dictionary to store the scene parameters
sceneParams = {'name': 'Room with 4 balls',     # name of new scene
               'eraseOLDscene': True,           # erase old scene
               'sceneWidthInPixels' : 1920,     # 1920 pixels along the horizontal-dimension
               'sceneHeightInPixels' : 1200,    # 1200 pixels along the vertical-dimension
               'sceneUnitScale': 1.0/100.0,     # set unit scale to 1.0 cm
               'sceneGridSpacing': 10.0/100.0,  # set the spacing between grid lines to 10 cm
               'sceneGridLinesNum': 20,         # display 20 grid lines
              };
# Create an instance of BlenderScene
roomWithBalls = BlenderScene(sceneParams);

# Add room
# The x-axis is along the horizontal axis (red arrow in Blender's 3D View)
# the y-axis is along the depth axis (green arrow in Blender's 3D View) and that
# the z-axis is along the vertical axis (blue arrow in Blender's 3D View)

roomWidth  = 180; roomHeight = 90; roomDepth  = 74;
roomOffset = Vector(( 0, # along horizontal (red, x) axis
                      0, # along depth (green, y) axis
                      0  # along vertical (blue,z) axis
            ));
addRoomEnclosure(roomWidth, roomHeight, roomDepth, roomOffset);
houseOBJ = addHouse();


# Finally shutdown the BlenderScene object
roomWithBalls.shutdown();