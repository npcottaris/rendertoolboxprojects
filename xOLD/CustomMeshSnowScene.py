# Import the Blender-Python API module
import bpy

# Import mathutils, math, and random modules
from mathutils import *
from math import *
from random import *

class BlenderScene:
    # Method to initialize the BlenderScene object
    def __init__(self, params):
        print('\nInitializing new scene');
        # Remove objects from previous scene
        if params['erasePreviousScene']:
            self.unlinkAllObjects();
            self.removeAllMeshes();
            self.removeAllLamps();
            self.removeAllCameras();
            self.removeAllMaterials();
            self.removeAllObjects();
        
        # exposure boost    
        bpy.data.worlds[0].exposure = 0.5;
        # contrast boost
        bpy.data.worlds[0].color_range = 1;
        
        # Set scene name
        bpy.context.scene.name = params['name'];
        
        # Set rendering resolution   
        bpy.context.scene.render.resolution_x = params['sceneWidthInPixels'];
        bpy.context.scene.render.resolution_y = params['sceneHeightInPixels'];
        
        # Set rendering quality (highest possible)
        bpy.context.scene.render.resolution_percentage = 50;
        bpy.context.scene.render.use_antialiasing = True
        bpy.context.scene.render.use_full_sample = True

        # Set BLENDER as the rendering engine
        bpy.context.scene.render.engine = 'BLENDER_RENDER'; 
        
        # Set CYCLES as the rendering engine
        #bpy.context.scene.render.engine = 'CYCLES';
        #bpy.context.scene.cycles.samples = 100;
        #bpy.context.scene.cycles.film_exposure = 5;
        
        # Set image format
        bpy.context.scene.render.image_settings.file_format = 'TIFF'
        bpy.context.scene.render.image_settings.quality = 50;
        bpy.context.scene.render.image_settings.color_mode = 'RGB';

        # Set the unit system to Metric and the unit scale to 1.0 cm 
        bpy.context.screen.scene.unit_settings.system = 'METRIC';
        bpy.context.screen.scene.unit_settings.scale_length = params['sceneUnitScale'];
        # set the grid spacing and the number of grid lines
        self.setGrid(params['sceneGridSpacing'], params['sceneGridLinesNum']);
     
    # Method to set the grid spacing and the number of grid lines      
    def setGrid(self, gridSpacing, gridLinesNum):
        # Search all Blender windows to find the grid_space parameter
        # print('Will search {} Blender windows'.format(len(bpy.data.screens)));
        foundGridParam = False;
        for screen in bpy.data.screens:
            # print('  Screen {} has {} areas'.format(screen.name, len(screen.areas)));
            # Loop through all areas to find one who's type is 'VIEW_3D'
            # Such an area is a subclass of SpaceView3D, in which grid params are defined
            for area in screen.areas:
                # print('   Area is of type {}'.format(area.type));
                if area.type == 'VIEW_3D':
                    # search through the spaces to find spaces with type VIEW_3D
                    # print('    Will search {} spaces in current area'.format(len(area.spaces)))
                    for space in area.spaces:
                        if space.type == 'VIEW_3D':
                            #print('    >> Setting grid scale')
                            foundGridParam = True;
                            space.grid_scale = gridSpacing;
                            space.grid_lines = gridLinesNum;
        if not(foundGridParam):
            print('Did not find any "VIEW_3D" space in which the grid is defined');
               
    # Method to remove a single oject from the current scene
    def removeObjectFromScene(self,  object):
        # Remove the object from the scene
        print('Removing object "{}", from old scene ("{}")'.format(object.name, bpy.context.scene.name));
        bpy.data.objects.remove(object);
        
    # Method to remove all objects from the current scene
    def removeAllObjects(self):
        for object in bpy.data.objects:
            self.removeObjectFromScene(object);  
         
    def unlinkObjectFromScene(self,  object):
        # Check to see if the object is in the scene, and if it is, unlink it from the scene
        if object.name in bpy.context.scene.objects:
            bpy.context.scene.objects.unlink(object);
            print('Unlinking object "{}", from old scene ("{}")'.format(object.name, bpy.context.scene.name));
               
    # Method to unlink all objects from the current scene
    def unlinkAllObjects(self):
        for object in bpy.data.objects:
            self.unlinkObjectFromScene(object);
            
    # Method to remove all mesh data
    def removeAllMeshes(self):
        for mesh in bpy.data.meshes:
            print('Clearing all users for mesh "{}"'.format(mesh.name));
            mesh.user_clear();
            print('Removing mesh "{}", from old scene ("{}")'.format(mesh.name, bpy.context.scene.name));
            bpy.data.meshes.remove(mesh);
            
    # Method to remove all lamp data
    def removeAllLamps(self):
        for lamp in bpy.data.lamps:
            print('Clearing all users for lamp "{}"'.format(lamp.name));
            lamp.user_clear();
            print('Removing lamp "{}", from old scene ("{}")'.format(lamp.name, bpy.context.scene.name));
            bpy.data.lamps.remove(lamp);
            
    # Method to remove all camera data
    def removeAllCameras(self):
        for camera in bpy.data.cameras:
            print('Clearing all users for camera "{}"'.format(camera.name));
            camera.user_clear();
            print('Removing camera "{}", from old scene ("{}")'.format(camera.name, bpy.context.scene.name));
            bpy.data.cameras.remove(camera);
            
    # Method to remove all material data
    def removeAllMaterials(self):
        for material in bpy.data.materials:
            print('Clearing all users for material "{}"'.format(material.name));
            material.user_clear();
            print('Removing material "{}", from old scene ("{}")'.format(material.name, bpy.context.scene.name));
            bpy.data.materials.remove(material);
    
       
    # Method to add a plane with a desired scale, rotation, and location
    def addPlane(self,params):
        # generate mesh
        # vertices: list of 3-tuples, with each 3-tuple representing (X,Y,Z) coords of a vertex
        vertices = [(-1, -1, 0),(1, -1, 0),(1, 1, 0),(-1, 1, 0)];
        # edges: pairs, each pais containing two indices to the vertices list
        edges = [];
        # faces: list of N-tuples (N >= 3) containing indices to the vertices list
        faces = [(0,1,2,3)];
        meshObj = bpy.data.meshes.new('') 
        meshObj.from_pydata(vertices, edges, faces); 
        meshObj.validate();
        meshObj.update();

        # generate object
        obj = bpy.data.objects.new(params['name'], meshObj);
    
        # attach a material to the plane obj
        obj.data.materials.append(params['material']);

        xformParams = { 'sizeVector':     params['scaling'],
                        'rotationVector': params['rotation'],
                        'positionVector': params['position']
                    };
        obj = doSpatialTrasform(obj, xformParams);
    
        # link the planeobject to the scene
        bpy.context.scene.objects.link(obj);



    # Method to generate a point lamp type     
    def generatePointLamp(self):
        # generate a lamp type
        theLampType = bpy.data.lamps.new('pointLamp', 'POINT');
        # configure the lamp type
        theLampType.energy = 1;
        theLampType.color  = Vector((0.4, 0.3, 0.2));      
        theLampType.use_specular    = True;
        theLampType.use_diffuse     = True;
        theLampType.falloff_type    = 'INVERSE_SQUARE';
        theLampType.distance        = 30;  # falloff distance (1/2 intensity) in cm
        return(theLampType);
    
    # Method to generate an area lamp type     
    def generateAreaLamp(self):
        # generate a lamp type
        theLampType = bpy.data.lamps.new('areaLamp', 'AREA');
        # configure the lamp type
        theLampType.energy = 1;
        theLampType.color  = Vector((1.0, 1.0, 1.0));      
        theLampType.use_specular    = True;
        theLampType.use_diffuse     = True;
        theLampType.distance        = 30;  # falloff distance (1/2 intensity) in cm
        theLampType.shape           = 'RECTANGLE';
        theLampType.size            = 20;
        theLampType.size_y          = 30;
        return(theLampType);
    
    # Method to add a lamp object to the current scene
    def addLamp(self,params):
        # generate a lamp object
        theLamp = bpy.data.objects.new(params['name'], params['type']);
        # position the lamp object
        theLamp.location        = params['location'];
        theLamp.rotation_euler  = params['orientation'];
        theLamp.show_name       = params['showName'];
        # link the lamp object to the current scene (if not linked, the lamp is not functional)
        bpy.context.screen.scene.objects.link(theLamp);
        print('lamp type: {}'.format(theLamp.data.type));
#        if theLamp.data.type == 'AREA':
#            # if lamp type is AREA, add a transparent mesh of the same size and location as the added lamp 
#            # this will be used by RenderToolbox3 to create an area lamp
#            transparentMaterial = bpy.data.materials.new(params['name']);
#            transparentMaterial.alpha = 1;
#            planeParams = { 'name'    : '{}-mesh'.format(params['name']),
#                            'scaling' : Vector((theLamp.data.size, theLamp.data.size_y, 1)),
#                            'rotation': params['orientation'],
#                            'position': params['location'] + Vector((0,1,0)),
#                            'material': transparentMaterial,
#                          };
#            self.addPlane(planeParams);
        
    # Method to generate a camera type
    def generateCamera(self, params):
        # generate a camera type
        theCameraType = bpy.data.cameras.new('CAMERA');
        # configure the camera type
        clipRange = params['clipRange'];
        theCameraType.draw_size   = 5;   # Apparent size of the Camera object in the 3D View
        theCameraType.clip_start  = clipRange[0];
        theCameraType.clip_end    = clipRange[1];
        theCameraType.show_limits = True;  # draw the clipping range and focus point on the camera
        theCameraType.type = 'PERSP' ;   # perspective camera
        # Field of view: theta = 2 * tan[d/(2F)], d = sensor width, F: lens focal length
        # Camera lens field of view in degrees
        theCameraType.angle =  params['fieldOfViewInDegrees']/180*pi;
        return(theCameraType);
    
    # Method to add a camera object to the current scene
    def addCamera(self,params):
        # generate a lamp object
        theCamera = bpy.data.objects.new(params['name'], params['type']);
        # position the camera object
        theCamera.location        = params['location'];
        theCamera.rotation_euler  = params['orientation'];
        theCamera.show_name       = params['showName'];
        # link the camera object to the current scene (if not linked, the camera is not functional)
        bpy.context.screen.scene.objects.link(theCamera);
        
    # Method to generate a material
    def generateMaterial(self, params):
        theMaterialType = bpy.data.materials.new(params['name']);
        # Options for diffuse shaders: Minnaert, Fresnel, Toon, Oren-Nayar, Lambert
        theMaterialType.diffuse_shader      = params['diffuse_shader'];
        theMaterialType.diffuse_intensity   = params['diffuse_intensity'];
        theMaterialType.diffuse_color       = params['diffuse_color'];
        # Options for specular shaders: CookTorr, Phong, Blinn, Toon, WardIso
        theMaterialType.specular_shader     = params['specular_shader'];
        theMaterialType.specular_intensity  = params['specular_intensity'];
        theMaterialType.specular_color      = params['specular_color'];
        theMaterialType.alpha   = 1;
        theMaterialType.ambient = 1;
        return(theMaterialType);              
                    
    # Method to render the current scene from all linked cameras
    def renderFromAllCameras(self, filePath):
        # Get scene 
        currentScene = bpy.data.scenes[0];
        # Search for all cameras linked to the scene
        for object in bpy.data.objects:
            if object.type == 'CAMERA':
                # Make this camera the active one
                bpy.context.scene.camera = object;
                print('Rendering view from {}'.format(bpy.context.scene.camera.name));
                # generate render filename
                currentScene.render.filepath = '{}/{} {} view'.format(filePath, currentScene.name, bpy.context.scene.camera.name);
                bpy.ops.render.render(write_still = True, scene=currentScene.name);
                print('Finished with {} view',format(bpy.context.scene.camera.name));
    
    # Method to export a collada file for the current 3D scene 
    def exportToColladaFile(self, filePath):
        # Get scene 
        currentScene = bpy.data.scenes[0];
        fileName = '{}/{}.dae'.format(filePath, currentScene.name);
        #The transrotloc option is necessary for RT3 to successfully parse the collada file
        bpy.ops.wm.collada_export(filepath=fileName, export_transformation_type_selection='transrotloc');
    # Method to shutdown the BlenderScene object           
    def shutdown(self):
        print('Bye now ... \n');
        
# -------- end of BlenderScene class definition -------

    
# Method to apply a spatial transformation to an object 
def doSpatialTrasform(obj,params):
    # Rotate to desired angle
    obj.rotation_euler = params['rotationVector'];
    # Scale to desired size
    obj.scale = 0.5*params['sizeVector'];
    # Move to desired location
    obj.location = params['positionVector'];
    # Return plane
    return(obj);

# Method to add a custon mesh representing the ground
def addGroundMesh(material):
    # create ground mesh 
    # 
    xRange = 160;  # horizontal axis
    yRange = 200;  # depth axis
    # center
    xo = -xRange/2;
    yo = -yRange/2;
    # mesh resolution in cm
    dx = 0.2*5;
    dy = 0.2*5;
    xBinsNum = round(xRange/dx);
    yBinsNum = round(yRange/dy);
    
    # create a 2D list and fill it with 0
    elevation = [[0 for x in range(0,xBinsNum+1)] for y in range(0,yBinsNum+1)];
     
    # pre-create fy lists
    fy = [0 for x in range(0, yBinsNum+1)];
    fySinTheta = [0 for x in range(0, yBinsNum+1)];
    fyCosTheta = [0 for x in range(0, yBinsNum+1)];
          
    depthOffset = -30;        
    # large bumps
    for bumpIndex in range(1,10):
        sigmaX = 4+random()*3;
        sigmaY = sigmaX;
        maxBumpAmplitude = 0.0+random()*7.0;
        xc = xRange/2 + gauss(0,xRange/3);
        yc = depthOffset+yRange/2 + gauss(0,yRange/3);
        for y in range(0, yBinsNum+1):
            fy[y]= y*dy - yc;
        for x in range(0, xBinsNum+1):
            fx = x*dx - xc;
            for y in range(0, yBinsNum+1):
                elevation[y][x] += maxBumpAmplitude * exp(-0.5*pow(fx/sigmaX,2.0)) * exp(-0.5*pow(fy[y]/sigmaY,2.0));
    
    # medium bumps
    for bumpIndex in range(1,200):
        theta = gauss(pi/2-0.3*pi/4,pi/12);
        sinTheta = sin(theta);
        cosTheta = cos(theta);
        sigmaX = 0.7+random()*0.9;
        sigmaY = 4.0*sigmaX;
        maxBumpAmplitude = 2*(random()-0.5)*9;
        xc = xRange/2 + gauss(0,xRange/4);
        yc = depthOffset+yRange/2 + gauss(0,xRange/4);
        for y in range(0, yBinsNum+1):
            fy = y*dy - yc;
            fySinTheta[y] = fy * sinTheta;
            fyCosTheta[y] = fy * cosTheta;
        for x in range(0, xBinsNum+1):
            fx = x*dx - xc;
            fxCosTheta = fx * cos(theta);
            fxSinTheta = fx * sin(theta);
            for y in range(0, yBinsNum+1):
                xx = fxCosTheta - fySinTheta[y];
                yy = fxSinTheta + fyCosTheta[y];
                elevation[y][x] += maxBumpAmplitude * exp(-0.5*pow(xx/sigmaX,2.0)) * exp(-0.5*pow(yy/sigmaY,2.0));
     
    # small bumps
    for bumpIndex in range(1,500):
        theta = gauss(pi/2-0.3*pi/4,pi/12);
        sinTheta = sin(theta);
        cosTheta = cos(theta);
        sigmaX = 0.35+random()*0.35;
        sigmaY = 3.0*sigmaX;
        maxBumpAmplitude = 2*(random()-0.5)*4;
        xc = xRange/2 + gauss(0,xRange/8);
        yc = depthOffset+yRange/2 + gauss(0,xRange/8);
        for y in range(0, yBinsNum+1):
            fy = y*dy - yc;
            fySinTheta[y] = fy * sinTheta;
            fyCosTheta[y] = fy * cosTheta;
        for x in range(0, xBinsNum+1):
            fx = x*dx - xc;
            fxCosTheta = fx * cos(theta);
            fxSinTheta = fx * sin(theta);
            for y in range(0, yBinsNum+1):
                xx = fxCosTheta - fySinTheta[y];
                yy = fxSinTheta + fyCosTheta[y];
                elevation[y][x] += maxBumpAmplitude * exp(-0.5*pow(xx/sigmaX,2.0)) * exp(-0.5*pow(yy/sigmaY,2.0));


           
    maxElevation = max(max(elevation));
    minElevation = min(min(elevation));
    maxElevation = max([maxElevation, -minElevation]);
    print('max Elevation before scaling:{}', format(maxElevation));
    
    maxDesiredElevation = 1.75;
    rows = len(elevation);
    cols = len(elevation[0]);
    elevation = [[elevation[y][x]/maxElevation*maxDesiredElevation for x in range(0, cols)] for y in range(0, rows)];
                
    maxElevation = max(max(elevation));
    minElevation = min(min(elevation));
    maxElevation = max([maxElevation, -minElevation]);
    print('max Elevation after scaling:{}', format(maxElevation));
    
    # vertices: list of 3-tuples, with each 3-tuple representing (X,Y,Z) coords of a vertex
    vertices = [];
    # edges: pairs, each pais containing two indices to the vertices list
    edges = [];
    # faces: list of N-tuples (N >= 3) containing indices to the vertices list
    faces = [];
    
    for iy in range(1,yBinsNum+1,1):
        
        # first quad
        if iy == 1:
            # add the first quad along x axis for y = 1;
            newFaces = ((0, 1, 2, 3));
            elevation1 = elevation[0][0];
            elevation2 = elevation[0][1];
            elevation3 = elevation[1][1];
            elevation4 = elevation[1][0];
            newVertices = ((xo, yo+(iy-1)*dy, elevation1), (xo+dx, yo+(iy-1)*dy, elevation2), (xo+dx, yo+iy*dy, elevation3), (xo, yo+iy*dy, elevation4));
            topVertexIndicesFromLastRow = [3,2];
        else:
            # add the first quad along x axis for y > 1;
            newFaces = ((topVertexIndicesFromLastRow[0],topVertexIndicesFromLastRow[1], lastVertexIndex+1,lastVertexIndex+2));
            elevation1 = elevation[iy][1];
            elevation2 = elevation[iy][0];
            newVertices = ((xo+dx, yo+iy*dy, elevation1),(xo, yo+iy*dy, elevation2));
            topVertexIndicesFromLastRow2 = [lastVertexIndex+2,lastVertexIndex+1];
            lastVertexIndex = lastVertexIndex+2;
        vertices.extend(newVertices);    
        faces.append(newFaces);
        
        # subsequent quads
        if iy == 1:
            for ix in range(1,xBinsNum,1):
                if ix == 1:
                    ii = 1;
                else:
                    ii = ix*2;                
                elevation1 = elevation[0][ix+1];
                elevation2 = elevation[1][ix+1];
                newVertices = ((xo+(ix+1)*dx, yo+(iy-1)*dy, elevation1), (xo+(ix+1)*dx, yo+iy*dy, elevation2));
                newFaces = ((ii, ix*2+2, ix*2+3, ii+1));
                vertices.extend(newVertices);
                faces.append(newFaces);
                topVertexIndicesFromLastRow.append(ix*2+3);
                lastVertexIndex = ix*2+3;
        else:
            for ix in range(1,xBinsNum,1):
                if ix == 1:
                    newFaces = ((topVertexIndicesFromLastRow[ix], topVertexIndicesFromLastRow[ix+1], lastVertexIndex+1, lastVertexIndex-1));
                else:
                    newFaces = ((topVertexIndicesFromLastRow[ix], topVertexIndicesFromLastRow[ix+1], lastVertexIndex+1, lastVertexIndex));
                elevation1 = elevation[iy][ix+1];
                # just one new vertex (upper-right corner)
                newVertices = ((xo+(ix+1)*dx, yo+iy*dy, elevation1));
                vertices.append(newVertices);
                faces.append(newFaces);
                topVertexIndicesFromLastRow2.append(lastVertexIndex+1);
                lastVertexIndex = lastVertexIndex+1; 
            topVertexIndicesFromLastRow = topVertexIndicesFromLastRow2;

        
    meshObj = bpy.data.meshes.new("Ground Mesh") 
    meshObj.from_pydata(vertices, edges, faces); 
    meshObj.validate() 
    meshObj.update() 
    
    # create an object using the mesh just created.
    # Note: this implies that this object is a user of the mesh object.
    # So we have to first delete this object, then the mesh object
    obj = bpy.data.objects.new("Ground OBJ", meshObj);
    
    # attach a material to the house
    obj.data.materials.append(material);
    
    # link the ground object to the scene
    bpy.context.scene.objects.link(obj) ;
   
    return(obj);


# Method to add a custom mesh representing the house
def addHouse(params):
    # create house mesh 
    # vertices: each triplet represents (X,Y,Z) coords
    vertices = [(2, 1, 0), (2, -1, 0), (-2, -1, 0), (-2, 1, 0), 
                (2, 1, 2), (2, -1, 2), (-2, -1, 2), (-2, 1, 2), (2, 0, 3), (-2, 0, 3)];
    # edges: pairs, each pais containing two indices to the vertices list
    edges = [];
    # faces: list of N-tuples (N >= 3) containing indices to the vertices list
    faces = [(0, 1, 2, 3), (8, 9, 6, 5), (0, 4, 8, 5, 1), (1, 5, 6, 2), (2, 6, 9, 7, 3), (4, 0, 3, 7), (4, 7, 9, 8)];
    floor = (0, 1, 2, 3);
    leftRoof = (8, 9, 6, 5);
    rightRoof = (4, 7, 9, 8);
    leftSide = (1, 5, 6, 2);
    rightSide = (4, 0, 3, 7);
    frontSide = (0, 4, 8, 5, 1);
    rearSide = (2, 6, 9, 7, 3)
    faces = [leftRoof, rightRoof, leftSide, rightSide, frontSide, rearSide];
    
    meshObj = bpy.data.meshes.new("House Mesh") 
    meshObj.from_pydata(vertices, edges, faces); 
    meshObj.validate() 
    meshObj.update() 

    # create an object using the mesh just created.
    # Note: this implies that this object is a user of the mesh object.
    # So we have to first delete this object, then the mesh object
    obj = bpy.data.objects.new("House OBJ", meshObj);
    
    # attach a material to the house
    obj.data.materials.append(params['material']);
    
    # Scale house
    scaling = params['scaling'];
    horizontalScaling = scaling.x;  # along x-axis
    depthScaling      = scaling.y;  # along y-axis
    verticalScaling   = scaling.z;  # along z axis
            
    # rotate house
    horizAxisRotation = 0;
    depthAxisRotation = 0;
    vertAxisRotation = params['rotation']/180*pi;
    
    location = params['center'];
    horizPosition = location.x;
    depthPosition = location.y;
    vertPosition  = location.z; 
    position3D = Vector((horizPosition, depthPosition, vertPosition));
    xformParams = { 'sizeVector':     Vector((horizontalScaling, depthScaling, verticalScaling)),
                    'rotationVector': Vector((horizAxisRotation, depthAxisRotation, vertAxisRotation)),
                    'positionVector': position3D};
    obj = doSpatialTrasform(obj, xformParams);
    
    # link the house object to the scene
    bpy.context.scene.objects.link(obj) ;
    
    return(obj);
    
    
    
    
    
# ---------- main() program begins here ---------

# Create a dictionary to store the scene parameters
sceneParams = {'name'               : 'SnowScene',          # name of new scene
               'erasePreviousScene' : True,                 # erase old scene
               'sceneWidthInPixels' : 1920,                 # 1920 pixels along the horizontal-dimension
               'sceneHeightInPixels': 1200,                 # 1200 pixels along the vertical-dimension
               'sceneUnitScale'     : 1.0/100.0,            # set unit scale to 1.0 cm
               'sceneGridSpacing'   : 10.0/100.0,           # set the spacing between grid lines to 10 cm
               'sceneGridLinesNum'  : 10,                   # display 20 grid lines
              };
              
# Instantiate a BlenderScene object
myScene = BlenderScene(sceneParams);
         
# Generate a point lamp type         
pointLamp = myScene.generatePointLamp();

# Generate an area lamp type
areaLamp = myScene.generateAreaLamp();
           
# Generate our camera type
nearClipDistance = 0.1;
farClipDistance  = 300;
params = {  'clipRange'             : Vector((nearClipDistance , farClipDistance)),
            'fieldOfViewInDegrees'  : 36,   # horizontal FOV
         };
wideScreenCamera = myScene.generateCamera(params);


# Now generate scene

# -------------- Add Cameras ------------------
cameraToSceneCenterDistance = 150;
cameraTilt = -13;   # negative tilt -> downward looking camera
cameraElevation = 30;

# Left camera
params = {  'name'          : 'Camera', 
            'type'          : wideScreenCamera,
            'location'      : Vector((-3.2, -cameraToSceneCenterDistance , cameraElevation)),     
            'orientation'   : Vector((pi/2 + cameraTilt/180*pi, 0,0)),    #
            'showName'      : True,
          };          
myScene.addCamera(params);


## Right camera
#params = {  'name'          : 'R-cam', 
#            'type'          : wideScreenCamera,
#            'location'      : Vector((3.2, -cameraToSceneCenterDistance , cameraElevation)),
#            'orientation'   : Vector((pi/2 + cameraTilt/180*pi,0,0)),     
#            'showName'      : True,
#          };  
#          
#addSecondCamera = False;
#if addSecondCamera:        
#    myScene.addCamera(params);

# ---------------------------------------------

# ------------ Generate materials ------------------
params = {  'name': 'house material',
            'diffuse_shader': 'LAMBERT',
            'diffuse_intensity': 0.8,
            'diffuse_color': Vector((0.05, 0.2, 0.6)),
            'specular_shader': 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color': Vector((0.08, 0.1, 0.41)),
        };
        
houseMaterial = myScene.generateMaterial(params);

params = {  'name': 'snow at night',
            'diffuse_shader': 'LAMBERT',
            'diffuse_intensity': 0.8,
            'diffuse_color': Vector((0.6, 0.6, 0.60)),
            'specular_shader': 'WARDISO',
            'specular_intensity': 0.12,
            'specular_color': Vector((0.9, 0.9, 0.6)),
        };
        
snowAtNightMaterial = myScene.generateMaterial(params);
       
params = {  'name': 'backwall material',
            'diffuse_shader': 'LAMBERT',
            'diffuse_intensity': 0.0,
            'diffuse_color': Vector((0.0, 0.0, 0.00)),
            'specular_shader': 'WARDISO',
            'specular_intensity': 0.0,
            'specular_color': Vector((0.0, 0.0, 0.0)),
        };
        
backWallMaterial = myScene.generateMaterial(params);

# -----------------------------------
     
backWallParams = {'name'    : 'BackWallPlane',
                  'scaling' : Vector((200, 150, 1)),
                  'rotation': Vector((pi/2, 0, 0)),
                  'position': Vector((0, 100,0)),
                  'material': backWallMaterial,
                  };
myScene.addPlane(backWallParams);
                    
addGroundMesh(snowAtNightMaterial);     

houseParams = { 'center'    : Vector((0,-30,-1.0)),  # -1 along z to make sure it's straight base is invisible
                'scaling'   : Vector((20,30,17)),
                'rotation'  :-30,
                'material'  : houseMaterial,
                };          
addHouse(houseParams);

# Let there be some light !
# -------------- Add lamps ------------------
# point lamps in a circle
radius = 18;
lampElevation = 1.8;

houseRotation = houseParams['rotation'];

#for angle in range(0,360,30):
#    theta = angle/180 * pi;
#    xPos = radius*1.5 * cos(theta);
#    yPos = radius * sin(theta);
#    xxPos = houseParams['center'].x + xPos * cos(houseRotation/180*pi) - yPos * sin(houseRotation/180*pi);
#    yyPos = houseParams['center'].y + xPos * sin(houseRotation/180*pi) + yPos * cos(houseRotation/180*pi);
#    params = {  'name'          : 'point lamps', 
#                'type'          : pointLamp,
#                'location'      : Vector((xxPos, yyPos, lampElevation)),
#                'orientation'   : Vector((0,0,-pi)),
#                'showName'      : False,
#              };          
#    myScene.addLamp(params);
    

# Area lamp
height = 30;
# For lamps pointing upwards use negative tilt, for downwards use positive
tilt = pi/3;  
distanceToSceneCenter = -50;
 
params = {  'name'          : 'left area lamp', 
            'type'          : areaLamp,
            'location'      : Vector((-40, distanceToSceneCenter , height)),
            'orientation'   : Vector((-pi/2+pi/6, -pi/2 + tilt, pi/2)),
            'showName'      : True,
          };          
myScene.addLamp(params);

params = {  'name'          : 'right area lamp', 
            'type'          : areaLamp,
            'location'      : Vector((40, distanceToSceneCenter , height)),
            'orientation'   : Vector((pi/2-pi/6, -pi/2 + tilt, pi/2)),
            'showName'      : True,
          };          
myScene.addLamp(params);



myScene.renderFromAllCameras('/Users/Shared/Dropbox/Python/BlenderPython/BlenderStartupItems');
myScene.exportToColladaFile('/Users/Shared/Dropbox/Python/BlenderPython/BlenderStartupItems');